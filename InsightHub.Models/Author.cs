﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InsightHub.Models
{
    public class Author
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        public Guid UserId { get; set; }
        public User User { get; set; }
        public ICollection<Report> WrittenReports { get; set; }
        [StringLength(450, ErrorMessage ="{0} must be between {2} and {1} characters", MinimumLength = 100)]
        public string Bio { get; set; }
        public string ImagePath { get; set; }
    }
}
