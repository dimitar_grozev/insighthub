﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Models
{
	public class ClientReports
    {
        [Required]
        public Guid UserId { get; set; }
        public Client Client { get; set; }
        [Required]
        public Guid ReportId { get; set; }
        public Report Report { get; set; }
    }
}
