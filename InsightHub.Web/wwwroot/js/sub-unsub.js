﻿$(function () {
    $(document).on('click', '#sub', function (x) {
        x.preventDefault();
        let industry = $(this).data("value");

        $.ajax({
            url: '/Users/Subscribe/',
            type: "POST",
            data: { 'industry': industry },
            success: function (data) {
                $("#sub").html("Unsubscribe");
                $("#sub").attr("id", "unsub");
            },
            error: function (data) {
                console.log("Error");
            }
        })
    });
});

$(function () {
    $(document).on('click', '#unsub', function (x) {
        x.preventDefault();
        let industry = $(this).data("value");

        $.ajax({
            url: '/Users/Unsubscribe/',
            type: "POST",
            data: { 'industry': industry },
            success: function (data) {
                $("#unsub").html("Subscribe");
                $("#unsub").attr("id", "sub");
            },
            error: function (data) {
                console.log("Error");
            }
        })
    });
});