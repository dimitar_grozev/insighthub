﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Azure;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InsightHub.Web.Controllers
{
    public class BlobsController : Controller
    {
        private readonly IBlobService blobService;
        private readonly IUserService userService;

        public BlobsController(IBlobService blobService, IUserService userService)
        {
            this.blobService = blobService;
            this.userService = userService;
        }

        [HttpGet]
        [Authorize(Roles = "Client, Admin, Author")]
        public async Task<IActionResult> GetBlob(string reportId)
        {
            try
            {
                var data = await this.blobService.GetBlobAsync(reportId);

                var isLoggedIn = User.Identity.IsAuthenticated;
                if (isLoggedIn && User.IsInRole("Client"))
                {
                    var userId = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
                    await this.userService.DownloadAsync(userId, Guid.Parse(reportId));
                }

                return File(data.Content, data.ContentType);
            }
            catch (RequestFailedException)
            {
                return Unauthorized();
            }
            catch
            {
                return NotFound();
            }
        }

        //[HttpPost]

        //public async Task<IActionResult> UploadFile([FromBody] UploadFileRequest request)
        //{
        //    await this.blobService.UploadFileBlobAsync(request);

        //    return Ok();
        //}

        [HttpDelete]
        public async Task<IActionResult> DeleteBlob(string id)
        {
            try
            {
                await this.blobService.DeleteBlob(id);

                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }
    }
}