﻿using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using InsightHub.Web.Mappers;
using InsightHub.Web.Models.AuthorsPageModels;
using Microsoft.AspNetCore.Mvc;

namespace InsightHub.Web.Controllers
{
    public class AuthorsController : Controller
    {
        private readonly IUserService userService;
        private readonly IReportService reportService;

        public AuthorsController(IUserService userService,IReportService reportService)
        {
            this.userService = userService;
            this.reportService = reportService;
        }
        // GET: Authors
        public async Task<ActionResult> Index()
        {
            var authorDtos =await this.userService.GetAuthorsForPage();
            var authorsVM = authorDtos.GetVM();

            return View(authorsVM);
        }

        // GET: Authors/Details/5
        public async Task<ActionResult> Details(AuthorReportsQueryVM reportQuery)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (reportQuery.CurrentPage == 0)
                        reportQuery.CurrentPage = 1;

                    var reports = await this.reportService.GetReportByPage(null,reportQuery.SearchCriteria,reportQuery.SearchString,null,reportQuery.CurrentPage, reportQuery.ItemsPerPage);
                    var authorInfo = await this.userService.GetAuthorInfoAsync(reportQuery.UserId);
                    reportQuery.Reports = reports.Item1.GetPageVM();
                    reportQuery.TotalPages = reports.Item2;
                    reportQuery.ImagePath = authorInfo[0];
                    reportQuery.Bio = authorInfo[1];
                    return View(reportQuery);
                }
                catch
                {
                    return NotFound();
                }
            }
            return RedirectToAction("Index", "Home");
        }


    }
}