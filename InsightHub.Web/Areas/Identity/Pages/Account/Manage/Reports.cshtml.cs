using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Web.Areas.Admin.Models;
using InsightHub.Web.Mappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace InsightHub.Web.Areas.Identity.Pages.Account.Manage
{
	public class ReportsModel : PageModel
	{
		private readonly IReportService reportService;
		public ICollection<ReportVM> Reports;
		public ReportsModel(IReportService reportService)
		{
			this.reportService = reportService;
		}
		public async Task<IActionResult> OnGet()
		{
			var userId = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
			var role = User.FindFirst(ClaimTypes.Role).Value.ToLower();
			ICollection<ReportDTO> reports = new List<ReportDTO>();
			if (role == "author")
			{
				reports =await this.reportService.GetAuthorReportsAsync(userId);

			}
			else if (role == "client")
			{
				reports = await this.reportService.GetClientReportsAsync(userId);

			}
			this.Reports = reports.GetAdminReport();

			return Page();
		}
	}
}
