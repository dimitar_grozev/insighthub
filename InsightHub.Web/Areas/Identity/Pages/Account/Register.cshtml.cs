﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using InsightHub.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using InsightHub.Services.Contracts;
using NToastNotify;

namespace InsightHub.Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly IUserService userService;
        private readonly IToastNotification toastNotification;
        private readonly INotificationService notificationService;

        public RegisterModel(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender,
            IUserService userService,
            IToastNotification toastNotification,
            INotificationService notificationService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            this.userService = userService;
            this.toastNotification = toastNotification;
            this.notificationService = notificationService;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public class InputModel
        {
            [Required]
            [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Email ex: johndoe@sth.else")]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 4)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
            [Required]
            [Display(Name = "First Name")]
            [StringLength(30, ErrorMessage = "{0} must have max length of {1}")]
            [RegularExpression(@"[\D]+", ErrorMessage ="First name cannot include non-alphabetical symbols")]
            public string FirstName { get; set; }
            [Required]
            [Display(Name = "Last Name")]
            [StringLength(40, ErrorMessage = "{0} must have max length of {1}")]
            [RegularExpression(@"[\D]+", ErrorMessage = "Last name cannot include non-alphabetical symbols")]
            public string LastName { get; set; }
            [Required]
            [RegularExpression(@"^[+][0-9]+", ErrorMessage = "Telephone number should start with '+' followed by numbers only")]
            public string Telephone { get; set; }
            public bool Role { get; set; }
            [Display(Name ="About you")]
            [StringLength(450, ErrorMessage = "{0} must be between {2} and {1} characters", MinimumLength = 100)]
            public string Bio { get; set; }
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            try
            {

                returnUrl = returnUrl ?? Url.Content("~/");
                ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
                if (ModelState.IsValid)
                {
                    var user = new User { UserName = Input.Email, Email = Input.Email, FirstName = Input.FirstName,
                                          LastName = Input.LastName, PhoneNumber = Input.Telephone, Fullname=Input.FirstName + " " + Input.LastName};

                    var result = await _userManager.CreateAsync(user, Input.Password);
                    if (result.Succeeded)
                    {
                        await this.notificationService.CreateNotificationAsync($"User {Input.Email} has registered on {DateTime.Now}");
                        if(Input.Role)
                        {
                            await this.userService.CreateAuthorAsync(user.Id,Input.Bio);
                            await _userManager.AddToRoleAsync(user, "Author");
                        }
                        else
                        {
                            await this.userService.CreateClientAsync(user.Id);
                            await _userManager.AddToRoleAsync(user, "Client");
                        }
                        _logger.LogInformation("User created a new account with password.");

                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                        var callbackUrl = Url.Page(
                            "/Account/ConfirmEmail",
                            pageHandler: null,
                            values: new { area = "Identity", userId = user.Id, code = code },
                            protocol: Request.Scheme);

                        await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                            $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                        if (_userManager.Options.SignIn.RequireConfirmedAccount)
                        {
                            return RedirectToPage("RegisterConfirmation", new { email = Input.Email });
                        }
                        else
                        {
                            this.toastNotification.AddInfoToastMessage("Please wait for admin approval!");
                            return RedirectToAction("Index", "Home", new { area = ""});
                        }
                    }
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            catch
            {
                this.toastNotification.AddWarningToastMessage("Something went wrong! Please try again");
                return Page();
            }
            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
