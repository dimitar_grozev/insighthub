﻿using System;
using System.ComponentModel;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Areas.Author.Models
{
	public class CreateReportVM
    {
		[DisplayName("Title")]
		[Required]
		[StringLength(150, ErrorMessage = "{0} must be between {2} and {1} characters", MinimumLength = 10)]
		public string Name { get; set; }
		[DisplayName("Industry")]
		[Required]
		public Guid IndustryId { get; set; }
		[DisplayName("Short summary")]
		[Required]
		[MinLength(500, ErrorMessage = "{0} must be at least {1} characters long")]
		public string Summary { get; set; }
		[Required]
		public IFormFile Content { get; set; }
		[DisplayName("Tags (max. 3)")]
		public string Tags { get; set; }
		public IFormFile ImagePath { get; set; }
	}
}
