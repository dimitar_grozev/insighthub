﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using InsightHub.Web.Areas.Author.Models;
using InsightHub.Web.Mappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NToastNotify;

namespace InsightHub.Web.Areas.Author.Controllers
{
    [Area("Author")]
    [Authorize(Roles = "Author, Admin")]

    public class ReportsController : Controller
    {
        private readonly IReportService reportService;
        private readonly IIndustryService industryService;
        private readonly IBlobService blobService;
        private readonly IPhotoService photoService;
        private readonly IToastNotification toastNotification;

        public ReportsController(IReportService reportService, IIndustryService industryService, 
                                 IBlobService blobService, IPhotoService photoService,IToastNotification toastNotification)
        {
            this.reportService = reportService;
            this.industryService = industryService;
            this.blobService = blobService;
            this.photoService = photoService;
            this.toastNotification = toastNotification;
        }


        // GET: Reports/Create
        public async Task<ActionResult> Create()
        {
            try
            {
                ViewData["IndustryId"] = new SelectList(await this.industryService.GetAsync(), "Id", "Name");

                return View();
            }
            catch
            {
                this.toastNotification.AddWarningToastMessage("Something went wrong! Count not create report!");
                return RedirectToAction("Index", "Home", new { area = ""});

            }
        }

        // POST: Reports/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateReportVM report)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var reportDto = report.GetDto();
                    reportDto.AuthorId = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
                    //TODO Private method for userID
                    var newReport = await this.reportService.CreateAsync(reportDto);



                    await this.blobService.UploadFileBlobAsync(newReport.Id.ToString(), report.Content);
                    if(report.ImagePath != null)
                    {
                        await this.photoService.UploadPhoto(report.ImagePath, newReport.ImagePath);
                    }

                    this.toastNotification.AddInfoToastMessage($"{report.Name} was successfully created. Please for admin approval!");
                    return RedirectToAction("Index", "Home", new { area = ""});
                }
                catch
                {
                    this.toastNotification.AddWarningToastMessage("Something went wrong! Count not create report!");
                    return RedirectToAction("Create", "Reports");
                }
            }

            return RedirectToAction("Create", "Reports");
        }

        // GET: Reports/Edit/5
        public async Task<ActionResult> Edit(Guid reportId)
        {
            try
            {
                ViewData["IndustryId"] = new SelectList(await this.industryService.GetAsync(), "Id", "Name");

                var report = await this.reportService.GetAsync(reportId);
                var reportVm = report.GetUpdateVM();
                return View(reportVm);
            }
            catch
            {
                this.toastNotification.AddWarningToastMessage("Something went wrong! Count not edit report!");
                return RedirectToAction("Index", "Home", new { area = "" });
            }
        }

        // POST: Reports/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(UpdateReportVM report, string url)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var reportDto = report.GetDTO();
                    var newReport = await this.reportService.UpdateAsync(reportDto);
                    if(report.Content != null)
                    {
                        await this.blobService.DeleteBlob(report.Id.ToString());
                        await this.blobService.UploadFileBlobAsync(newReport.Id.ToString(), report.Content);
                    }
                    if (report.Photo != null)
                    {
                        await this.photoService.UploadPhoto(report.Photo, newReport.ImagePath);
                    }

                    if (url == "author")
                        return RedirectToPage("/Account/Manage/Reports", new { area = "Identity" });

                    return RedirectToAction("Index", "Reports", new { area = "" });
                }
                catch
                {
                    this.toastNotification.AddWarningToastMessage("Something went wrong! Count not edit report!");
                    return RedirectToAction("Edit", "Reports");
                }
            }
            return RedirectToAction("Edit", "Reports");
        }

        public async Task<ActionResult> Delete(Guid reportId, string url)
        {
            try
            {
                await this.reportService.DeleteAsync(reportId);
                await this.blobService.DeleteBlob(reportId.ToString());

                if (url == "author")
                    return RedirectToPage("/Account/Manage/Reports", new { area = "Identity" });

                return RedirectToAction("Index", "Reports", new { area = "" });
            }
            catch
            {
                this.toastNotification.AddWarningToastMessage("Something went wrong! Count not delete report!");

                return View();
            }
        }
    }
}