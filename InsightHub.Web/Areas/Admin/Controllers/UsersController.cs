﻿using System;
using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using InsightHub.Web.Areas.Admin.Models;
using InsightHub.Web.Mappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;

namespace InsightHub.Web.Areas.Admin.Controllers
{
	[Area("Admin")]
	[Authorize(Roles = "Admin")]
	public class UsersController : Controller
	{
		private readonly IUserService userService;
		private readonly IToastNotification toastNotification;
		private readonly INotificationService notificationService;
		private readonly IReportService reportService;

		public UsersController(IUserService userService,
								IToastNotification toastNotification,
								INotificationService notificationService,
								IReportService reportService)
		{
			this.userService = userService;
			this.toastNotification = toastNotification;
			this.notificationService = notificationService;
			this.reportService = reportService;
		}
		// GET: Users
		public async Task<ActionResult> Authors()
		{
			try
			{
				var active = await this.userService.GetActiveAuthorsAsync();
				var disabled = await this.userService.GetDisabledAuthorsAsync();
				var activeVM = active.GetVM();
				var disabledVM = disabled.GetVM();
				var usersVM = new UsersVM { Active = activeVM, Disabled = disabledVM };

				return View(usersVM);
			}
			catch
			{
				this.toastNotification.AddWarningToastMessage("Something went wrong.Please try again later!");
				return RedirectToAction("Index", "Home", new { area = "" });
			}
		}
		public async Task<ActionResult> Clients()
		{
			try
			{
				var active = await this.userService.GetActiveClientsAsync();
				var disabled = await this.userService.GetDisabledClientsAsync();
				var activeVM = active.GetVM();
				var disabledVM = disabled.GetVM();
				var usersVM = new UsersVM { Active = activeVM, Disabled = disabledVM };

				return View(usersVM);
			}
			catch
			{
				this.toastNotification.AddWarningToastMessage("Something went wrong.Please try again later!");
				return RedirectToAction("Index", "Home", new { area = "" });
			}
		}
		public async Task<IActionResult> Pending()
		{
			try
			{
				var users =await this.userService.GetPendingUsersAsync();
				var usersVM = users.GetVM();
				var reports = await this.reportService.GetPendingAsync();
				var reportsVM = reports.GetAdminReport();
				var pendingVM = new PendingVM { Users = usersVM ,Reports = reportsVM};
				return View(pendingVM);
			}
			catch
			{
				this.toastNotification.AddWarningToastMessage("Something went wrong.Please try again later!");
				return RedirectToAction("Index", "Home", new { area = "" });
			}
		}
		public async Task<IActionResult> Disable(Guid userId, string url)
		{
			try
			{

				var isDisabled = await this.userService.DisableAsync(userId);
				this.toastNotification.AddInfoToastMessage("User was disabled successfully!");

				return url switch {

					   "authors" => RedirectToAction("Authors", "Users"),
					   "clients" => RedirectToAction("Clients","Users"),
					   _ => RedirectToAction("Index","Home", new { area = ""})

				
					};

			}
			catch
			{
				this.toastNotification.AddWarningToastMessage("Something went wrong. Could not disable user!");
				return RedirectToAction("Authors", "Users");
			}
		}

		public async Task<IActionResult> Enable(Guid userId, string url)
		{
			try
			{
				var isEnabled = await this.userService.EnableAsync(userId);
				this.toastNotification.AddInfoToastMessage("User was enabled successfully!");

				return url switch
				{

					"authors" => RedirectToAction("Authors", "Users"),
					"clients" => RedirectToAction("Clients", "Users"),
					_ => RedirectToAction("Index", "Home", new { area = "" })


				};

			}
			catch
			{
				this.toastNotification.AddWarningToastMessage("Something went wrong. Could not enable user!");
				return RedirectToAction("Authors", "Users");
			}
		}
		public async Task<IActionResult> Delete(Guid userId, string url)
		{
			try
			{
				var isDeleted = await this.userService.DeleteAsync(userId);

				this.toastNotification.AddInfoToastMessage("User was deleted successfully!");

				return url switch
				{

					"authors" => RedirectToAction("Authors", "Users"),
					"clients" => RedirectToAction("Clients", "Users"),
					_ => RedirectToAction("Index", "Home", new { area = "" })


				};
			}
			catch
			{
				this.toastNotification.AddWarningToastMessage("Something went wrong. Could not delete user!");
				return RedirectToAction("Authors", "Users");
			}
		}
		public async Task<IActionResult> ApproveUser(Guid userId)
		{
			try
			{
				var isApproved =await this.userService.ApproveUserAsync(userId);
				this.toastNotification.AddInfoToastMessage("Account has been successfully approved!");

				return RedirectToAction("Pending", "Users");
			}
			catch
			{
				this.toastNotification.AddWarningToastMessage("Something went wrong. Could not approve user!");
				return RedirectToAction("Pending", "Users");
			}
		}

		public async Task<IActionResult> RefuseUser(Guid userId)
		{
			try
			{
				var isRefused = await this.userService.RefusePendingUserAsync(userId);
				this.toastNotification.AddInfoToastMessage("Account has been successfully refused!");

				return RedirectToAction("Pending", "Users");
			}
			catch
			{
				this.toastNotification.AddWarningToastMessage("Something went wrong. Could not refuse user approval!");
				return RedirectToAction("Pending", "Users");
			}
		}

		public  JsonResult Notifications()
		{
			var count = this.notificationService.GetCount();

			return Json(count);
		}

		public async Task<JsonResult> RemoveNotifications()
		{
			var isRemoved =await this.notificationService.Remove();

			return Json("Deleted");
		}

	}
}