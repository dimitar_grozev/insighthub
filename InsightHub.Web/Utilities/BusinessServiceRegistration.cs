﻿using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace InsightHub.API.Utilities
{
	public static class BusinessServiceRegistration
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITagService, TagService>();
            services.AddScoped<IIndustryService, IndustryService>();
            services.AddScoped<IReportService, ReportService>();
            services.AddScoped<INotificationService, NotificationService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddSingleton<IBlobService, BlobService>();
            services.AddScoped<IPhotoService, PhotoService>();

            return services;
        }
    }
}
