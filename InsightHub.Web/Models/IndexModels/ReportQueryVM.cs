﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace InsightHub.Web.Models.IndexModels
{
	public class ReportQueryVM
    {
        public string Industry { get; set; }
        public string SearchCriteria { get; set; }
        public List<SelectListItem> SearchDropdown { get; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "name", Text = "Title" },
            new SelectListItem { Value = "tag", Text = "Tag" },
            new SelectListItem { Value = "author", Text = "Author" },
            new SelectListItem { Value = "isFeatured", Text = "Featured" },

        };
        public string SearchString { get; set; }
        public string SortString { get; set; }
        public List<SelectListItem> SortDropdown { get; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "name_asc", Text = "Name: Ascending" },
            new SelectListItem { Value = "name_desc", Text = "Name: Descending" },
            new SelectListItem { Value = "downloads_asc", Text = "Downloads: Ascending" },
            new SelectListItem { Value = "downloads_desc", Text = "Downloads: Descending" },
            new SelectListItem { Value = "date_asc", Text = "Date: Ascending" },
            new SelectListItem { Value = "date_desc", Text = "Date: Descending" },
        };
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int ItemsPerPage { get => 5; }
        public ICollection<ReportPageVM> Reports {get; set;}
    }
}
