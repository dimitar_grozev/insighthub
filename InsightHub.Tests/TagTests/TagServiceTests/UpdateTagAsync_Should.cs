﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.TagTests.TagServiceTests
{
	[TestClass]
    public class UpdateTagAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_UpdateTagParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_UpdateTagParamsAreValid));

            var newName = "new name";

            var testTag = new Tag
            {
                Id = Guid.NewGuid(),
                Name = "TestTag"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(testTag);
                await arrangeContext.SaveChangesAsync();
            }

            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);
            var result = await sut.UpdateAsync(testTag.Id, newName);

            Assert.AreEqual(newName, result.Name);
        }

        [TestMethod]
        public async Task ThrowWnen_UpdateTagIdIsEmpty()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWnen_UpdateTagIdIsEmpty));

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);


            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAsync(Guid.Empty, "testName"));
        }

        [TestMethod]
        public async Task ThrowWnen_UpdateTagNameIsNull()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWnen_UpdateTagNameIsNull));

            string name = null;

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAsync(Guid.NewGuid(),name));
        }

        [TestMethod]
        public async Task ThrowWnen_UpdateTagNameIsTaken()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWnen_UpdateTagNameIsTaken));

            var testTag1 = new Tag
            {
                Id = Guid.NewGuid(),
                Name = "TestTag1"
            };

            var testTag2 = new Tag
            {
                Id = Guid.NewGuid(),
                Name = "TestTag2"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddRangeAsync(testTag1, testTag2);
                await arrangeContext.SaveChangesAsync();
            }
            // Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);

            await Assert.ThrowsExceptionAsync<InvalidOperationException>(() => sut.UpdateAsync(testTag1.Id, testTag2.Name));
        }

        [TestMethod]

        public async Task ThrowWhen_UpdateTagIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_UpdateTagIsDeleted));

            var name = "new name";

            var testTag = new Tag
            {
                Id = Guid.NewGuid(),
                Name = "TestTag",
                IsDeleted = true
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(testTag);
                await arrangeContext.SaveChangesAsync();
            }
            // Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAsync(testTag.Id, name));
        }

        [TestMethod]

        public async Task ThrowWhen_UpdateTagIdIsWrong()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_UpdateTagIdIsWrong));

            var name = "new name";

            var testTag = new Tag
            {
                Id = Guid.NewGuid(),
                Name = "TestTag",
                IsDeleted = true
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(testTag);
                await arrangeContext.SaveChangesAsync();
            }
            // Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAsync(Guid.NewGuid(), name));
        }
    }
}
