﻿using InsightHub.API.Controllers;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.TagTests.TagControllerTests.TagAPIControllerTests
{
	[TestClass]
	public class TagAPIGetById_Should
	{

		[TestMethod]
		public async Task ShouldReturnCorrectStatusCodeWhen_TagAPIGetByIdDataIsValid()
		{
			var mockTagService = new Mock<ITagService>();
			mockTagService.Setup(x => x.GetAsync(It.IsAny<Guid>())).Returns(Task.FromResult(new TagDTO()));

			var tagController = new TagsController(mockTagService.Object);

			var result = await tagController.Get(Guid.NewGuid());
			var statusCode = result as OkObjectResult;

			Assert.AreEqual(200, statusCode.StatusCode);
		}

		[TestMethod]
		public async Task CallsCorrectServicesWhen_TagAPIGetByIdDataIsValid()
		{
			var mockTagService = new Mock<ITagService>();
			mockTagService.Setup(x => x.GetAsync(It.IsAny<Guid>())).Returns(Task.FromResult(new TagDTO()));

			var tagController = new TagsController(mockTagService.Object);

			var result = await tagController.Get(Guid.NewGuid());

			mockTagService.Verify(x => x.GetAsync(It.IsAny<Guid>()), Times.Once);
		}
	}
}
