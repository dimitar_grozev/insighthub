﻿using InsightHub.API.Controllers;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace InsightHub.Tests.TagTests.TagControllerTests.TagAPIControllerTests
{
	[TestClass]
	public class TagAPIPost_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrectCodeWhen_TagAPIPostDataIsValid()
		{
			var mockTagService = new Mock<ITagService>();
			mockTagService.Setup(x => x.CreateAsync(It.IsAny<string>())).Returns(Task.FromResult(new TagDTO()));

			var tagController = new TagsController(mockTagService.Object);

			var result =await tagController.Post("random");

			var code = result as OkObjectResult;

			Assert.AreEqual(200, code.StatusCode);
		}

		[TestMethod]
		public async Task ShouldCallCorrectServices_TagAPIPostDataIsValid()
		{
			var mockTagService = new Mock<ITagService>();
			mockTagService.Setup(x => x.CreateAsync(It.IsAny<string>())).Returns(Task.FromResult(new TagDTO()));


			var tagController = new TagsController(mockTagService.Object);

			var result = await tagController.Post("random");

			mockTagService.Verify(x => x.CreateAsync(It.IsAny<string>()), Times.Once);
		}
	}
}
