﻿using InsightHub.API.Controllers;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.TagTests.TagControllerTests.TagAPIControllerTests
{
	[TestClass]
	public class TagAPIDelete_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrectCodeWhen_TagAPIDeleteDataIsValid()
		{
			var mockTagService = new Mock<ITagService>();
			mockTagService.Setup(x => x.DeleteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

			var tagController = new TagsController(mockTagService.Object);

			var result =await tagController.Delete(Guid.NewGuid());

			var code = result as OkObjectResult;

			Assert.AreEqual(200, code.StatusCode);
		}

		[TestMethod]
		public async Task CallsCorrectServicesWhen_TagAPIDeleteDataIsValid()
		{
			var mockTagService = new Mock<ITagService>();
			mockTagService.Setup(x => x.DeleteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

			var tagController = new TagsController(mockTagService.Object);

			var result = await tagController.Delete(Guid.NewGuid());

			mockTagService.Verify(x => x.DeleteAsync(It.IsAny<Guid>()), Times.Once);
		}
	}
}
