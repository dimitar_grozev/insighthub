﻿using InsightHub.API.Controllers;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.TagTests.TagControllerTests.TagAPIControllerTests
{
	[TestClass]
	public class TagAPIPut_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrectCodeWhen_TagAPIPutDataIsValid()
		{

			var mockTagService = new Mock<ITagService>();
			mockTagService.Setup(x => x.UpdateAsync(It.IsAny<Guid>(), It.IsAny<string>())).Returns(Task.FromResult(new TagDTO()));

			var tagController = new TagsController(mockTagService.Object);

			var result =await tagController.Put(Guid.NewGuid(), "random");

			var code = result as OkObjectResult;

			Assert.AreEqual(200, code.StatusCode);
		}

		[TestMethod]
		public async Task CallsCorrectServicesWhen_TagAPIPutDataIsInvalid()
		{

			var mockTagService = new Mock<ITagService>();
			mockTagService.Setup(x => x.UpdateAsync(It.IsAny<Guid>(), It.IsAny<string>())).Returns(Task.FromResult(new TagDTO()));

			var tagController = new TagsController(mockTagService.Object);

			var result = await tagController.Put(Guid.NewGuid(), "random");

			mockTagService.Verify(x => x.UpdateAsync(It.IsAny<Guid>(),It.IsAny<string>()), Times.Once);
		}
	}
}
