﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.NotificationTests.NotificationServiceTests
{
	[TestClass]
    public class Remove_Should
    {
        [TestMethod]
        public async Task ReturnTrueWhen_RemoveNotificationsDataIsValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnTrueWhen_RemoveNotificationsDataIsValid));

            var notification = new Notification
            {
                Id = Guid.NewGuid(),
                Description = "test description"
            };

            using(var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Notifications.AddAsync(notification);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new NotificationService(assertContext);

            var result = await sut.Remove();

            Assert.IsTrue(result);
            Assert.AreEqual(0, assertContext.Notifications.Count());
        }
    }
}
