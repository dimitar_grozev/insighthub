﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportServiceTests.SearchReport_Should
{
	[TestClass]
	public class GetReportsByPage_Should_WhenOnlyIndustryIsPassed
	{
		private async Task Seed(DbContextOptions<InsightHubContext> options)
		{
			var report = new Report
			{
				Id = Guid.NewGuid(),
				Name = "test report 3",
				NormalizedName = "TEST REPORT 3",
				AuthorId = Utils.author.Id,
				IndustryId = Utils.industry.Id,
				CreatedOn = DateTime.Parse("Jan 1, 1809"),
				IsApproved = true,
				IsFeatured = true,
				Downloads = new List<ClientReports>()
			};

			using var insertContext = new InsightHubContext(options);
			await insertContext.Reports.AddAsync(report);
			await insertContext.SaveChangesAsync();
		}
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_OnlyIndustryIsPassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_OnlyIndustryIsPassed));
			Utils.Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", null, null, null, 1, 1);

			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
		}


	}
}
