﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportServiceTests.SearchReport_Should
{
	[TestClass]
	public class GetReportsByPage_Should_WhenIndustryAndSearchCriteriaIsPassed
	{

		private async Task Seed(DbContextOptions<InsightHubContext> options)
		{
			var report = new Report
			{
				Id = Guid.NewGuid(),
				Name = "test report 3",
				NormalizedName = "TEST REPORT 3",
				AuthorId = Utils.author.Id,
				IndustryId = Utils.industry.Id,
				CreatedOn = DateTime.Parse("Jan 1, 1809"),
				IsApproved = true,
				IsFeatured = true,
				Downloads = new List<ClientReports>()
			};

			using var insertContext = new InsightHubContext(options);
			await insertContext.Reports.AddAsync(report);
			await insertContext.SaveChangesAsync();
		}
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustryAndSearchCriteriaNameArePassed_111()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustryAndSearchCriteriaNameArePassed_111));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "name", "test", null, 1, 1);
			var reports = result.Item1.ToList();
			Assert.AreEqual(2, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report", reports[0].Name);
		}
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustryAndSearchCriteriaNameArePassed_222()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustryAndSearchCriteriaNameArePassed_222));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "name", "test", null, 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(2, result.Item1.Count);
			Assert.AreEqual("test report", reports[0].Name);
		}
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustryAndSearchCriteriaNameArePassed_333()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustryAndSearchCriteriaNameArePassed_333));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "name", "test", null, 2, 2);
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(0, result.Item1.Count);
		}
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustryAndSearchCriteriIsFeaturedArePassed_1()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustryAndSearchCriteriIsFeaturedArePassed_1));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "isFeatured", "", null, 1, 1);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[0].Name);
		}
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustryAndSearchCriteriaIsFeaturedArePassed_2()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustryAndSearchCriteriaIsFeaturedArePassed_2));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "isFeatured", "", null, 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[0].Name);
		}
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustryAndSearchCriteriaIsFeaturedArePassed_3()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustryAndSearchCriteriaIsFeaturedArePassed_3));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "isFeatured", "", null, 2, 2);
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(0, result.Item1.Count);
		}
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustryAndSearchCriteriaAuthorArePassed_1()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustryAndSearchCriteriaAuthorArePassed_1));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "author", "John Smith", null, 1, 1);
			var reports = result.Item1.ToList();
			Assert.AreEqual(2, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report", reports[0].Name);
		}
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustryAndSearchCriteriaAuthorArePassed_2()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustryAndSearchCriteriaAuthorArePassed_2));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "author", "John Smith", null, 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(2, result.Item1.Count);
			Assert.AreEqual("test report", reports[0].Name);
		}
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustryAndSearchCriteriaAuthorArePassed_3()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustryAndSearchCriteriaAuthorArePassed_3));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "author", "John Smith", null, 2, 2);
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(0, result.Item1.Count);
		}
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustryAndSearchCriteriaТagArePassed_1()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustryAndSearchCriteriaТagArePassed_1));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "tag", "tag", null, 1, 1);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report", reports[0].Name);
		}
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustryAndSearchCriteriaTagArePassed_2()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustryAndSearchCriteriaTagArePassed_2));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "tag", "tag", null, 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report", reports[0].Name);
		}
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustryAndSearchCriteriaTagArePassed_3()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustryAndSearchCriteriaTagArePassed_3));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "tag", "tag", null, 2, 2);
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(0, result.Item1.Count);
		}
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustryAndSearchCriteriaNullArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustryAndSearchCriteriaNullArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", null, "tag", null, 1, 1);
			var reports = result.Item1.ToList();

			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report", reports[0].Name);
		}
	}
}
