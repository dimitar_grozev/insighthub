﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportServiceTests.SearchReport_Should
{
	[TestClass]
	public class GetReportsByPage_Should_WhenIndustrySearchCriteriaAndSortCriteriaIsPassed
	{
		private async Task Seed(DbContextOptions<InsightHubContext> options)
		{
			var report = new Report
			{
				Id = Guid.NewGuid(),
				Name = "test report 3",
				NormalizedName = "TEST REPORT 3",
				AuthorId = Utils.author.Id,
				IndustryId = Utils.industry.Id,
				CreatedOn = DateTime.Parse("Jan 1, 1809"),
				IsApproved = true,
				IsFeatured = true,
				Downloads = new List<ClientReports>()
			};

			using var insertContext = new InsightHubContext(options);
			await insertContext.Reports.AddAsync(report);
			await insertContext.SaveChangesAsync();
		}


		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaNameAndSortCriteriaNameAscArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaNameAndSortCriteriaNameAscArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "name", "test", "name_asc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(2, result.Item1.Count);
			Assert.AreEqual("test report", reports[0].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaNameAndSortCriteriaNameDescArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaNameAndSortCriteriaNameDescArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "name", "test", "name_asc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(2, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[1].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaNameAndSortCriteriaDownloadAscArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaNameAndSortCriteriaDownloadAscArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "name", "test", "downloads_asc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(2, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[0].Name);
			Assert.AreEqual("test report", reports[1].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaNameAndSortCriteriaDownloadDescArePassed1()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaNameAndSortCriteriaDownloadDescArePassed1));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "name", "test", "downloads_desc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(2, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[1].Name);
			Assert.AreEqual("test report", reports[0].Name);
		}


		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaNameAndSortCriteriaCreatedOnAscArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaNameAndSortCriteriaCreatedOnAscArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "name", "test", "date_asc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(2, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[0].Name);
			Assert.AreEqual("test report", reports[1].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaNameAndSortCriteriaNullArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaNameAndSortCriteriaNullArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "name", "test", null , 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(2, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[1].Name);
			Assert.AreEqual("test report", reports[0].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaAuthorAndSortCriteriaNameAscArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaAuthorAndSortCriteriaNameAscArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "author", "John Smith", "name_asc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(2, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[1].Name);
			Assert.AreEqual("test report", reports[0].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaAuthorAndSortCriteriaNameDescArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaAuthorAndSortCriteriaNameDescArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "author", "John Smith", "name_desc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(2, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[0].Name);
			Assert.AreEqual("test report", reports[1].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaAuthorAndSortCriteriaDownloadAscArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaAuthorAndSortCriteriaDownloadAscArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "author", "John Smith", "downloads_asc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(2, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[0].Name);
			Assert.AreEqual("test report", reports[1].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaAuthorAndSortCriteriaDownloadDescArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaAuthorAndSortCriteriaDownloadDescArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "author", "John Smith", "downloads_desc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(2, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[1].Name);
			Assert.AreEqual("test report", reports[0].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaAuthorAndSortCriteriaCreatedOnAscArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaAuthorAndSortCriteriaCreatedOnAscArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "author", "John Smith", "date_asc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(2, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[0].Name);
			Assert.AreEqual("test report", reports[1].Name);
		}


		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaAuthorAndSortCriteriaNullArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaAuthorAndSortCriteriaNullArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "author", "John Smith", null, 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(2, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[1].Name);
			Assert.AreEqual("test report", reports[0].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaIsFeaturedAndSortCriteriaNameAscArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaIsFeaturedAndSortCriteriaNameAscArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "isFeatured", "", "name_asc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[0].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaIsFeaturedAndSortCriteriaNameDescArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaIsFeaturedAndSortCriteriaNameDescArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "isFeatured", "", "name_desc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[0].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaIsFeaturedAndSortCriteriaDownloadAscArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaIsFeaturedAndSortCriteriaDownloadAscArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "isFeatured", "", "downloads_asc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[0].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaIsFeaturedAndSortCriteriaDownloadDescArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaIsFeaturedAndSortCriteriaDownloadDescArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "isFeatured", "", "downloads_desc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[0].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaIsFeaturedAndSortCriteriaCreatedOnAscArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaIsFeaturedAndSortCriteriaCreatedOnAscArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "isFeatured", "", "date_asc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[0].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaIsFeaturedAndSortCriteriaNullArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaIsFeaturedAndSortCriteriaNullArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "isFeatured", "", null, 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report 3", reports[0].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaTagAndSortCriteriaNameAscArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaTagAndSortCriteriaNameAscArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "tag", "tag", "name_asc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report", reports[0].Name);
		}
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaTagAndSortCriteriaNameDescArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaTagAndSortCriteriaNameDescArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "tag", "tag", "name_desc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report", reports[0].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaTagAndSortCriteriaDownloadsAscArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaTagAndSortCriteriaDownloadsAscArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "tag", "tag", "downloads_asc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report", reports[0].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaTagAndSortCriteriaDownloadsDescArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaTagAndSortCriteriaDownloadsDescArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "tag", "tag", "downloads_desc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report", reports[0].Name);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_IndustrySearchCriteriaTagAndSortCriteriaCreatedOnAscArePassed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_IndustrySearchCriteriaTagAndSortCriteriaCreatedOnAscArePassed));
			Utils.Seed(options);
			await Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetReportByPage("test industry", "tag", "tag", "date_asc", 1, 2);
			var reports = result.Item1.ToList();
			Assert.AreEqual(1, result.Item2);
			Assert.AreEqual(1, result.Item1.Count);
			Assert.AreEqual("test report", reports[0].Name);
		}

		
	}
}
