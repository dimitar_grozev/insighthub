﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportServiceTests
{
	[TestClass]
	public class GetAuthorReportsAsync_Should
	{
		[TestMethod]
		public async Task ShouldThrowWhen_GetAuthorReportsIdIsEmpty()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_GetAuthorReportsIdIsEmpty));

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAuthorReportsAsync(Guid.Empty));
			
		}

		[TestMethod]
		public async Task ShouldThrowWhen_GetAuthorReportsIdIsInvalid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_GetAuthorReportsIdIsInvalid));

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAuthorReportsAsync(Guid.NewGuid()));

		}


		[TestMethod]
		public async Task ShouldReturnCorrect_GetAuthorReportsIdIsValid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrect_GetAuthorReportsIdIsValid));
			Utils.Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetAuthorReportsAsync(Utils.user.Id);

			Assert.AreEqual(1, result.Count());
		}
	}
}
