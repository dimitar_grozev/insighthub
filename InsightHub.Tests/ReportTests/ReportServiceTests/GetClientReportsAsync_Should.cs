﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportServiceTests
{
	[TestClass]
	public class GetClientReportsAsync_Should
	{
		[TestMethod]
		public async Task ShouldThrowWhen_GetClientReportsUserIdIsEmpty()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_GetClientReportsUserIdIsEmpty));
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetClientReportsAsync(Guid.Empty));
		}

		[TestMethod]
		public async Task ShouldThrowWhen_GetClientReportsUserIdIsInvalid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_GetClientReportsUserIdIsInvalid));
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetClientReportsAsync(Guid.NewGuid()));
		}

		[TestMethod]
		public async Task ShouldReturnCorrect_GetClientReportsUserIdIsValid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrect_GetClientReportsUserIdIsValid));
			Utils.Seed(options);

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result =await sut.GetClientReportsAsync(Utils.user2.Id);

			Assert.AreEqual(1, result.Count);
		}
	}
}
