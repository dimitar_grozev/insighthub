﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportServiceTests
{
	[TestClass]
	public class UpdateAsync_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_UpdateReportParamsAreValid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_UpdateReportParamsAreValid));
			Utils.Seed(options);

			var reportDto = new ReportDTO
			{
				Id = Utils.report.Id,
				Name = "New name of report",
				AuthorId = Utils.user.Id,
				IndustryId = Utils.industry.Id,
				Tags = new List<string> { "happy"}
			};

			var mockTagService = new Mock<ITagService>();
			mockTagService.Setup(t => t.CreateAsync(It.IsAny<string>()))
							.ReturnsAsync(new TagDTO { Id = Guid.NewGuid(), Name = "random tag" });

			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService.Object, mockNotificationService);

			var result = await sut.UpdateAsync(reportDto);

			Assert.AreEqual(reportDto.Name, result.Name);
		}

		[TestMethod]
		public async Task ShouldThrowWhen_UpdateReportIdIsEmptry()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_UpdateReportIdIsEmptry));
			Utils.Seed(options);

			var reportDto = new ReportDTO
			{
				Id = Guid.Empty,

			};

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;
			//Act,Assert

			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAsync(reportDto));
		}

	

		[TestMethod]
		public async Task ShouldThrowWhen_UpdateReportIndustryIdIsInvalid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_UpdateReportIndustryIdIsInvalid));
			Utils.Seed(options);

			var reportDto = new ReportDTO
			{
				Id = Guid.NewGuid(),
				Name = "Test name",
				IndustryId = Guid.NewGuid()
			};

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;
			//Act,Assert

			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			await Assert.ThrowsExceptionAsync<InvalidOperationException>(() => sut.UpdateAsync(reportDto));
		}

		[TestMethod]
		public async Task ShouldThrowWhen_UpdateReportIndustryIsDeletedIsInvalid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_UpdateReportIndustryIsDeletedIsInvalid));
			Utils.Seed(options);
			var industry = new Industry
			{
				Id = Guid.NewGuid(),
				Name = "Random Name",
				IsDeleted = true
			};
			var reportDto = new ReportDTO
			{
				Id = Guid.NewGuid(),
				Name = "Test name",
				IndustryId = industry.Id
			};
			using(var arrangeContext = new InsightHubContext(options))
			{
				await arrangeContext.Industries.AddAsync(industry);
				await arrangeContext.SaveChangesAsync();
			}
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;
			//Act,Assert

			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			await Assert.ThrowsExceptionAsync<InvalidOperationException>(() => sut.UpdateAsync(reportDto));
		}

		[TestMethod]
		public async Task ShouldThrowWhen_UpdateReportReportIdInvalid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_UpdateReportReportIdInvalid));
			Utils.Seed(options);
	
			var reportDto = new ReportDTO
			{
				Id = Guid.NewGuid(),
				Name = "Test name",
				IndustryId =Utils.industry.Id
			};
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;
			//Act,Assert

			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAsync(reportDto));
		}

		[TestMethod]
		public async Task ShouldThrowWhen_UpdateReportReportIsDeletedInvalid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_UpdateReportReportIsDeletedInvalid));
			Utils.Seed(options);

			var reportDto = new ReportDTO
			{
				Id = Utils.report3.Id,
				Name = "Test name",
				IndustryId = Utils.industry.Id
			};
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;
			//Act,Assert

			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAsync(reportDto));
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_UpdateReportNewTagsAreAdded()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_UpdateReportNewTagsAreAdded));
			Utils.Seed(options);

			var reportDto = new ReportDTO
			{
				Id = Utils.report.Id,
				Name = "Test name",
				IndustryId = Utils.industry.Id,
				Tags = new List<string> { "happy", "sad" }
			};
			var mockNotificationService = new Mock<INotificationService>().Object;
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var tagService = new TagService(assertContext);

			var sut = new ReportService(assertContext, tagService, mockNotificationService);

			var result = await sut.UpdateAsync(reportDto);

			Assert.AreEqual(4, assertContext.ReportTags.Count());
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_UpdateReportNewTagsExistingTagsAreUsed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_UpdateReportNewTagsExistingTagsAreUsed));
			Utils.Seed(options);

			var reportDto = new ReportDTO
			{
				Id = Utils.report.Id,
				Name = "Test name",
				IndustryId = Utils.industry.Id,
				Tags = new List<string> { "Tag"}
			};
			var mockNotificationService = new Mock<INotificationService>().Object;
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var tagService = new TagService(assertContext);

			var sut = new ReportService(assertContext, tagService, mockNotificationService);

			var result = await sut.UpdateAsync(reportDto);

			Assert.AreEqual(3, assertContext.ReportTags.Count());
		}
	}
}
