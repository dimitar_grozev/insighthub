﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportServiceTests
{
	[TestClass]
	public class GetPendingAsync_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_PendingReportsAreCorrect1()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_PendingReportsAreCorrect1));
			Utils.Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var result =await sut.GetPendingAsync();
			var list = result.ToList();
			Assert.AreEqual(1, list.Count);
			Assert.IsInstanceOfType(list[0], typeof(ReportDTO));
			Assert.AreEqual(Utils.report2.Id, list[0].Id);
		}
	}
}
