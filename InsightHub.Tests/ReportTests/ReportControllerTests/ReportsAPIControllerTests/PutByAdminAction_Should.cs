﻿using InsightHub.API.Controllers;
using InsightHub.Data;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Services.DTOs.API_DTO_s;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportControllerTests.ReportsAPIControllerTests
{
	[TestClass]
    public class PutByAdminAction_Should
    {
        [TestMethod]
        public async Task ReturnOkWhen_UpdateReportDataIsCorrect()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnOkWhen_UpdateReportDataIsCorrect));
            var mockReportService = new Mock<IReportService>();
            mockReportService.Setup(x => x.UpdateAsync(It.IsAny<ReportDTO>())).Returns(Task.FromResult(new ReportDTO()));

            var mockBlobService = new Mock<IBlobService>().Object;
            var mockPhotoService = new Mock<IPhotoService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new ReportsController(mockReportService.Object, mockBlobService, mockPhotoService);

            var result = await sut.Put(Guid.NewGuid(),new UpdateAdminReportApiDTO()) as OkObjectResult;

            Assert.AreEqual(200, result.StatusCode);
        }
        [TestMethod]
        public async Task CallRightServiceWhen_UpdateReportDataIsCorrect()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(CallRightServiceWhen_UpdateReportDataIsCorrect));
            var mockReportService = new Mock<IReportService>();
            mockReportService.Setup(x => x.UpdateAsync(It.IsAny<ReportDTO>())).Returns(Task.FromResult(new ReportDTO()));

            var mockBlobService = new Mock<IBlobService>().Object;
            var mockPhotoService = new Mock<IPhotoService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new ReportsController(mockReportService.Object, mockBlobService, mockPhotoService);

            var result = await sut.Put(Guid.NewGuid(), new UpdateAdminReportApiDTO()) as OkResult;

            mockReportService.Verify(x => x.UpdateAsync(It.IsAny<ReportDTO>()), Times.Once);
        }
    }
}
