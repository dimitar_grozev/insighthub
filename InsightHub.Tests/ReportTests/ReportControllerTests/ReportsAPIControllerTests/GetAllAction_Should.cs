﻿using InsightHub.API.Controllers;
using InsightHub.Data;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportControllerTests.ReportsAPIControllerTests
{
	[TestClass]
    public class GetAllAction_Should
    {
        [TestMethod]
        public async Task ReturnOkWhen_ReportsDataIsCorrect()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnOkWhen_ReportsDataIsCorrect));
            ICollection<ReportDTO> collection = new List<ReportDTO>();
            var mockReportService = new Mock<IReportService>();
            mockReportService.Setup(x => x.GetAsync()).Returns(Task.FromResult(collection));

            var mockBlobService = new Mock<IBlobService>().Object;
            var mockPhotoService = new Mock<IPhotoService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new ReportsController(mockReportService.Object, mockBlobService, mockPhotoService);

            var result = await sut.Get() as OkObjectResult;

            Assert.AreEqual(200, result.StatusCode);
        }

        [TestMethod]
        public async Task CallRightServiceWhen_ReportsDataIsCorrect()
        {
            var options = Utils.GetOptions(nameof(CallRightServiceWhen_ReportsDataIsCorrect));
            ICollection<ReportDTO> collection = new List<ReportDTO>();
            var mockReportService = new Mock<IReportService>();
            mockReportService.Setup(x => x.GetAsync()).Returns(Task.FromResult(collection));

            var mockBlobService = new Mock<IBlobService>().Object;
            var mockPhotoService = new Mock<IPhotoService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new ReportsController(mockReportService.Object, mockBlobService, mockPhotoService);

            var result = await sut.Get() as OkObjectResult;

            mockReportService.Verify(x => x.GetAsync(), Times.Once);
        }
    }
}
