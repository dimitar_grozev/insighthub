﻿using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Web.Areas.Author.Controllers;
using InsightHub.Web.Areas.Author.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NToastNotify;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportControllerTests.ReportsWebControllerTests.AuthorAreaReportTests
{
	[TestClass]
	public class AuthorReportsEdit_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrectViewWhen_EditReportDataIsValid()
		{
			//Arrange
			ICollection<IndustryDTO> list = new List<IndustryDTO>();
			var mockReportService = new Mock<IReportService>();
			mockReportService.Setup(x => x.GetAsync(It.IsAny<Guid>())).Returns(Task.FromResult(new ReportDTO() { Tags= new List<string>()}));
			var mockIndustryService = new Mock<IIndustryService>();
			mockIndustryService.Setup(x => x.GetAsync()).Returns(Task.FromResult(list));
			var mockToastNotification = new Mock<IToastNotification>().Object;
			var mockPhotoService = new Mock<IPhotoService>();
			var mockBlobService = new Mock<IBlobService>();

			//Act,Assert
			var reportController = new ReportsController(mockReportService.Object, mockIndustryService.Object, mockBlobService.Object, mockPhotoService.Object,mockToastNotification);

			var result = await reportController.Edit(Guid.NewGuid()) as ViewResult;
			var model = result.ViewData.Model;

			Assert.IsInstanceOfType(model, typeof(UpdateReportVM));
		}

		[TestMethod]
		public async Task CallsCorrectServicesWhen_EditReportDataIsValid()
		{
			//Arrange
			ICollection<IndustryDTO> list = new List<IndustryDTO>();
			var mockReportService = new Mock<IReportService>();
			mockReportService.Setup(x => x.GetAsync(It.IsAny<Guid>())).Returns(Task.FromResult(new ReportDTO() { Tags = new List<string>() }));
			var mockIndustryService = new Mock<IIndustryService>();
			mockIndustryService.Setup(x => x.GetAsync()).Returns(Task.FromResult(list));
			var mockToastNotification = new Mock<IToastNotification>().Object;

			var mockPhotoService = new Mock<IPhotoService>();
			var mockBlobService = new Mock<IBlobService>();

			//Act,Assert
			var reportController = new ReportsController(mockReportService.Object, mockIndustryService.Object, mockBlobService.Object, mockPhotoService.Object,mockToastNotification);

			var result = await reportController.Edit(Guid.NewGuid());
			mockReportService.Verify(x => x.GetAsync(It.IsAny<Guid>()), Times.Once);
			mockIndustryService.Verify(x => x.GetAsync(), Times.Once);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectViewWhen_EditPostReportDataIsValid1()
		{
			//Arrange
			ICollection<IndustryDTO> list = new List<IndustryDTO>();
			var mockReportService = new Mock<IReportService>();
			mockReportService.Setup(x => x.GetAsync(It.IsAny<Guid>())).Returns(Task.FromResult(new ReportDTO() { Tags = new List<string>() }));
			var mockIndustryService = new Mock<IIndustryService>();
			mockIndustryService.Setup(x => x.GetAsync()).Returns(Task.FromResult(list));
			var mockToastNotification = new Mock<IToastNotification>().Object;

			var mockPhotoService = new Mock<IPhotoService>();
			var mockBlobService = new Mock<IBlobService>();
			UpdateReportVM report = new UpdateReportVM();
			//Act,Assert
			var reportController = new ReportsController(mockReportService.Object, mockIndustryService.Object, mockBlobService.Object, mockPhotoService.Object,mockToastNotification);

			var result =(RedirectToPageResult) await reportController.Edit(report,"author");

			Assert.AreEqual("/Account/Manage/Reports", result.PageName);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectViewWhen_EditPostReportDataIsValid2()
		{
			//Arrange
			ICollection<IndustryDTO> list = new List<IndustryDTO>();
			var mockReportService = new Mock<IReportService>();
			mockReportService.Setup(x => x.GetAsync(It.IsAny<Guid>())).Returns(Task.FromResult(new ReportDTO() { Tags = new List<string>() }));
			var mockIndustryService = new Mock<IIndustryService>();
			mockIndustryService.Setup(x => x.GetAsync()).Returns(Task.FromResult(list));
			var mockToastNotification = new Mock<IToastNotification>().Object;

			var mockPhotoService = new Mock<IPhotoService>();
			var mockBlobService = new Mock<IBlobService>();
			UpdateReportVM report = new UpdateReportVM();
			//Act,Assert
			var reportController = new ReportsController(mockReportService.Object, mockIndustryService.Object, mockBlobService.Object, mockPhotoService.Object,mockToastNotification);

			var result = (RedirectToActionResult)await reportController.Edit(report, "admin");

			Assert.AreEqual("Index", result.ActionName);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectViewWhen_EditPostReportDataIsInvalid()
		{
			//Arrange
			ICollection<IndustryDTO> list = new List<IndustryDTO>();
			var mockReportService = new Mock<IReportService>();
			mockReportService.Setup(x => x.UpdateAsync(It.IsAny<ReportDTO>())).Throws(new ArgumentNullException());
			var mockIndustryService = new Mock<IIndustryService>();
			mockIndustryService.Setup(x => x.GetAsync()).Throws(new ArgumentNullException());
			var mockToastNotification = new Mock<IToastNotification>().Object;

			var mockPhotoService = new Mock<IPhotoService>();
			var mockBlobService = new Mock<IBlobService>();
			UpdateReportVM report = new UpdateReportVM();
			//Act,Assert
			var reportController = new ReportsController(mockReportService.Object, mockIndustryService.Object, mockBlobService.Object, mockPhotoService.Object,mockToastNotification);

			var result = (RedirectToActionResult)await reportController.Edit(report, "admin");

			Assert.AreEqual("Edit", result.ActionName);
		}
	}
}
