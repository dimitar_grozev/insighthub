﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
	public class UpdateAuthorProfileAsync_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_UpdateAuthorProfileParamsAreValid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_UpdateAuthorProfileParamsAreValid));
			var author = new Author { Id = Guid.NewGuid() };
			var mockEmailService = new Mock<IEmailService>().Object;

			using(var arrangeContext = new InsightHubContext(options))
			{
				await arrangeContext.Authors.AddAsync(author);
				await arrangeContext.SaveChangesAsync();
			}
			//Act,Assert
			using var assertContext = new InsightHubContext(options);

			var sut = new UserService(assertContext, mockEmailService);

			var result =await sut.UpdateAuthorProfileAsync(author.Id, "randompic.png","test bio");

			Assert.IsTrue(result);
			Assert.AreEqual("randompic.png", assertContext.Authors.Find(author.Id).ImagePath);
		}

		[TestMethod]
		public async Task ShouldThrowWhen_UpdateAuthorBioIsInvalid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_UpdateAuthorBioIsInvalid));
			Utils.Seed(options);
			var mockEmailService = new Mock<IEmailService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);

			var sut = new UserService(assertContext, mockEmailService);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAuthorProfileAsync(Utils.author.Id, null,null));
		}

		[TestMethod]
		public async Task ShouldThrowWhen_UpdateAuthorProfileAuthorIdIsInvalid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_UpdateAuthorProfileAuthorIdIsInvalid));
			Utils.Seed(options);
			var mockEmailService = new Mock<IEmailService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);

			var sut = new UserService(assertContext, mockEmailService);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAuthorProfileAsync(Guid.NewGuid(), "picture.png","test bio"));
		}


		[TestMethod]
		public async Task ShouldThrowWhen_UpdateAuthorProfileAuthorIdIsEmpty()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_UpdateAuthorProfileAuthorIdIsEmpty));
			Utils.Seed(options);
			var mockEmailService = new Mock<IEmailService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);

			var sut = new UserService(assertContext, mockEmailService);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAuthorProfileAsync(Guid.Empty, "picture.png","test bio"));
		}
	}
}
