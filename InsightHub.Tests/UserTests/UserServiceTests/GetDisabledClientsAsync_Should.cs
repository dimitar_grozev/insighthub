﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class GetDisabledClientsAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_AllClientsForDisabledAreActive()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_AllClientsForDisabledAreActive));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetDisabledClientsAsync();

            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_OneClientForDisabledIsNotApproved()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_OneClientForDisabledIsNotApproved));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user2.IsApproved = false;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user2);
                await testContext.SaveChangesAsync();
            }

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetDisabledClientsAsync();

            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_OneCientForDisabledIsDisabled()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_OneCientForDisabledIsDisabled));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user2.IsDisabled = true;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user2);
                await testContext.SaveChangesAsync();
            }
            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetDisabledClientsAsync();
            var resultList = result.ToList();

            Assert.AreEqual(1, result.Count);
            Assert.IsInstanceOfType(result, typeof(ICollection<UserDTO>));
            Assert.AreEqual(Utils.user2.Id, resultList[0].Id);
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_OneClientForDisabledIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_OneClientForDisabledIsDeleted));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user2.IsDeleted = true;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user2);
                await testContext.SaveChangesAsync();
            }
            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetDisabledClientsAsync();
            var resultList = result.ToList();

            Assert.AreEqual(0, result.Count);
        }
    }
}
