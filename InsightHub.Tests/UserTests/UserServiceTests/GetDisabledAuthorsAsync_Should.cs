﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class GetDisabledAuthorsAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_AllAuthorsForDisabledAreActive()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_AllAuthorsForDisabledAreActive));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetDisabledAuthorsAsync();
            var resultList = result.ToList();

            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_OneForDisabledIsNotApproved()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_OneForDisabledIsNotApproved));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user1.IsApproved = false;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user1);
                await testContext.SaveChangesAsync();
            }

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetDisabledAuthorsAsync();
            var resultList = result.ToList();

            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_OneForDisabledIsDisabled()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_OneForDisabledIsDisabled));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user1.IsDisabled = true;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user1);
                await testContext.SaveChangesAsync();
            }
            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetDisabledAuthorsAsync();
            var resultList = result.ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(Utils.user1.Id, resultList[0].Id);
            Assert.IsInstanceOfType(result, typeof(ICollection<UserDTO>));
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_OneForDisabledIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_OneForDisabledIsDeleted));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user1.IsDeleted = true;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user1);
                await testContext.SaveChangesAsync();
            }
            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetDisabledAuthorsAsync();
            var resultList = result.ToList();

            Assert.AreEqual(0, result.Count);
        }
    }
}
