﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class IsSubscribed_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_IsSubParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_IsSubParamsAreValid));
            var mockEmailService = new Mock<IEmailService>().Object;

            Utils.Seed(options);

            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.IsSubscribed(Utils.user2.Id, Utils.industry.Name);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public async Task ThrowWhen_IsSubUserIdIsEmpty()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_IsSubUserIdIsEmpty));
            var mockEmailService = new Mock<IEmailService>().Object;
            var industryName = "test industry";

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.IsSubscribed(Guid.Empty, industryName));
        }

        [TestMethod]
        public async Task ThrowWhen_IsSubUserDoenNotExist()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_IsSubUserDoenNotExist));
            var mockEmailService = new Mock<IEmailService>().Object;
            var industryName = "test industry";

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.IsSubscribed(Guid.NewGuid(), industryName));
        }

        [TestMethod]
        public async Task ThrowWhen_IsSubIndustryIsNull()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_IsSubUserIdIsEmpty));
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.Seed(options);

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.IsSubscribed(Utils.user2.Id, null));
        }

        [TestMethod]
        public async Task ThrowWhen_IsSubIndustryDoenNotExist()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_IsSubUserDoenNotExist));
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.Seed(options);

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.IsSubscribed(Utils.user2.Id, "no name"));
        }
    }
}
