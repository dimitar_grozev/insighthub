﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class GetUsersAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrecWhen_GetUsersParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrecWhen_GetUsersParamsAreValid));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetUsersAsync();

            Assert.AreEqual(3, result.Count);
            Assert.IsInstanceOfType(result, typeof(ICollection<UserDTO>));
        }

        [TestMethod]
        public async Task ThrowWhen_GetUsersCollectionIsEmpty()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_GetUsersCollectionIsEmpty));
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.GetUsersAsync());
        }
    }
}
