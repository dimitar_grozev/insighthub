﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
	public class GetAuthorIdAsync_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_GetAuthorIdIsCorrect()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_GetAuthorIdIsCorrect));
			Utils.Seed(options);

			var mockEmailService = new Mock<IEmailService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new UserService(assertContext, mockEmailService);

			var result =await sut.GetAuthorIdAsync(Utils.user.Id);

			Assert.AreEqual(Utils.author.Id, result);
		}

		[TestMethod]
		public async Task ShouldThrowWhen_GetAuthorIdIsEmpty()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_GetAuthorIdIsEmpty));
			Utils.Seed(options);

			var mockEmailService = new Mock<IEmailService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new UserService(assertContext, mockEmailService);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAuthorIdAsync(Guid.Empty));
		}

		[TestMethod]
		public async Task ShouldThrowWhen_GetAuthorIdIsInvalid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_GetAuthorIdIsInvalid));
			Utils.Seed(options);

			var mockEmailService = new Mock<IEmailService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new UserService(assertContext, mockEmailService);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAuthorIdAsync(Guid.NewGuid()));
		}
	}
}
