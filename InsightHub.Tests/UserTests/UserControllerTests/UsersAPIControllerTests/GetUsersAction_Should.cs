﻿using InsightHub.API.Controllers;
using InsightHub.Data;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserControllerTests.UsersAPIControllerTests
{
	[TestClass]
    public class GetUsersAction_Should
    {
        [TestMethod]
        public async Task ReturnOkWhen_UsersDataIsCorrect()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnOkWhen_UsersDataIsCorrect));
            ICollection<UserDTO> collection = new List<UserDTO>();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.GetUsersAsync()).Returns(Task.FromResult(collection));

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UsersController(mockUserService.Object);

            var result = await sut.Get() as OkObjectResult;

            Assert.AreEqual(200, result.StatusCode);
        }

        [TestMethod]
        public async Task CallRightServiceWhen_UsersDataIsCorrect()
        {
            var options = Utils.GetOptions(nameof(CallRightServiceWhen_UsersDataIsCorrect));
            ICollection<UserDTO> collection = new List<UserDTO>();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.GetUsersAsync()).Returns(Task.FromResult(collection));

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UsersController(mockUserService.Object);

            var result = await sut.Get() as OkObjectResult;

            mockUserService.Verify(x => x.GetUsersAsync(), Times.Once);
        }
    }
}
