﻿using InsightHub.Services.Contracts;
using InsightHub.Web.Areas.Admin.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NToastNotify;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserControllerTests.UsersWebControllerTests.AdminAreaUsersTests
{
	[TestClass]
    public class DisableAction_Should
    {
        [TestMethod]
        public async Task CallCorrectServiceWhen_DisableDataIsCorrect()
        {
            var userId = Guid.NewGuid();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.DisableAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.Disable(userId, "authors");

            mockUserService.Verify(x => x.DisableAsync(userId), Times.Once);
        }

        [TestMethod]
        public async Task RedirectWhen_DisableComingFromAuthors()
        {
            var userId = Guid.NewGuid();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.DisableAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.Disable(userId, "authors");

            Assert.AreEqual("Authors", result.ActionName);
        }

        [TestMethod]
        public async Task RedirectWhen_DisableComingFromClients()
        {
            var userId = Guid.NewGuid();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.DisableAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.Disable(userId, "clients");

            Assert.AreEqual("Clients", result.ActionName);
        }

        [TestMethod]
        public async Task RedirectWhen_DisableComingFromElse()
        {
            var userId = Guid.NewGuid();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.DisableAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.Disable(userId, "default");

            Assert.AreEqual("Index", result.ActionName);
        }

        [TestMethod]
        public async Task RedirectWhen_DisableServiceThrows()
        {
            var userId = Guid.NewGuid();
            var exception = new ArgumentNullException();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.DisableAsync(It.IsAny<Guid>())).Throws(exception);

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.Disable(userId, "default");

            Assert.AreEqual("Authors", result.ActionName);
        }
    }
}
