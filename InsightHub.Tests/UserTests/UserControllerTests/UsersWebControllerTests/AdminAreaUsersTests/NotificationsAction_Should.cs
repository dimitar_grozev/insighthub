﻿using InsightHub.Services.Contracts;
using InsightHub.Web.Areas.Admin.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NToastNotify;

namespace InsightHub.Tests.UserTests.UserControllerTests.UsersWebControllerTests.AdminAreaUsersTests
{
	[TestClass]
    public class NotificationsAction_Should
    {
        [TestMethod]
        public void ReturnCorrectWhen_GetRightNotifications()
        {
            var mockUserService = new Mock<IUserService>().Object;
            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = controller.Notifications();

            Assert.IsInstanceOfType(result, typeof(JsonResult));
        }
    }
}
