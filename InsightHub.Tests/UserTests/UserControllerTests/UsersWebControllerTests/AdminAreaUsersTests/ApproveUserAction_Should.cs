﻿using InsightHub.Services.Contracts;
using InsightHub.Web.Areas.Admin.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NToastNotify;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserControllerTests.UsersWebControllerTests.AdminAreaUsersTests
{
	[TestClass]
    public class ApproveUserAction_Should
    {
        [TestMethod]
        public async Task CallCorrectServiceWhen_ApproveUserDataIsCorrect()
        {
            var userId = Guid.NewGuid();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.ApproveUserAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.ApproveUser(userId);

            mockUserService.Verify(x => x.ApproveUserAsync(userId), Times.Once);
        }

        [TestMethod]
        public async Task RedirectWhen_ApproveDataIsCorrect()
        {
            var userId = Guid.NewGuid();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.ApproveUserAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.ApproveUser(userId);

            Assert.AreEqual("Pending", result.ActionName);
        }

        [TestMethod]
        public async Task RedirectWhen_ApproveServiceThrows()
        {
            var userId = Guid.NewGuid();
            var exception = new ArgumentNullException();

            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.ApproveUserAsync(It.IsAny<Guid>())).Throws(exception);

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.ApproveUser(userId);

            Assert.AreEqual("Pending", result.ActionName);
        }
    }
}
