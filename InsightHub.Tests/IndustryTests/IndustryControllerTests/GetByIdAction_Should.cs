﻿using InsightHub.API.Controllers;
using InsightHub.Data;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.IndustryTests.IndustryControllerTests
{
	[TestClass]
    public class GetByIdAction_Should
    {
        [TestMethod]
        public async Task ReturnOkWhen_IndustryByIdDataIsCorrect()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnOkWhen_IndustryByIdDataIsCorrect));

            var mockIndustryService = new Mock<IIndustryService>();
            mockIndustryService.Setup(x => x.GetAsync(It.IsAny<Guid>())).Returns(Task.FromResult(new IndustryDTO()));

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new IndustriesController(mockIndustryService.Object);

            var result = await sut.Get(Guid.NewGuid()) as OkObjectResult;

            Assert.AreEqual(200, result.StatusCode);
        }
    }
}
