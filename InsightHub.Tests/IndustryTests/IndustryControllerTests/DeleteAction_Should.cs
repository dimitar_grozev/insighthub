﻿using InsightHub.API.Controllers;
using InsightHub.Data;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.IndustryTests.IndustryControllerTests
{
	[TestClass]
    public class DeleteAction_Should
    {
        [TestMethod]
        public async Task ReturnOkWhen_DeleteIndustryDataIsCorrect()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnOkWhen_DeleteIndustryDataIsCorrect));
            var mockIndustryService = new Mock<IIndustryService>();
            mockIndustryService.Setup(x => x.DeleteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new IndustriesController(mockIndustryService.Object);

            var result = await sut.Delete(Guid.NewGuid()) as OkObjectResult;

            Assert.AreEqual(200, result.StatusCode);
        }
    }
}
