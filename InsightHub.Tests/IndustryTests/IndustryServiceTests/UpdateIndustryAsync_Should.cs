﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.IndustryTests.IndustryServiceTests
{
	[TestClass]
	public class UpdateIndustryAsync_Should
	{
		[TestMethod]
		public async Task ReturnCorrenct_UpdatedIndustryWhenParamsAreValid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ReturnCorrenct_UpdatedIndustryWhenParamsAreValid));
			var industry = new Industry()
			{
				Id = Guid.NewGuid(),
				Name = "Industry"
			};
			string newName = "New Name";
			using (var arrangeContext = new InsightHubContext(options))
			{
				await arrangeContext.AddAsync(industry);
				await arrangeContext.SaveChangesAsync();
			}
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);
			var result = await sut.UpdateAsync(industry.Id, newName);
			Assert.IsInstanceOfType(result, typeof(IndustryDTO));
			Assert.AreEqual(newName, result.Name);
		}

		[TestMethod]
		public async Task ThrowWhen_UpdatedIndustryIsDeleted()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ThrowWhen_UpdatedIndustryIsDeleted));
			var industry = new Industry()
			{
				Id = Guid.NewGuid(),
				Name = "Industry",
				IsDeleted = true
			};
			using (var arrangeContext = new InsightHubContext(options))
			{
				await arrangeContext.AddAsync(industry);
				await arrangeContext.SaveChangesAsync();
			}
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);
			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAsync(industry.Id, "New Name"));
		}
		[TestMethod]
		public async Task ThrowWhen_UpdatedIndustryIdIsInvalid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ThrowWhen_UpdatedIndustryIdIsInvalid));
			var industry = new Industry()
			{
				Id = Guid.NewGuid(),
				Name = "Industry",
				IsDeleted = true
			};
			using (var arrangeContext = new InsightHubContext(options))
			{
				await arrangeContext.AddAsync(industry);
				await arrangeContext.SaveChangesAsync();
			}
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);
			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAsync(Guid.NewGuid(), "New Name"));
		}
		[TestMethod]
		public async Task ThrowWhen_UpdatedIndustryIdIsEmptry()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ThrowWhen_UpdatedIndustryIdIsEmptry));

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);
			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAsync(Guid.Empty, "New Name"));
		}

		[TestMethod]
		public async Task ThrowWhen_UpdatedIndustryNewNameIsNull()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ThrowWhen_UpdatedIndustryNewNameIsNull));

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);
			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAsync(Guid.NewGuid(), null));
		}
		[TestMethod]
		public async Task ThrowWhen_UpdatedIndustryNewNameIsTaken()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ThrowWhen_UpdatedIndustryNewNameIsTaken));

			var industry = new Industry()
			{
				Id = Guid.NewGuid(),
				Name = "Industry",
				IsDeleted = true
			};
			var industry2 = new Industry()
			{
				Id = Guid.NewGuid(),
				Name = "Industry2",
				IsDeleted = true
			};
			using (var arrangeContext = new InsightHubContext(options))
			{
				await arrangeContext.AddRangeAsync(industry,industry2);
				await arrangeContext.SaveChangesAsync();
			}
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);
			await Assert.ThrowsExceptionAsync<InvalidOperationException>(() => sut.UpdateAsync(industry2.Id, industry.Name));
		}
	}
}
