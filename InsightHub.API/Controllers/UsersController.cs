﻿using System;
using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace InsightHub.API.Controllers
{
	//[Authorize(Roles = "Admin")]
	[Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }
        // GET: api/Users
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var users = await this.userService.GetUsersAsync();
            return Ok(users);
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var user = await this.userService.GetUserAsync(id);
            return Ok(user);
        }

        [HttpGet("Clients")]
        public async Task<IActionResult> GetClients()
        {
            var activeClients = await this.userService.GetActiveClientsAsync();
            var disabledClients = await this.userService.GetDisabledClientsAsync();

            var clients = new UsersDTO { Active = activeClients, Disabled = disabledClients };

            return Ok(clients);
        }
        [HttpGet("Authors")]
        public async Task<IActionResult> GetAuthors()
        {
            var activeAuthors = await this.userService.GetActiveAuthorsAsync();
            var disabledAuthors = await this.userService.GetDisabledAuthorsAsync();

            var clients = new UsersDTO { Active = activeAuthors, Disabled = disabledAuthors };

            return Ok(clients);
        }
    }
}
