using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Services.Mappers;
using InsightHub.Services.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Services
{
	public class UserService : IUserService
	{
		private readonly InsightHubContext context;
		private readonly IEmailService emailService;

		public UserService(InsightHubContext context, IEmailService emailService)
		{
			this.context = context;
			this.emailService = emailService;
		}
		public async Task<ICollection<UserDTO>> GetUsersAsync()
		{
			var users = await this.context.Users
									.Where(u => u.IsDeleted == false)
									.ToListAsync();
			users.CheckCollection();
			var userDtos = users.GetDto();

			return userDtos;
		}
		public async Task<UserDTO> GetUserAsync(Guid id)
		{
			id.IsEmpty(ExceptionMessages.guidEmptyMsg);
			var user = await this.context.Users.FindAsync(id);
			user.CheckForNull(ExceptionMessages.userNullMsg);
			var userDto = user.GetDto();

			return userDto;
		}

		public async Task<ICollection<UserDTO>> GetActiveAuthorsAsync()
		{

			var authors = await this.context.Authors
										.Include(a => a.User)
										.Where(a => a.User.IsApproved == true && a.User.IsDeleted == false && a.User.IsDisabled == false)
										.Select(a => a.User)
										.ToListAsync();

			var authorDto = authors.GetDto();

			return authorDto;
		}
		public async Task<ICollection<AuthorDTO>> GetAuthorsForPage()
		{
			var authors = await this.context.Authors
											.Include(a => a.User)
											.Where(a => a.User.IsApproved && !a.User.IsDeleted && !a.User.IsDisabled)
											.ToListAsync();

			var authorDtos = authors.GetDTO();

			return authorDtos;
		}
		public async Task<ICollection<UserDTO>> GetDisabledAuthorsAsync()
		{
			var authors = await this.context.Authors
										.Include(a => a.User)
										.Where(a => a.User.IsApproved == true && a.User.IsDeleted == false && a.User.IsDisabled == true)
										.Select(a => a.User)
										.ToListAsync();

			var authorDto = authors.GetDto();

			return authorDto;
		}
		public async Task<ICollection<UserDTO>> GetActiveClientsAsync()
		{
			var clients = await this.context.Clients
										.Include(a => a.User)
										.Where(a => a.User.IsApproved == true && a.User.IsDeleted == false && a.User.IsDisabled == false)
										.Select(a => a.User)
										.ToListAsync();

			var clientDtos = clients.GetDto();

			return clientDtos;
		}
		public async Task<ICollection<UserDTO>> GetDisabledClientsAsync()
		{
			var clients = await this.context.Clients
										.Include(a => a.User)
										.Where(a => a.User.IsApproved == true && a.User.IsDeleted == false && a.User.IsDisabled == true)
										.Select(a => a.User)
										.ToListAsync();

			var clientDtos = clients.GetDto();

			return clientDtos;
		}
		public async Task<ICollection<UserDTO>> GetPendingUsersAsync()
		{
			var users = await this.context.Users
									.Include(u=>u.Author)
									.Include(u=>u.Client)
									.Where(u => u.IsApproved == false && !u.IsDeleted)
									.ToListAsync();

			var userDtos = users.GetDto();

			return userDtos;
		}

		public async Task<bool> CreateAuthorAsync(Guid userId, string bio)
		{
			userId.IsEmpty(ExceptionMessages.guidEmptyMsg);

			if (await this.context.Users.AnyAsync(u => u.Id == userId))
			{
				var author = new Author
				{
					Id = Guid.NewGuid(),
					UserId = userId,
					Bio = bio,
					ImagePath = "default-profile-picture.jpg"
				};

				await this.context.Authors.AddAsync(author);
				await this.context.SaveChangesAsync();

				return true;

			}
			return false;
		}


		public async Task<bool> CreateClientAsync(Guid userId)
		{
			userId.IsEmpty(ExceptionMessages.guidEmptyMsg);

			if (await this.context.Users.AnyAsync(u => u.Id == userId))
			{
				var client = new Client
				{
					Id = Guid.NewGuid(),
					UserId = userId
				};

				await this.context.Clients.AddAsync(client);
				await this.context.SaveChangesAsync();

				return true;

			}
			return false;
		}

		public async Task<string> CheckUser(string email)
		{
			email.CheckForNull(ExceptionMessages.nameNullMsg);
			var user = await this.context.Users.FirstOrDefaultAsync(u => u.Email == email);

			if (user == null)
				return "unexisting";
			if (!user.IsApproved)
				return "notApproved";
			if (user.IsDeleted)
				return "deleted";
			if (user.IsDisabled)
				return "disabled";

			return "ok";
		}
		public async Task<bool> DeleteAsync(Guid id)
		{
			id.IsEmpty(ExceptionMessages.guidEmptyMsg);
			var userRole = await this.context.UserRoles.FirstOrDefaultAsync(ur => ur.UserId == id);
			userRole.CheckForNull(ExceptionMessages.userNullMsg);
			var role = await this.context.Roles.FindAsync(userRole.RoleId);

			if (role.NormalizedName == "ADMIN")
			{
				throw new InvalidOperationException();
			}
			var user = await this.context.Users.FirstOrDefaultAsync(u => u.Id == id && u.IsDeleted == false);
			user.IsDeleted = true;
			user.DeletedOn = DateTime.UtcNow;

			this.context.Update(user);
			await this.context.SaveChangesAsync();

			return true;

		}
		public async Task<bool> DisableAsync(Guid userId)
		{
			userId.IsEmpty(ExceptionMessages.guidEmptyMsg);
			var userRole = await this.context.UserRoles.FirstOrDefaultAsync(ur => ur.UserId == userId);
			userRole.CheckForNull(ExceptionMessages.userNullMsg);

			var role = await this.context.Roles.FindAsync(userRole.RoleId);

			if (role.NormalizedName == "ADMIN")
			{
				throw new InvalidOperationException();
			}
			var user = await this.context.Users.Include(u => u.Author)
											   .ThenInclude(a => a.WrittenReports)
											   .FirstOrDefaultAsync(u => u.Id == userId && u.IsDisabled == false);
			user.CheckForNull(ExceptionMessages.userNullMsg);

			user.LockoutEnd = DateTimeOffset.UtcNow.AddMonths(1);
			user.IsDisabled = true;
			user.ModifiedOn = DateTime.UtcNow;
			user.Author.WrittenReports.Select(r => { r.IsDeleted = true; return r; }).ToList();
			this.context.Update(user);
			await this.context.SaveChangesAsync();

			return true;
		}
		public async Task<bool> EnableAsync(Guid userId)
		{
			userId.IsEmpty(ExceptionMessages.guidEmptyMsg);
			var user = await this.context.Users.Include(u => u.Author)
											   .ThenInclude(a => a.WrittenReports)
											   .FirstOrDefaultAsync(u => u.Id == userId && u.IsDisabled == true);
			user.CheckForNull(ExceptionMessages.userNullMsg);

			user.LockoutEnd = DateTime.UtcNow;
			user.IsDisabled = false;
			user.ModifiedOn = DateTime.UtcNow;
			user.Author.WrittenReports.Select(r => { r.IsDeleted = false; return r; }).ToList();

			this.context.Update(user);
			await this.context.SaveChangesAsync();

			return true;
		}

		public async Task<bool> ApproveUserAsync(Guid userId)
		{
			userId.IsEmpty(ExceptionMessages.guidEmptyMsg);
			var user = await this.context.Users.FirstOrDefaultAsync(u => u.Id == userId && !u.IsApproved);
			user.CheckForNull(ExceptionMessages.userNullMsg);

			user.IsApproved = true;

			this.context.Update(user);
			await this.context.SaveChangesAsync();
			this.emailService.SendEmailNotificationOnAccountApproval(user.Email);
			return true;
		}

		public async Task<bool> ApproveReportAsync(Guid reportId)
		{
			reportId.IsEmpty(ExceptionMessages.guidEmptyMsg);
			var report = await this.context.Reports.FirstOrDefaultAsync(r => r.Id == reportId && !r.IsApproved);
			report.CheckForNull(ExceptionMessages.userNullMsg);

			report.IsApproved = true;


			this.context.Update(report);
			await this.context.SaveChangesAsync();

			var userEmails = await this.context.UserIndustries.Include(ui => ui.Client)
															  .ThenInclude(c => c.User)
															  .Where(ui => ui.IndustryId == report.IndustryId)
															  .Select(ui => ui.Client.User.Email)
															  .ToListAsync();
			foreach (var email in userEmails)
			{
				this.emailService.SendEmailWithNewReports(email, report.Name,reportId);
			}
			return true;
		}
		public async Task<bool> RefusePendingReportAsync(Guid reportId)
		{
			reportId.IsEmpty(ExceptionMessages.guidEmptyMsg);
			var report = await this.context.Reports.FindAsync(reportId);
			report.CheckForNull(ExceptionMessages.reportNullMsg);
			report.IsDeleted = true;
			report.DeletedOn = DateTime.UtcNow;

			this.context.Update(report);
			await this.context.SaveChangesAsync();

			return true;
		}

		public async Task<bool> RefusePendingUserAsync(Guid userId)
		{
			userId.IsEmpty(ExceptionMessages.guidEmptyMsg);

			var user = await this.context.Users.FindAsync(userId);
			user.CheckForNull(ExceptionMessages.userNullMsg);
			user.IsDeleted = true;
			user.DeletedOn = DateTime.UtcNow;

			this.context.Update(user);
			await this.context.SaveChangesAsync();

			return true;
		}

		public async Task<bool> IsSubscribed(Guid userId, string industryName)
		{
			userId.IsEmpty();
			industryName.CheckForNull(ExceptionMessages.industryNullMsg);

			var client = await this.GetClientAsync(userId);
			client.CheckForNull(ExceptionMessages.userNullMsg);
			var industry = await this.context.Industries.FirstOrDefaultAsync(i => i.NormalizedName == industryName.ToUpper() &&
																				  !i.IsDeleted);

			industry.CheckForNull(ExceptionMessages.industryNullMsg);
			var hasSubscription = await this.context.UserIndustries.AnyAsync(ui => ui.ClientId == client.Id &&
																				   ui.IndustryId == industry.Id);

			return hasSubscription;

		}

		public async Task<bool> Subscribe(Guid userId, string industryName)
		{
			userId.IsEmpty();
			industryName.CheckForNull(ExceptionMessages.industryNullMsg);

			var client = await this.GetClientAsync(userId);
			client.CheckForNull(ExceptionMessages.userNullMsg);
			var industry = await this.context.Industries.FirstOrDefaultAsync(i => i.NormalizedName == industryName.ToUpper() &&
																				  !i.IsDeleted);
			industry.CheckForNull(ExceptionMessages.industryNullMsg);

			var newSub = new UserIndustry
			{
				ClientId = client.Id,
				IndustryId = industry.Id
			};

			await this.context.UserIndustries.AddAsync(newSub);
			await this.context.SaveChangesAsync();

			return true;
		}

		public async Task<bool> Unsubscribe(Guid userId, string industryName)
		{
			userId.IsEmpty();
			industryName.CheckForNull(ExceptionMessages.industryNullMsg);

			var client = await this.GetClientAsync(userId);
			client.CheckForNull(ExceptionMessages.userNullMsg);
			var industry = await this.context.Industries.FirstOrDefaultAsync(i => i.NormalizedName == industryName.ToUpper() &&
																				  !i.IsDeleted);
			industry.CheckForNull(ExceptionMessages.industryNullMsg);

			var sub = await this.context.UserIndustries.FirstOrDefaultAsync(ui => ui.ClientId == client.Id &&
																				  ui.IndustryId == industry.Id);
			sub.CheckForNull(ExceptionMessages.subNullMsg);

			this.context.UserIndustries.Remove(sub);
			await this.context.SaveChangesAsync();

			return true;
		}

		public async Task<bool> DownloadAsync(Guid userId, Guid reportId)
		{
			userId.IsEmpty();
			reportId.IsEmpty();

			var client = await this.GetClientAsync(userId);
			client.CheckForNull(ExceptionMessages.userNullMsg);
			var reportExists = await this.context.Reports.AnyAsync(r => r.Id == reportId);
			if (!reportExists)
				throw new ArgumentNullException(ExceptionMessages.reportNullMsg);

			var entryExists = await this.context.ClientReports.AnyAsync(cr => cr.UserId == client.Id &&
																			  cr.ReportId == reportId);
			if (!entryExists)
			{
				var downloadEntry = new ClientReports
				{
					UserId = client.Id,
					ReportId = reportId
				};

				await this.context.ClientReports.AddAsync(downloadEntry);
				await this.context.SaveChangesAsync();

				return true;
			}

			return false;
		}

		private async Task<Client> GetClientAsync(Guid userId)
		{
			var client = await this.context.Clients.FirstOrDefaultAsync(c => c.User.Id == userId &&
																			 c.User.IsApproved &&
																			 !c.User.IsDeleted &&
																			 !c.User.IsDisabled);
			return client;
		}

		public async Task<string[]> GetAuthorInfoAsync(Guid authorId)
		{
			authorId.IsEmpty();

			var author = await this.context.Authors.FirstOrDefaultAsync(a => a.Id == authorId &&
																		   a.User.IsApproved &&
																		   !a.User.IsDeleted &&
																		   !a.User.IsDisabled);
			author.CheckForNull(ExceptionMessages.userNullMsg);

			string[] authorInfo = new string[2] { author.ImagePath, author.Bio };
			return authorInfo;
		}

		public async Task<ICollection<AuthorDTO>> GetHomepageAuthorsAsync()
		{
			var authors = await this.context.Authors
											.Include(a => a.WrittenReports)
												.ThenInclude(r => r.Downloads)
											.Include(a => a.User)
											.Where(a => a.User.IsApproved &&
													   !a.User.IsDeleted &&
													   !a.User.IsDisabled)
											.OrderByDescending(a => a.WrittenReports.Count)
											.Where(a => a.User.IsApproved && !a.User.IsDeleted && !a.User.IsDisabled)
											.Take(3)
											.ToListAsync();

			var authorDtos = authors.GetDTO();

			return authorDtos;
		}
		public async Task<Guid> GetAuthorIdAsync(Guid userId)
		{
			userId.IsEmpty();
			var author = await this.context.Authors.FirstOrDefaultAsync(a => a.UserId == userId);

			author.CheckForNull(ExceptionMessages.userNullMsg);

			return author.Id;
		}

		public async Task<bool> UpdateAuthorProfileAsync(Guid authorId, string imagePath,string bio)
		{
			authorId.IsEmpty();
			bio.CheckForNull(ExceptionMessages.stringNullMsg);

			var author = await this.context.Authors.FindAsync(authorId);

			author.CheckForNull(ExceptionMessages.userNullMsg);

			if(imagePath!=null)
				author.ImagePath = imagePath;
			author.Bio = bio;

			this.context.Update(author);
			await this.context.SaveChangesAsync();

			return true;
		}

		public async Task<bool> ToggleReportAsync(Guid reportId)
		{
			reportId.IsEmpty();
			var report = await this.context.Reports.FirstOrDefaultAsync(r => r.Id == reportId && r.IsApproved && !r.IsDeleted);

			report.CheckForNull(ExceptionMessages.reportNullMsg);

			report.IsFeatured = report.IsFeatured ? false : true;

			this.context.Update(report);
			await this.context.SaveChangesAsync();

			return report.IsFeatured;
		}
	}
}
