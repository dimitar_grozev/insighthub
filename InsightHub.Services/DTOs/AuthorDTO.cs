﻿using System;

namespace InsightHub.Services.DTOs
{
	public class AuthorDTO
	{
		public Guid Id { get; set;}
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Bio { get; set; }
		public string ImagePath { get; set; }
	}
}
