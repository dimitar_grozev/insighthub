﻿using InsightHub.Services.Utilities;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
	public interface IBlobService
    {
         Task<MyBlob> GetBlobAsync(string name);
         Task UploadFileBlobAsync(string id, IFormFile file);
         Task UploadFileBlobAsync(string filePath, string fileName);
         Task DeleteBlob(string blobName);
    }
}
