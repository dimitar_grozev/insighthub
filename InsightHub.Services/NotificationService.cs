﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Services
{
	public class NotificationService:INotificationService
	{
		private readonly InsightHubContext context;

		public NotificationService(InsightHubContext context)
		{
			this.context = context;
		}

		public async Task<bool> CreateNotificationAsync(string description)
		{
			var notification = new Notification()
			{
				Id = Guid.NewGuid(),
				Description = description
			};

			await this.context.Notifications.AddAsync(notification);
			await this.context.SaveChangesAsync();

			return true;
		}

		public  int GetCount()
		{
			var usersCount = this.context.Users.Where(u=>u.IsApproved==false && u.IsDeleted == false).Count();
			var reportsCount = this.context.Reports.Where(r=>r.IsApproved==false && r.IsDeleted == false).Count();

			return usersCount+reportsCount;
		}

		public async Task<bool> Remove()
		{
			var notifications =await this.context.Notifications.ToListAsync();
			this.context.Notifications.RemoveRange(notifications);
			await this.context.SaveChangesAsync();

			return true;
		}
	}
}
