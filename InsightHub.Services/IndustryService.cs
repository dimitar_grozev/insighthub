﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Services.Mappers;
using InsightHub.Services.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace InsightHub.Services
{
	public class IndustryService : IIndustryService
	{
		private readonly InsightHubContext context;

		public IndustryService(InsightHubContext context)
		{
			this.context = context;
		}

		public async Task<IndustryDTO> CreateAsync(string name)
		{
			name.CheckForNull(ExceptionMessages.nameNullMsg);

			if (await this.context.Industries.AnyAsync(i => i.Name == name))
			{
				throw new InvalidOperationException("Industry name must be unique!");
			}
			var industry = new Industry()
			{
				Id = Guid.NewGuid(),
				Name = name,
				NormalizedName = name.ToUpper()
			};

			await this.context.Industries.AddAsync(industry);
			await this.context.SaveChangesAsync();
			var industryDto = industry.GetDTO();

			return industryDto;
		}
		public async Task<IndustryDTO> GetAsync(Guid id)
		{
			id.IsEmpty();
			var industry = await this.context.Industries.FirstOrDefaultAsync(i => i.Id == id && !i.IsDeleted);
			industry.CheckForNull(ExceptionMessages.industryNullMsg);
			var industryDto = industry.GetDTO();

			return industryDto;
		}
		public async Task<ICollection<IndustryDTO>> GetAsync()
		{
			var industries = await this.context.Industries.Where(i => !i.IsDeleted).ToListAsync();

			industries.CheckCollection();
			var industryDtos = industries.GetDTO();

			return industryDtos;
		}

		public async Task<IndustryDTO> UpdateAsync(Guid id, string newName)
		{
			id.IsEmpty();
			newName.CheckForNull(ExceptionMessages.nameNullMsg);
			if (await this.context.Industries.AnyAsync(i => i.Name == newName))
			{
				throw new InvalidOperationException("Industry name must be unique!");
			}
			var industry = await this.context.Industries.FirstOrDefaultAsync(i => i.Id == id && !i.IsDeleted);
		
			
			industry.CheckForNull(ExceptionMessages.industryNullMsg);

			industry.Name = newName;
			industry.NormalizedName = newName.ToUpper();
			industry.ModifiedOn = DateTime.UtcNow;

			this.context.Update(industry);
			await this.context.SaveChangesAsync();

			return industry.GetDTO();
		}
		public async Task<bool> DeleteAsync(Guid id)
		{
			id.IsEmpty();
			var industry = await this.context.Industries.FirstOrDefaultAsync(i => i.Id == id && !i.IsDeleted);
			industry.CheckForNull(ExceptionMessages.industryNullMsg);
			

			industry.IsDeleted = true;
			industry.DeletedOn = DateTime.UtcNow;

			await this.context.SaveChangesAsync();

			return true;


		}


	}
}
