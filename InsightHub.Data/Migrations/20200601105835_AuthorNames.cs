﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace InsightHub.Data.Migrations
{
    public partial class AuthorNames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("00150dd5-5388-456f-85d8-52af27a1ea0b"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("00150dd5-5388-456f-85d8-52af27a1ea0b"), new Guid("67966830-655d-42f7-9424-ef5cf441ff8d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("00150dd5-5388-456f-85d8-52af27a1ea0b"), new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("010fe3d8-635b-4335-b92b-8d748733ac3a"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("010fe3d8-635b-4335-b92b-8d748733ac3a"), new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("010fe3d8-635b-4335-b92b-8d748733ac3a"), new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("07d2b39c-f0cf-4112-a55a-8e5f5fd427b8"), new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("07d2b39c-f0cf-4112-a55a-8e5f5fd427b8"), new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("0af3dad4-8d94-47b4-85d4-62f8e18ef8ca"), new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("0af3dad4-8d94-47b4-85d4-62f8e18ef8ca"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("0af3dad4-8d94-47b4-85d4-62f8e18ef8ca"), new Guid("4a817d53-485b-4634-9c09-22eb56d367fc") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("0af3dad4-8d94-47b4-85d4-62f8e18ef8ca"), new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("0af3dad4-8d94-47b4-85d4-62f8e18ef8ca"), new Guid("aba89aa6-e637-4518-83c1-125b625add12") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("1396511b-76e7-4f36-983e-1ebf5a1698dc"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("1396511b-76e7-4f36-983e-1ebf5a1698dc"), new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("1396511b-76e7-4f36-983e-1ebf5a1698dc"), new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("36833c8e-dd9b-4132-9459-7b61081c1600"), new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("36833c8e-dd9b-4132-9459-7b61081c1600"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("36833c8e-dd9b-4132-9459-7b61081c1600"), new Guid("d1b04963-e698-41bf-aded-25bb755c2877") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("36833c8e-dd9b-4132-9459-7b61081c1600"), new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("3e4b5376-5e4a-405f-a5e9-450d3cd63619"), new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("3e4b5376-5e4a-405f-a5e9-450d3cd63619"), new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5e441759-d367-446f-8456-34d118b05561"), new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5e441759-d367-446f-8456-34d118b05561"), new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5e441759-d367-446f-8456-34d118b05561"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("66358a59-4f29-4967-9aef-bc0e559e82e8"), new Guid("0070a7dc-c760-4a34-8c87-d4747824191b") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("66358a59-4f29-4967-9aef-bc0e559e82e8"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("66358a59-4f29-4967-9aef-bc0e559e82e8"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("66358a59-4f29-4967-9aef-bc0e559e82e8"), new Guid("76352896-e547-485f-ae20-73b4f05b664f") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("66358a59-4f29-4967-9aef-bc0e559e82e8"), new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("66358a59-4f29-4967-9aef-bc0e559e82e8"), new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("66358a59-4f29-4967-9aef-bc0e559e82e8"), new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("6879d04c-61a0-4195-96ab-0ec54585c1cd"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("6879d04c-61a0-4195-96ab-0ec54585c1cd"), new Guid("aba89aa6-e637-4518-83c1-125b625add12") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("6879d04c-61a0-4195-96ab-0ec54585c1cd"), new Guid("cffed078-aa51-4fe2-913e-59e6e2624828") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("737fb1e5-534a-4a4c-8f2a-2137c2e13c56"), new Guid("76352896-e547-485f-ae20-73b4f05b664f") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("737fb1e5-534a-4a4c-8f2a-2137c2e13c56"), new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("737fb1e5-534a-4a4c-8f2a-2137c2e13c56"), new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("776d42cb-e955-43dc-9a36-cc355d4309d3"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("776d42cb-e955-43dc-9a36-cc355d4309d3"), new Guid("4a817d53-485b-4634-9c09-22eb56d367fc") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("776d42cb-e955-43dc-9a36-cc355d4309d3"), new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("7cc33bae-61dd-4359-8b7c-7a11afd05c88"), new Guid("0070a7dc-c760-4a34-8c87-d4747824191b") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("7cc33bae-61dd-4359-8b7c-7a11afd05c88"), new Guid("180360e0-6db8-412e-a6f4-3d68a939c583") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("7cc33bae-61dd-4359-8b7c-7a11afd05c88"), new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("828fa2c8-26be-4cc7-9a67-f420e86b3330"), new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("828fa2c8-26be-4cc7-9a67-f420e86b3330"), new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("92a409a6-5571-4b19-8cc5-3091ebb1d477"), new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("92a409a6-5571-4b19-8cc5-3091ebb1d477"), new Guid("180360e0-6db8-412e-a6f4-3d68a939c583") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("962db5bb-b7dd-4b12-b992-0e91e9cc4a11"), new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("962db5bb-b7dd-4b12-b992-0e91e9cc4a11"), new Guid("67966830-655d-42f7-9424-ef5cf441ff8d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("962db5bb-b7dd-4b12-b992-0e91e9cc4a11"), new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a6213e7c-8058-4fc8-999b-e071bc460520"), new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a6213e7c-8058-4fc8-999b-e071bc460520"), new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b2af9ae6-be5b-45d7-9df3-45cb1f8cbf6d"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b2af9ae6-be5b-45d7-9df3-45cb1f8cbf6d"), new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b2af9ae6-be5b-45d7-9df3-45cb1f8cbf6d"), new Guid("9062907e-6a91-4737-95f9-8db332df47f4") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b2af9ae6-be5b-45d7-9df3-45cb1f8cbf6d"), new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b2af9ae6-be5b-45d7-9df3-45cb1f8cbf6d"), new Guid("d1b04963-e698-41bf-aded-25bb755c2877") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b77ea49d-d372-4a0b-8b1c-3aab566a4216"), new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b77ea49d-d372-4a0b-8b1c-3aab566a4216"), new Guid("cffed078-aa51-4fe2-913e-59e6e2624828") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("e0ec89ca-3408-4c23-b418-993f7f01d6f6"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("e0ec89ca-3408-4c23-b418-993f7f01d6f6"), new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("e0ec89ca-3408-4c23-b418-993f7f01d6f6"), new Guid("9062907e-6a91-4737-95f9-8db332df47f4") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("e0ec89ca-3408-4c23-b418-993f7f01d6f6"), new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7") });

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("0070a7dc-c760-4a34-8c87-d4747824191b"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("11dcaf06-8a35-403a-8262-ebd98344039a"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("180360e0-6db8-412e-a6f4-3d68a939c583"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("4a817d53-485b-4634-9c09-22eb56d367fc"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("67966830-655d-42f7-9424-ef5cf441ff8d"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("76352896-e547-485f-ae20-73b4f05b664f"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("9062907e-6a91-4737-95f9-8db332df47f4"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("aba89aa6-e637-4518-83c1-125b625add12"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("cffed078-aa51-4fe2-913e-59e6e2624828"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("d1b04963-e698-41bf-aded-25bb755c2877"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("00150dd5-5388-456f-85d8-52af27a1ea0b"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("010fe3d8-635b-4335-b92b-8d748733ac3a"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("07d2b39c-f0cf-4112-a55a-8e5f5fd427b8"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("0af3dad4-8d94-47b4-85d4-62f8e18ef8ca"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("1396511b-76e7-4f36-983e-1ebf5a1698dc"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("36833c8e-dd9b-4132-9459-7b61081c1600"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("3e4b5376-5e4a-405f-a5e9-450d3cd63619"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("5e441759-d367-446f-8456-34d118b05561"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("66358a59-4f29-4967-9aef-bc0e559e82e8"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("6879d04c-61a0-4195-96ab-0ec54585c1cd"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("737fb1e5-534a-4a4c-8f2a-2137c2e13c56"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("776d42cb-e955-43dc-9a36-cc355d4309d3"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("7cc33bae-61dd-4359-8b7c-7a11afd05c88"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("828fa2c8-26be-4cc7-9a67-f420e86b3330"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("92a409a6-5571-4b19-8cc5-3091ebb1d477"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("962db5bb-b7dd-4b12-b992-0e91e9cc4a11"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("a6213e7c-8058-4fc8-999b-e071bc460520"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("b2af9ae6-be5b-45d7-9df3-45cb1f8cbf6d"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("b77ea49d-d372-4a0b-8b1c-3aab566a4216"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("e0ec89ca-3408-4c23-b418-993f7f01d6f6"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("332ac8b8-a6b0-49ef-8007-dba8a8ad25ab"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("4ed92a05-a299-4b37-8040-7f9a29b9795b"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("4f06865f-a9d7-428a-956b-9a72268403ec"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("b3f88e39-6e58-4f50-a539-02780f00aeec"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("c0f3e2d5-33b1-454c-a7aa-f1ba51684e0a"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("0b3a1500-1c80-45da-9ea0-b054df73b99f"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("426b4ff5-6787-4b81-abeb-f25d8a3fad83"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("d0a91bed-717c-4906-a230-b791e8119932"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("ea56d76a-dedd-419f-946e-71f114733aff"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("edf9d9bb-e1bb-42fa-b8e5-e8e1f88ecfb0"));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("297d06e6-c058-486f-a18a-06a971ebfcd7"),
                column: "ConcurrencyStamp",
                value: "ebd461a7-4684-44d7-8d40-dbecd35dead3");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("6c8fcd7e-62f6-4f3e-a73d-acbfd60b97ab"),
                column: "ConcurrencyStamp",
                value: "dab7e41a-4471-4516-94d4-2d4483e81b9d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7f66990c-36bc-4381-9f81-32e06e168319"),
                column: "ConcurrencyStamp",
                value: "e6618b6b-8b9b-43c4-9fc7-ae03ef5be6df");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb93"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "Fullname", "PasswordHash" },
                values: new object[] { "4bc56afe-a3e6-4c6c-ab6c-2bc8e08c0b13", new DateTime(2020, 6, 1, 10, 58, 33, 952, DateTimeKind.Utc).AddTicks(5414), "John Smith", "AQAAAAEAACcQAAAAECkwVmRxcsdWNnLxmcnr+/Lt28FyHMHXYaDsLqxg6CX53kPPWT9TB4M4KGNn2hd5BQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb94"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "Fullname", "PasswordHash" },
                values: new object[] { "2f7f5035-6655-45a3-8a6b-c8d6f1359329", new DateTime(2020, 6, 1, 10, 58, 33, 952, DateTimeKind.Utc).AddTicks(7646), "Jim Doe", "AQAAAAEAACcQAAAAEE7pVpqipyn6vJ3Cjq4FwgLEhugFCCGfL4kdWtEndVhCesI1fibgFl7M4Up16iChfw==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb95"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "Fullname", "PasswordHash" },
                values: new object[] { "d7d5e531-2933-40b7-a857-276f3bc0b1bc", new DateTime(2020, 6, 1, 10, 58, 33, 952, DateTimeKind.Utc).AddTicks(7672), "Rose James", "AQAAAAEAACcQAAAAEKQoc7eltm3DjbT7pq7VCpAS3IF46Gfm6NXk81wn794JfmkiDWhReVh7ArjpXu05PQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb96"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "Fullname", "PasswordHash" },
                values: new object[] { "f5496b99-a7ca-4454-ba6a-32a4c313f1be", new DateTime(2020, 6, 1, 10, 58, 33, 952, DateTimeKind.Utc).AddTicks(7680), "Cate Williams", "AQAAAAEAACcQAAAAEAOrmLvrKrP0iF3s3Gmtby2cwgn79U7eHrotkXyoobLm24fDen2sxYExwmziBhWBuw==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb97"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "Fullname", "PasswordHash" },
                values: new object[] { "edbf26bb-a841-4a5e-a055-03216ba3f89d", new DateTime(2020, 6, 1, 10, 58, 33, 952, DateTimeKind.Utc).AddTicks(7686), "Max Upton", "AQAAAAEAACcQAAAAEGLB7hzuNewRR97yDyIDmo3mUu1pIf75BC0KwbXiorernWupuyL42Qayb04ZbpLAhw==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb98"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "Fullname", "PasswordHash" },
                values: new object[] { "7b194114-0e13-4317-b97b-66ae3d2b5cb7", new DateTime(2020, 6, 1, 10, 58, 33, 952, DateTimeKind.Utc).AddTicks(7769), "David Scott", "AQAAAAEAACcQAAAAEAciV+mPlOO9cvQ8w4LK5+yJhOn8+cfwXgwTE4RdFTfcQJW7r00EFjdQwK03UKuqZQ==" });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "Bio", "ImagePath", "UserId" },
                values: new object[,]
                {
                    { new Guid("e0821c76-d991-4df7-abdd-5654e28cc872"), "Jim Doe has been involved with ICT research in Africa since 1997 and has participated in diverse research projects in 14 African countries. He started his career in 1994 as a public relations officer with Software Technologies Limited (an East African Oracle distributor), before serving as a research analyst/project officer for Telecom Forum Africa in 1997.", "3.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb94") },
                    { new Guid("ebaf1989-5a9d-4dfc-979c-7bd7347fce9e"), "Rosa James has a background in journalism and has been published in various local, regional, and international magazines and newspapers. She has spoken at various industry events across sub-Saharan Africa including countries like Ethiopia, Ghana, Kenya, Nigeria, Rwanda, Tanzania and Zambia as well as leading and facilitating at IDC’s events in the region. Rose is also a published author of a children’s novel.", "1.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb95") },
                    { new Guid("400ef15e-4eca-4d4e-9d96-bdc21028277f"), "Cate Williams has been an analyst InsightHub for several years. Early on as an analyst she covered CRM applications, but more recently her work has been in integration middleware covering markets such as API management, file sync and share, and B2B integration.", "2.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb96") },
                    { new Guid("cf20fa7b-5e0e-4f63-a608-d17a31057370"), "Max Upton has more than 15 years of experience in business research, focusing on financial services and business innovation. His consulting work for InsightHub has allowed him to work closely with leading banks and regulators in the region for their technology and innovation strategies. Mr. Upton is concurrently head of InsightHub's business and operations in Thailand.", "7.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb97") },
                    { new Guid("99c3b74a-0e9e-4ebc-b737-a11906b3ffc5"), "Previously, David Scott served for ten years as Enterprise Architect at CareFirst Blue Cross/Blue Shield, responsible for building business and technology alignment roadmaps for the ACA/HIX Health Reform, Mandated Implementations, PCMH, and Provider Network domains to ensure alignment of annual and tactical IT project planning with business goals.", "8.jpg", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb98") }
                });

            migrationBuilder.InsertData(
                table: "Industries",
                columns: new[] { "Id", "DeletedOn", "ImagePath", "IsDeleted", "ModifiedOn", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("bd529ec1-3ab4-4c28-87d3-5ff76cf85498"), null, null, false, null, "Energy", "ENERGY" },
                    { new Guid("882bda0d-578d-44d5-b725-2abc36712488"), null, null, false, null, "Information Technology", "INFORMATION TECHNOLOGY" },
                    { new Guid("1d86b2df-8961-40d1-9806-57bf1e69ddd8"), null, null, false, null, "Healthcare", "HEALTHCARE" },
                    { new Guid("abc595f0-84cc-493c-9792-b4b40cbd5900"), null, null, false, null, "Finance", "FINANCE" },
                    { new Guid("28d170e0-dcea-4d55-abad-3190164dc74c"), null, null, false, null, "Marketing", "MARKETING" }
                });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "Id", "DeletedOn", "IsDeleted", "ModifiedOn", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("5d97d07c-9c8b-465f-affc-87b1ce805cf4"), null, false, null, "computers", "COMPUTERS" },
                    { new Guid("d79dc940-e7c4-4299-8f75-7a1951697869"), null, false, null, "hospital", "HOSPITAL" },
                    { new Guid("c56df33e-3e01-4025-9a8e-7f802781540f"), null, false, null, "influence", "INFLUENCE" },
                    { new Guid("6a951858-7b08-4a06-b02b-c9b5a69622eb"), null, false, null, "trend", "TREND" },
                    { new Guid("71d6a5e2-d611-41c4-a110-609ce4ff30da"), null, false, null, "sales", "SALES" },
                    { new Guid("8411dc1c-c965-4765-9fbf-b861c67ed887"), null, false, null, "profit", "PROFIT" },
                    { new Guid("a01b7213-9822-4a2c-9d7c-5ab9a298867b"), null, false, null, "insurance", "INSURANCE" },
                    { new Guid("d38f1457-19f5-4e0b-b210-9855754ee9fb"), null, false, null, "stock", "STOCK" },
                    { new Guid("1cc2c117-df96-4b62-b17a-bdff62e9c09e"), null, false, null, "nuclear", "NUCLEAR" },
                    { new Guid("ce9e44c6-dafb-4389-aea9-84c2b5967f92"), null, false, null, "solar", "SOLAR" },
                    { new Guid("5aea1ce4-d3a7-43e5-965c-cf779b903d70"), null, false, null, "oil", "OIL" },
                    { new Guid("0262675b-27d3-4a26-b5b1-9bd895effd4a"), null, false, null, "laboratory", "LABORATORY" },
                    { new Guid("88def612-ee91-47b5-88d1-572be26187f0"), null, false, null, "vaccine", "VACCINE" },
                    { new Guid("050c2347-2842-44db-bffa-065a874c148a"), null, false, null, "research", "RESEARCH" },
                    { new Guid("8c6a0203-ba5f-4f52-8bf2-2639df7c4376"), null, false, null, "medicine", "MEDICINE" },
                    { new Guid("cb0bd7f4-3134-494b-89cc-a5d2a01e0ade"), null, false, null, "web", "WEB" },
                    { new Guid("718be14d-318c-4f66-afa9-995e1d66ff1c"), null, false, null, "hardware", "HARDWARE" },
                    { new Guid("30f56b58-45a7-4f98-a523-7813de98325f"), null, false, null, "software", "SOFTWARE" },
                    { new Guid("f45de922-c57d-4d6c-a011-585d13eb8404"), null, false, null, "investment", "INVESTMENT" },
                    { new Guid("f62be2ad-e86b-417f-9c90-13e38e4d86aa"), null, false, null, "gas", "GAS" }
                });

            migrationBuilder.InsertData(
                table: "Reports",
                columns: new[] { "Id", "AuthorId", "Content", "DeletedOn", "ImagePath", "IndustryId", "IsApproved", "IsDeleted", "IsFeatured", "ModifiedOn", "Name", "NormalizedName", "Summary" },
                values: new object[,]
                {
                    { new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb"), new Guid("99c3b74a-0e9e-4ebc-b737-a11906b3ffc5"), null, null, null, new Guid("bd529ec1-3ab4-4c28-87d3-5ff76cf85498"), true, false, false, null, "Worldwide Utilities Connected Asset", "WORLDWIDE UTILITIES CONNECTED ASSET", "Energy and digital technologies are radically transforming utilities asset operations and strategies. Renewables and decentralized power generation, energy delivery networks (electricity, gas, and heat), and water and waste water management benefit from the developments and adoption of IoT, AI, and digital twins.  Energy Insights: Worldwide Utilities Connected Asset Strategies service focuses on all these. It provides guidance to end users in terms of digital transformation (DX) use cases road map, analysis of emerging IT trends, and evaluation of technology providers in this space. It looks into the future of work in the utilities value chain context. At the same time, it provides technology vendors a view on utilities' operational excellence asset strategies addressing their go to market.This service develops unique analysis and comprehensive data through  Energy Insights' proprietary research projects, along with ongoing communications with industry experts, utility management, ICT vendors, and service providers. With decades of experience in the utility industry, our analysts leverage a broad spectrum of expertise and intellectual property from both  and  Energy Insights. Research documents elucidate business strategy, best practices, technology selection, and vendor assessment, along with short perspectives on topical issues. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research documents." },
                    { new Guid("9062907e-6a91-4737-95f9-8db332df47f4"), new Guid("cf20fa7b-5e0e-4f63-a608-d17a31057370"), null, null, null, new Guid("28d170e0-dcea-4d55-abad-3190164dc74c"), true, false, false, null, "CMO Advisory Service", "CMO ADVISORY SERVICE", "CMO Advisory Service guides marketing leaders as they master the science of marketing. Digital transformation offers CMOs the opportunity to become revenue drivers and architects of the customer experience. Leaders leverage 's deep industry knowledge, powerful quantitative models, peer-tested practices, and personalized guidance to advance their operations. Whether seeking fresh perspectives on core challenges or counsel on emerging developments, offers a trusted source of insight.Markets and Subjects Analyzed. Marketing Investment and Transformational Operations. Planning and budgeting investments in the marketing mix, marketing technology, and marketing operations, accountability, and attribution. Application of New Technology for Marketing. Guidance on the implications of new technologies such as artificial intelligence (AI), collaboration, and marketing automation solutions. Delivering the New Customer Experience. Mapping and responding to the B2B customer decision journey. The Future Marketing Organization. Organizational design, staff allocation benchmarks, role definition, talent development, shared services, organizational alignment, and change management. Developing core competencies: Content marketing, customer intelligence and analytics, integrated digital and social engagement, sales enablement, and loyalty and advocacy" },
                    { new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7"), new Guid("99c3b74a-0e9e-4ebc-b737-a11906b3ffc5"), null, null, null, new Guid("28d170e0-dcea-4d55-abad-3190164dc74c"), true, false, false, null, "Marketing and Sales Solutions", "MARKETING AND SALES SOLUTIONS", "Marketing and sales technologies are driving forces for all companies as customers move to online, mobile-first, and collaborative relationship models. In reaching and selling to customers, it is essential that organizations aggressively develop the necessary operational and analytic skills, collaborative cultures, and creative problem solving needed to truly add value to the customer relationship. 's Marketing and Sales Solutions service provides strategic frameworks for thinking about the individual and aligned areas of marketing and sales technology as parts of a holistic business strategy. This program delivers insight, information, and data on the main drivers for the adoption of these technologies in the broader context of customer experience (CX) and networked business strategies.Markets and Subjects Analyzed. Marketing automation, campaign management, and go to market execution applications. Sales force automation applications. Predictive analytics and business KPIs. Mobile and digital applications and strategies. Customer data and analytics. CX strategies. Core Research. Reports on how 670 U.S. large enterprises use more than 300 vendors in 15 martech categories across 5 vertical markets:Consumer banking; Retail; CPG manufacturing; Securities and investment services; Travel and hospitality; MarketScape(s) on related solutions areas such as marketing clouds and artificial intelligence; TechScapes and PlanScapes on various topics such as GDPR, account-based marketing, personalization, and mobile marketing; Marketing software forecast and vendor shares; Sales force automation forecast and vendor shares. In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment. " },
                    { new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02"), new Guid("cf20fa7b-5e0e-4f63-a608-d17a31057370"), null, null, null, new Guid("28d170e0-dcea-4d55-abad-3190164dc74c"), true, false, false, null, "Customer Experience Management Strategies", "CUSTOMER EXPERIENCE MANAGEMENT STRATEGIES", "Customer Experience Management Strategies SIS provides a framework and critical knowledge for understanding the changing nature of the customer experience (CX) and guides chief experience officers and their organizations as they master the digital transformation of the customer experience. This product covers the concepts of experience management and customer experience, looking at how digital transformation is driving change to customer expectations, preferences, and behavior and how enterprises must adopt new technologies to meet these urgent challenges. Markets and Subjects Analyzed: Experience management; Customer-centric engagement Customer-centric operations; Customer-centric solution design; Impact of 3rd Platform technologies on customer experience; Intelligence and analytics-driven customer experience; Customer experience along the customer journey; Customer experience benchmarks Core Research: Customer Experience Taxonomy; Digital Transformation of the Customer Experience; Redefining Experience Management; PeerScape: Customer Experience; Customer Experience in an Algorithm Economy; Technology-Enabled Storytelling; MaturityScape: Customer Experience; Customer Intelligence and Analytics; Innovation Accelerators in Customer Experience: Artificial Intelligence; In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef"), new Guid("99c3b74a-0e9e-4ebc-b737-a11906b3ffc5"), null, null, null, new Guid("28d170e0-dcea-4d55-abad-3190164dc74c"), true, false, false, null, "Retail Insights: European Retail Digital Transformation Strategies", "RETAIL INSIGHTS: EUROPEAN RETAIL DIGITAL TRANSFORMATION STRATEGIES", "European retailing is in a state of change since digital technologies have become so pervasive in the retail journey and consumer expectations, in terms of customer experience, with their preferred brands rapidly evolving. European retailers are adapting to these changes through the adoption of commerce everywhere business models and technology. Retailers from across Europe are in the process of determining how digital impacts them and what their digital transformation approach and strategy should be. This reveals a more and more complex and highly differentiated European region, where differences exist and persist across countries as well as subsegments (fashion and apparel, department stores, food and grocery, etc.). Retail Insights is witnessing among European retailers, at varying degrees of maturity, a race to digitize because of the potential new revenue streams and retail operational efficiencies that can be derived. Retail Insights: European Retail Digital Transformation Strategies advisory service examines the impact of digital transformation on the European retailers' business, technology, and organizational areas. Specific coverage is given to provide valuable insights into the European retail industry, with a specific focus on digital transformation strategies applied by retail companies to improve the omni-channel customer experience. This advisory service develops unique analysis and comprehensive data through Retail Insights' proprietary research projects, along with ongoing communications with industry experts, retail CIOs, and line-of-business executives, and ICT product and service vendors. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports. Our analysts are also available to provide personalized advice for retail executives and ICT vendors to help them make better-informed decisions." },
                    { new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97"), new Guid("ebaf1989-5a9d-4dfc-979c-7bd7347fce9e"), null, null, null, new Guid("abc595f0-84cc-493c-9792-b4b40cbd5900"), true, false, false, null, "Consumer Banking Engagement Strategies", "CONSUMER BANKING ENGAGEMENT STRATEGIES", "Being successful in banking will be determined by how well institutions manage the transformation in both digital and physical channels. Customers have seemingly ubiquitous access to their accounts on their terms and on their devices as banks continue to strategize about the future of the branch network and new channels emerge. Unfortunately, many banks are still looking at their channel strategy in a silo without fully understanding that customer engagement is the key to a profitable relationship. Today’s technology has fostered this customer-led revolution, yet there are many more changes yet to be realized as new technology is introduced. Advances in how we engage the customer are pushing the limits and skill sets of business units, marketers, and IT personnel as customers demand more from their retail bank. The Financial Insights: Consumer Banking Engagement Strategies provides critical analysis of the opportunities and options facing banks as they wrestle with their technology plans and investment decisions in alignment with their strategic goals. This research delivers key insights regarding the business drivers of and value delivered from customer-facing banking technology investments. Topics Addressed Throughout the year, this service will address the following topics: Digital transformation: Strategies and use cases are developing as banks transform all channels, including account opening and onboarding, ATM and ITM, augmented and virtual reality, branch banking, call center, chatbot services, contextualized marketing, conversational banking, digital banking (online and mobile), and social business. Whether these are first-generation offerings or have been around for decades, strategies need to be developed to implement, support, and upgrade these channels to stay with the times. Engagement strategies: Financial institutions are realizing that the number of engagements a customer has is an important factor in profitability. Using big data and analytics to properly measure the number of engagements is a start, but most institutions need to go beyond a prescriptive approach to customer behavior to a more cognitive approach. Omni-experience: The customer life-cycle process offers multiple channels that define the experience and dictate current and future relationships. Customer trends and strategies: This includes topics from level of interaction today to what is likely to be future behavior." },
                    { new Guid("d1b04963-e698-41bf-aded-25bb755c2877"), new Guid("99c3b74a-0e9e-4ebc-b737-a11906b3ffc5"), null, null, null, new Guid("abc595f0-84cc-493c-9792-b4b40cbd5900"), true, false, false, null, "Asia/Pacific Financial Services IT", "ASIA/PACIFIC FINANCIAL SERVICES IT", "Financial Insights: Asia/Pacific Banking Customer Centricity program provides insights into the evolving needs of Asia/Pacific retail/consumer banking retail customers and guidelines on how banks are to respond to these trends. The program will give advice to technology buyers on the technology that supports the customer centricity agenda of the financial institution – particularly in the areas of customer relationship management (CRM), omni-experience and omni-channel solutions, and in loyalty management. The service will be backed by a comprehensive consumer survey on the preferences of Asia/Pacific retail/consumer banking customers in various aspects of customer experience. Financial Insights will then undertake on how financial institutions are to develop an effective customer management agenda, delving into the concept, approach, strategy, use cases, and enabling technologies for customer centricity. Throughout the year, this service will address the following topics: Customer Experience Trends in Asia/Pacific Retail Banking How Asia/Pacific Consumers Pay - Evolving Trends in Retail Payments Customer Centricity in Retail Banking IT and Operations Customer Centricity in Marketing and Product Development Customer Centricity Technology Providers for Asia/Pacific Retail Banking." },
                    { new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015"), new Guid("ebaf1989-5a9d-4dfc-979c-7bd7347fce9e"), null, null, "", new Guid("abc595f0-84cc-493c-9792-b4b40cbd5900"), true, false, false, null, "Universal Payment Strategies", "UNIVERSAL PAYMENT STRATEGIES", "The payment industry has seen drastic changes in the past decade. New technologies, entrants, and business models have forced incumbent vendors and their financial institution customers to rethink how they move money. Stakeholders across the payment value chain — card-issuing banks, merchant acquirers, payment networks, and payment processors — face increasingly complex decisions. In this turbulent market, the players need more than facts and figures; they need critical analysis and insightful opinions. Markets and Subjects Analyzed Throughout the year, this service will address the following topics: Developing trends in payments such as omni-channel and alternative payment networks Evaluation and integration of new payment channels like voice commerce and IoT Enterprise risk, compliance, and fraud issues affecting payment products Legal and regulatory issues around the world that will affect how payments develop Middle- and back-office technologies that will affect the payment strategies of financial institutions Emerging technologies such as blockchain, AI, and next-generation security and their potential for altering the payment landscape Core Research MarketScape: Gateways for Integrated Payments B2B Payments: Digital Transformation in Transactions Retail Revolution: Beyond Card Payments Blockchain Payments: Short-Term Realities Persistent Payments: Automated Transactions in a Connected World Real-Time Payment Productization: Overlays in Action Retail Fraud: Protecting a Multi-Payment Environment" },
                    { new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a"), new Guid("99c3b74a-0e9e-4ebc-b737-a11906b3ffc5"), null, null, null, new Guid("abc595f0-84cc-493c-9792-b4b40cbd5900"), true, false, false, null, "Worldwide Banking IT Spending Guide", "WORLDWIDE BANKING IT SPENDING GUIDE", "The Financial Insights: Worldwide Banking IT Spending Guide examines the banking industry opportunity from a technology, functional process, and geography perspective. This comprehensive database delivered via 's Customer Insights query tool allows the user to easily extract meaningful information about the banking technology market by viewing data trends and relationships and making data comparisons. Markets Covered This product covers the following segments of the banking market: 9 regions: USA, Canada, Japan, Western Europe, Central and Eastern Europe, Middle East and Africa, Latin America, PRC, and Asia/Pacific 4 technologies: Hardware, software, services, and internal IT spend 5 banking segments: Consumer banking, corporate and institutional banking, corporate administration, enterprise utilities, and shared services 30+ functional processes: Channels, payments, core processing, and more 4 company size tiers: Institution size by tier 1 through tier 4 2 institution types: Banks and credit unions 6 years of data Data Deliverables This spending guide is delivered on a semiannual basis via a web-based interface for online querying and downloads. For a complete delivery schedule, please contact an sales representative. The following are the deliverables for this spending guide: Annual five-year forecasts by regions, technologies, banking segments, functional processes, tiers, and institution types; delivered twice a year About This Spending Guide Financial Insights: Worldwide Banking IT Spending Guide provides guidance on the expected technology opportunity around this market at a regional and total worldwide level. Segmented by functional process, institution type, company size tier, region, and technology component, this guide provides IT vendors with insights into both large and rapidly growing segments of the banking technology market and how the market will develop over the coming years." },
                    { new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82"), new Guid("400ef15e-4eca-4d4e-9d96-bdc21028277f"), null, null, null, new Guid("abc595f0-84cc-493c-9792-b4b40cbd5900"), true, false, false, null, "Insurance Digital Transformation Approach", "INSURANCE DIGITAL TRANSFORMATION APPROACH", "Customer experience is high on the agenda for most insurers and intermediaries as customers expect true value for the premiums they pay, above and beyond the traditional products and services delivered to them. To continue to be relevant in a changing marketplace, insurance organizations across the globe need to deliver contextual and value-centric insurance that is rooted on the principles of proactive risk management and secure, transparent, seamless, and contextual engagements across the customer journey. To achieve this, the industry needs to accelerate its effort to transform from a traditional, product-centric mindset to a customer-centric mindset enabled by digital technologies and the power of data and analytics. Insurance organizations need to increasingly play the role of true risk advisors rather than mere product sellers. As they progress in their transformation journey, they need advice and guidance to understand the opportunities and possibilities with the new technologies and to build and deploy digital capabilities while also tackling existing barriers to change. The Financial Insights: Worldwide Insurance Digital Transformation Strategies advisory service provides clients with insightful information and analysis of global insurance trends. It also provides coverage of how digital technologies like Big Data, analytics, cloud, mobility, IoT, blockchain, and cognitive technologies impact the life and annuity, accident and health, and property and casualty insurance markets. The service offers timely, strategic, and actionable business IT and application advice to guide our clients as they undertake digital investment decisions, plan for future technology needs, and benchmark themselves against competitors. It provides ongoing research reports, analyst access, and admission to content-rich conferences and webcasts for the benefit of insurance organizations as well as technology vendors." },
                    { new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37"), new Guid("e0821c76-d991-4df7-abdd-5654e28cc872"), null, null, null, new Guid("abc595f0-84cc-493c-9792-b4b40cbd5900"), true, false, false, null, "International Corporate Banking Digital Transformation", "INTERNATIONAL CORPORATE BANKING DIGITAL TRANSFORMATION", "With growing competition from nonbank platforms and the commoditization of products, the ability to onboard, connect, and deliver services to corporate customers is becoming paramount. With PSD2 in Europe democratizing data access and payment services, corporate banks have to transform their value proposition to deliver new data-driven services to their clients and add value, rather than their traditional, commoditized product proposition. Reducing the connectivity cost, increasing speed of data delivery toward real time, and integrating bank systems with corporate enterprise resource planning (ERP), treasury management systems (TSM), payment factories, and in-house banks will be paramount to help corporate clients manage capital, monitor cash flows, and optimize their liquidity. These infrastructure upgrades will also be crucial to exploit the evolving ecosystems brought about by distributed ledger technology (DLT) and the internet of things (IoT), where data discovery, analysis, and sharing will accelerate trade and reduce risk. Sensors attached to goods in transit — from the manufacturing plant to the retail outlet — could offer opportunities to banks' cash management and trade services businesses, better matching flows of payments and goods between seller and buyer. The Financial Insights: Worldwide Corporate Banking Digital Transformation Strategies advisory service provides clients with insightful information and analysis of corporate and commercial banking trends, including cash and treasury, trade finance, commercial lending, and payments. It also provides coverage of how the impact of emerging 3rd Platform technologies like big data, analytics, cloud, mobility, IoT, blockchain, and cognitive technologies transforms the sector and how their convergence unlocks new business and operating models. The service offers timely, strategic, and actionable business IT and application advice to guide our clients as they undertake digital investment decisions, plan for future technology needs, and benchmark themselves against competitors. It provides ongoing research reports, analyst access, and admission to content-rich conferences and webcasts for the benefit of financial institutions as well as technology vendors." },
                    { new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0"), new Guid("ebaf1989-5a9d-4dfc-979c-7bd7347fce9e"), null, null, null, new Guid("1d86b2df-8961-40d1-9806-57bf1e69ddd8"), true, false, false, null, "Health systems and universal health coverage", "HEALTH SYSTEMS AND UNIVERSAL HEALTH COVERAGE", "In the SDG monitoring framework, progress towards universal health coverage (UHC) is tracked with two indicators: (i) a service coverage index (which measures coverage of selected essential health services on a scale of 0 to 100); and (ii) the proportion of the population with large out-of-pocket expenditures on health care (which measures the incidence of catastrophic health spending, rendered as percentage). The service coverage index improved from 45 globally in 2000 to 66 in 2017, with the strongest increase in low-and lower-middle-income countries, where the baseline at 2000 was lowest. However, the pace of that progress has slowed since 2010. The improvements are especially notable for infectious disease interventions and, to a lesser extent, for reproductive, maternal and child health services. Within countries, coverage of the latter services is typically lower in poorer households than in richer households. Overall, between one third and one half the world’s population (33% to 49%) was covered by essential health services in 2017 . Service coverage continued to be lower in low- and middle-income countries than in wealthier ones; the same held for health workforce densities and immunization coverage (Figure 1.2). Available data indicate that over 40% of all countries have fewer than 10 medical doctors per 10 000 people, over 55% have fewer." },
                    { new Guid("180360e0-6db8-412e-a6f4-3d68a939c583"), new Guid("e0821c76-d991-4df7-abdd-5654e28cc872"), null, null, null, new Guid("1d86b2df-8961-40d1-9806-57bf1e69ddd8"), true, false, false, null, "Investing in strengthening country health information systems", "INVESTING IN STRENGTHENING COUNTRY HEALTH INFORMATION SYSTEMS", "Accurate, timely, and comparable health-related statistics are essential for understanding population health trends. Decision-makers need the information to develop appropriate policies, allocate resources and prioritize interventions. For almost a fifth of countries, over half of the indicators have no recent primary or direct underlying data. Data gaps and lags prevent from truly understanding who is being included or left aside and take timely and appropriate action. The existing SDG indicators address a broad range of health aspects but do not capture the breadth of population health outcomes and determinants. Monitoring and evaluating population health thus goes beyond the indicators covered in this report and often requires additional and improved measurements. WHO is committed to supporting Member States to make improvements in surveillance and health information systems. These improvements will enhance the scope and quality of health information and standardize processes to generate comparable estimates at the global level. Getting accurate data on COVID-19 related deaths has been a challenge. The COVID-19 pandemic underscores the serious gaps in timely, reliable, accessible and actionable data and measurements that compromise preparedness, prevention and response to health emergencies. The International Health Regulations (IHR) (2005) monitoring framework is one of the data collection tools that have demonstrated value in evaluating and building country capacities to prevent, detect, assess, report and respond to public health emergencies. From self-assessment of the 13 core capacities in 2019, countries have shown steady progress across almost all capacities including surveillance, laboratory and coordination. As the pandemic progresses, objective and comparable data are crucial to determine the effectiveness of different national strategies used to mitigate and suppress, and thus to better prepare for the probable continuation of the epidemic over the next year or more." },
                    { new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2"), new Guid("ebaf1989-5a9d-4dfc-979c-7bd7347fce9e"), null, null, null, new Guid("1d86b2df-8961-40d1-9806-57bf1e69ddd8"), true, false, false, null, "Maternal mortality has declined but progress is uneven across regions", "MATERNAL MORTALITY HAS DECLINED BUT PROGRESS IS UNEVEN ACROSS REGIONS", "A total of 295 000 [UI 1 80%: 279 000–340 000] women worldwide lost their lives during and following pregnancy and childbirth in 2017, with sub-Saharan Africa and South Asia accounting for approximately 86% of all maternal deaths worldwide. The global maternal mortality ratio (MMR, the number of maternal deaths per 100 000 live births) was estimated at 211 [UI 80%: 199–243], representing a 38% reduction since 2000. On average, global MMR declined by 2.9% every year between 2000 1 UI = uncertainty interval. and 2017. If the pace of progress accelerates enough to achieve the SDG target (reducing global MMR to less than 70 per 100 000 live births), it would save the lives of at least one million women. The majority of maternal deaths are preventable through appropriate management of pregnancy and care at birth, including antenatal care by trained health providers, assistance during childbirth by skilled health personnel, and care and support in the weeks after childbirth. Data from 2014 to 2019 indicate that approximately 81% of all births globally took place in the presence of skilled health personnel, an increase from 64% in the 2000–2006 period. In sub-Saharan Africa, where roughly 66% of the world’s maternal deaths occur, only 60% of births were assisted by skilled health personnel during the 2014–2019 period. " },
                    { new Guid("0070a7dc-c760-4a34-8c87-d4747824191b"), new Guid("e0821c76-d991-4df7-abdd-5654e28cc872"), null, null, null, new Guid("1d86b2df-8961-40d1-9806-57bf1e69ddd8"), true, false, false, null, "European Value-Based Healthcare Digital Transformation", "EUROPEAN VALUE-BASED HEALTHCARE DIGITAL TRANSFORMATION", " With the advent of the value-based paradigm, healthcare organizations are today tasked to ensure equitable access and financial sustainability while improving and reducing the variation of clinical outcomes in healthcare systems. Increasing collaboration across the healthcare value chain, adopting a more personalized approach to treatment, championing the patient experience, and adopting outcome-based business models are fast emerging as necessary for the healthcare industry to succeed. The IDC Health Insights: European Value-Based Healthcare Digital Transformation Strategies service focuses on analyzing European healthcare providers, payers, and public health policy makers' digital strategy and best practices in supporting the adoption of value-based healthcare to deliver the maximum value to patients. Value-based healthcare is a key driver of digital transformation as it requires an unprecedented level of availability of patient information. Key solutions as HIE, population health management, smart patient engagement tools, advanced analytics and cognitive, and cloud and mobility will fundamentally impact care providers and payers' value-based healthcare strategies execution and success. Approach. This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244"), new Guid("ebaf1989-5a9d-4dfc-979c-7bd7347fce9e"), null, null, null, new Guid("1d86b2df-8961-40d1-9806-57bf1e69ddd8"), true, false, false, null, "European Digital Hospital Evolution", "EUROPEAN DIGITAL HOSPITAL EVOLUTION", "European Digital Hospital service provides detailed information about the evolution of digital hospital transformation and technology adoption. It offers valuable insights into IT solution trends within the hospital sector, including healthcare-specific technologies such as electronic health records; admission, discharge, and transfer; digital medical imaging systems; VNA and archiving; and business intelligence and patient management systems. This service focuses on a vital component of healthcare — the digital hospital. Healthcare systems across the world are facing care quality and sustainability challenges that require a new approach to care delivery and reimbursement models. The fully digital hospital is in the center of that journey, being a key enabler of change. The hospital CIOs are facing new technology but, at the same time, a growing legacy IT burden that must be managed alongside of new digital initiatives. In healthcare, management of legacy systems is therefore a growing challenge — when looking at not only obsolete digital applications but also infrastructure and, in the end, how healthcare providers purchase digital applications and platforms. When IT fails to deliver because of legacy and lack of agility, the business model and clinical processes become legacy as well. Key solutions such as EHR, RIS, PACS, VNA, ECM, advanced analytics, cloud, and mobility will fundamentally impact hospitals' digital strategies execution and success.This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806"), new Guid("e0821c76-d991-4df7-abdd-5654e28cc872"), null, null, null, new Guid("1d86b2df-8961-40d1-9806-57bf1e69ddd8"), true, false, false, null, "Health Insights: European Life Science and Pharma", "HEALTH INSIGHTS: EUROPEAN LIFE SCIENCE AND PHARMA", "Digital transformation is key in the European life science industry's ongoing efforts to transform itself as it seeks to find new paths to innovation, optimize operational efficiency and effectiveness, and regain long-term sustainability. The transition toward new care delivery ecosystems and outcome-based reimbursement models enhances life science organizations' needs to implement new approaches to information management and use. The digital mission in the life sciences is to integrate new and existing data, information, and knowledge into a more knowledge-centric approach to new drug and medical devices development, to product commercialization and management, and to treatment delivery. The European life science digital transformation service provides a forward-looking analysis of IT and technologies that are being adopted all along the value chain of the European life science industry. The service focuses on how European life science organizations can leverage the new range of technologies underpinned by the 3rd Platform technologies and innovation accelerators to support efforts to improve the operational efficiency of the industry and better engage with the ultimate end user of life science innovations, namely patients. This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae"), new Guid("e0821c76-d991-4df7-abdd-5654e28cc872"), null, null, null, new Guid("882bda0d-578d-44d5-b725-2abc36712488"), true, false, false, null, "Canadian Internet of Things Ecosystem and Trends", "CANADIAN INTERNET OF THINGS ECOSYSTEM AND TRENDS", "The Internet of Things (IoT) market is poised for exponential growth over the next several years. However, the IoT ecosystem is a complex segment with multiple layers and hundreds of players, including device and module manufacturers, communication service providers (CSPs), IoT platform players, applications, analytics and security software vendors, and professional services providers. 's Canadian Internet of Things Ecosystem and Trends service analyzes the regionally specific growth of this market from the autonomously connected units in the enterprise world to the applications and services that will demonstrate the power of a world of connected .Markets and Subjects Analyzed Market size and forecast for the Canadian IoT market Canadian IoT revenue forecast by industry Canadian IoT revenue forecast by use cases Taxonomy of the IoT ecosystem: How is the market organized, and what are the key segments and technologies? Canadian-specific demand-side data from various segments (e.g., IT and LOB) Analysis of Canadian-specific vendor readiness and go-to-market strategies for IoT Vertical use cases/case studies of IoT Core Research Taxonomy of the IoT Ecosystem Forecast for the Canadian IoT Market by Industry Forecast for the Canadian IoT Market by Use Cases Profiles of Canadian Vendors Leading IoT Evolution Case Studies of Successful Canadian IoT Implementations Canadian Internet of Things Decision-Maker Survey In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("cffed078-aa51-4fe2-913e-59e6e2624828"), new Guid("ebaf1989-5a9d-4dfc-979c-7bd7347fce9e"), null, null, null, new Guid("882bda0d-578d-44d5-b725-2abc36712488"), true, false, false, null, "Agile Application Life-Cycle, Quality and Portfolio Strategies", "AGILE APPLICATION LIFE-CYCLE, QUALITY AND PORTFOLIO STRATEGIES", "Agile Application Life-Cycle, Quality and Portfolio Strategies service provides insight into business and IT alignment through end-to-end application life-cycle management (ALM) and DevOps strategies including agile, IT portfolio management, software quality, testing and verification tools, software change and configuration, and continuous releases for digital transformation. This service provides insights on how product, organizational, and process transitions can help address regulatory compliance, security, and licensing issues covering cloud, SaaS, and open source software. It also analyzes trends related to ALM and software deployment with applications that leverage and target artificial intelligence (AI) and machine learning (ML), collaboration, mobile and embedded apps, IoT, virtualization, and complex sourcing and evolve to adaptive work, project and portfolio management (PPM), and agile adoption with organizational and process strategies (e.g., Scaled Agile Framework [SAFe]). Markets and Subjects Analyzed Software life-cycle process, requirements, configuration management and collaboration, and agile development and projects Software quality, including mobile, cloud, and embedded strategies PPM and work management software trends and strategies IT project and portfolio management software Developer, business, and operations trends Tools enabling DevOps and governance solutions to align complex deployment, financial management, infrastructure, and business priorities Trends in software configuration management (SCM), including connecting business to IT via requirements management and web services demands on agile life-cycle management solutions Automated software quality (ASQ) tools evolution: Hosted testing, web services, API and cloud testing, service virtualization, vulnerability, risk and value-based testing, and other emerging technologies (e.g., mobile, analysis, and metrics) Trends in collaborative software development with social media" },
                    { new Guid("4a817d53-485b-4634-9c09-22eb56d367fc"), new Guid("400ef15e-4eca-4d4e-9d96-bdc21028277f"), null, null, null, new Guid("882bda0d-578d-44d5-b725-2abc36712488"), true, false, false, null, "Software Channels and Ecosystems", "SOFTWARE CHANNELS AND ECOSYSTEMS", "Software Channels and Ecosystems research service offers intelligence and expertise to help channel executives and program managers develop, implement, support, and manage effective channel strategies and programs to drive successful relationships with the spectrum of partner activities (e.g., resale, managed/cloud services, consulting professional services, and software development). In addition, it provides a comprehensive view of the value of the indirect software market and its leading vendor proponents. This service also identifies and analyzes key industry trends and their impact on channel relationship drivers and channel partner business models. Subscribers are invited to 's semiannual Software Channel Leadership Council where and channel executives present and discuss key industry issues. Markets and Subjects Analyzed Multinational software vendors Digital transformation in the partner ecosystem Software and SaaS channel revenue flow Digital marketplaces Cloud and channels Partner-to-partner networking Key channel trends Software partner program profiles and innovative practices Core Research Channel Revenue Forecast and Analysis Partner Marketing and Communications Ecosystem Digital Transformation Cloud and Channels Emerging Partner Business Models Partner Collaboration." },
                    { new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193"), new Guid("400ef15e-4eca-4d4e-9d96-bdc21028277f"), null, null, null, new Guid("882bda0d-578d-44d5-b725-2abc36712488"), true, false, false, null, "Multicloud Data Management and Protection", "MULTICLOUD DATA MANAGEMENT AND PROTECTION", "Multicloud Data Management and Protection service enables storage vendors and IT users to have a more comprehensive view of the evolving data protection, availability, and recovery market. Integrating cloud technology with traditional hardware and software components, the report series will include market forecasts and provide a combination of timely tactical market information and long-term strategic analysis. Markets and Subjects Analyzed Cloud-based solutions, including backup as a service (BaaS) and disaster recovery as a service (DRaaS) Public, private, and hybrid cloud data protection technologies Disk-based data protection and recovery solutions packaged as software, appliance, or gateway system with a focus on technology evolution Market forecast based on total terabytes shipped and revenue for disk-based data protection and recovery (No vendor shares will be published.) End-user adoption of different data protection and recovery technologies and solutions Cloud service providers delivering cloud-based data protection Data protection for nontraditional data types (e.g., Mongo DB, Cassandra, Hadoop) Vendors delivering disk-based data protection and recovery solutions with focus on architecture based on use cases and applications Technologies and processes, including backup, snapshots, replication, continuous data protection, and data deduplication Impact of purpose-built backup appliances (PBBAs) and hyperconverged systems on overall market growth and customer adoption Implications of copy data management and impact on data availability, capacity management, cost, governance, and security The evolutionary impact of virtual infrastructure and object storage on data protection schemes Implications of flash technology for data protection Data protection best practice guidelines and adoption Core Research Market Analysis, Sizing, and Vendor Shares of Backup as a Service and Disaster Recovery as a Service Use of Disk-Based Technologies for Protection and Recovery Adoption Patterns and Role of PBBAs in Customer Environments End-User Needs and Requirements Data Protection and Recovery Software Market Size, Shares, and Forecasts Disk-Based Data Protection Market Size and Forecast Virtual Backup Appliance Solutions Intersection of Data Protection Software and Management with Cloud Services In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("aba89aa6-e637-4518-83c1-125b625add12"), new Guid("cf20fa7b-5e0e-4f63-a608-d17a31057370"), null, null, null, new Guid("882bda0d-578d-44d5-b725-2abc36712488"), true, false, false, null, "DevOps Analytics, Automation and Security", "DEVOPS ANALYTICS, AUTOMATION AND SECURITY", "DevOps is a powerful modern approach to unifying the business strategy, development, testing, deployment, and life-cycle operation of software. This approach is accomplished by improving business, IT, and development collaboration while taking full advantage of automation technologies, end-to-end processes, microservices architecture, and cloud infrastructure to accelerate development and delivery and enable innovation. This subscription service provides insight, forecasts, and thought leadership to assist IT management, IT professionals, IT vendors, and service providers in creating compelling DevOps strategies and solutions. Markets and Subjects Analyzed DevOps adoption drivers, benefits, and use cases Identification of DevOps innovators and best practices Identification of critical DevOps tools enabling automation, open source technologies, and market leaders Analysis of the impact DevOps is having on IT infrastructure hardware and software purchasing and deployment priorities across on-premises and cloud service platforms Major transformation and impact DevOps is having on staffing, skills, and internal processes Core Research Worldwide DevOps Software Forecast, 2018-2022 Worldwide DevOps Software Market Share, 2018-2022 The Role of Cloud-Based IDEs to Drive DevOps Adoption Changing Drivers of DevOps: Role of Serverless and Microservices/Cloud-Native Architectures Market Analysis Perspective: Worldwide Developer, Operations, and DevOps Evolving Approaches and Practices in DevOps The Shift to Continuous Delivery and Deployment Tools and Frameworks for Supporting DevOps Workflows In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd"), new Guid("99c3b74a-0e9e-4ebc-b737-a11906b3ffc5"), null, null, null, new Guid("882bda0d-578d-44d5-b725-2abc36712488"), true, false, false, null, "Cybersecurity Analytics, Intelligence, Response and Orchestration", "CYBERSECURITY ANALYTICS, INTELLIGENCE, RESPONSE AND ORCHESTRATION", "Cybersecurity Analytics, Intelligence, Response and Orchestration service covers security software and hardware products related to analytic security platforms, security and vulnerability management (SVM), and security orchestration platforms. Specific functions covered include vulnerability management and intelligence, SIEM, security analytics, threat hunting, incident detection and response, and orchestration. The service is designed to create in-depth coverage of the analytic-based and platform security markets. Markets and Subjects Analyzed Vulnerability management SIEM Security analytics Incident detection and response Threat analytics Automation Orchestration Core Research Security AIRO Forecast Security AIRO Market Share SIEM Market Share and Forecast Market MAP In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment. Key Questions Answered What is the size and market opportunity for security analytics solutions? Who are the major players in the security analytics space? What is the size and market opportunity for security orchestration solutions? What is the size and market opportunity for threat analytics solutions? How has the competitive landscape changed through digital transformation and adoption of cloud and enabling technologies?" },
                    { new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0"), new Guid("400ef15e-4eca-4d4e-9d96-bdc21028277f"), null, null, null, new Guid("bd529ec1-3ab4-4c28-87d3-5ff76cf85498"), true, false, false, null, "North America Utilities Digital Transformation Tactics", "ENERGY INSIGHTS: NORTH AMERICA UTILITIES DIGITAL TRANSFORMATION TACTICS", "Digital transformation is driving utilities to change their business and operating and information models. The Energy Insights: North America Utilities Digital Transformation Strategies service is designed to help North American utility IT and business management understand the disruptions that are transforming the energy and utility value chains and develop the strategies and programs to capitalize on the evolving opportunities. The service provides deep research and insight on the key topics that matter to North American utility decision makers and IT executives affected by the digital transformation. Through an integrated set of deliverables, subscribers gain access to analysis and insights on the impact of new technologies, IT strategies and best practices, and the industry, governmental, and regulatory forces that are shaping the evolution to new industry business models in the industry's generation, transmission and distribution/pipelines, and retail sectors. This service delivers research, insight, and guidance on IT strategy and investments to meet the utility's most critical corporate objectives." },
                    { new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069"), new Guid("e0821c76-d991-4df7-abdd-5654e28cc872"), null, null, null, new Guid("bd529ec1-3ab4-4c28-87d3-5ff76cf85498"), true, false, false, null, "Worldwide Oil and Gas Downstream IT Blueprint", "WORLDWIDE OIL AND GAS DOWNSTREAM IT BLUEPRINT", "Energy Insights: Worldwide Oil and Gas Downstream IT Strategies advisory research service takes the worldwide oil and gas IT strategies research and applies it to the downstream operations in oil and gas. The research focuses on companies that are involved in the production for sale of all petroleum products, from incoming crude to outgoing product. It will also encompass the technology companies that provide the technology foundation for the operation of the downstream business. Energy Insights: Worldwide Oil and Gas Downstream IT Strategies will focus its analysis on the digital transformation (DX) use cases, priorities, innovation technology, and road map for all aspect of companies that produce and distribute petroleum products. It will provide technology vendors a road map for product development and messaging in oil and gas downstream. Throughout the year, this service will address the following topics: Digital transformation – Investment priorities and road maps for transformation of the downstream business operation Operations technology — IT/OT convergence investments that help downstream organizations transform how they operate the production lines Asset management – Technology changes driving asset management organization and transformation Transformation to scale – Development of a transformed infrastructure to scale to market volatility Our research addresses the following issues that are critical to your success: What is the impact of transformational technologies on the oil and gas downstream DX road map? How does an oil and gas downstream company organize itself around the Future of Work? What are the market trends in oil and gas downstream that affect the use case prioritization for DX? How should trading partners be integrated into the DX road map? Who are the technology vendors that can support DX in oil and gas downstream?" },
                    { new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a"), new Guid("400ef15e-4eca-4d4e-9d96-bdc21028277f"), null, null, null, new Guid("bd529ec1-3ab4-4c28-87d3-5ff76cf85498"), true, false, false, null, "Global Utilities Customer Experience Scheme", "GLOBAL UTILITIES CUSTOMER EXPERIENCE SCHEME", "With the evolution of energy and water markets and the possibilities offered by digital transformation, utilities are developing new business models and are focusing on providing customers more interactive experiences. Smart metering, analytics, social media, mobility, cloud, and IoT are fundamentally impacting customer operation practices in both competitive and regulated markets. The Energy Insights: Worldwide Utilities Customer Experience Strategies service is designed to help utilities and energy retailers servicing customers in competitive and regulated markets at the worldwide level (including electricity, gas, and water). The service provides exclusive research and direct access to experts providing guidance to make the right IT investments and meet corporate objectives of customer satisfaction, reduction of cost to serve, and innovation. This service develops unique analysis and comprehensive data through Energy Insights' proprietary research projects, along with ongoing communications with industry experts, utility management, ICT vendors, and service providers. With decades of experience in the utility industry, our analysts leverage a broad spectrum of expertise and intellectual property from Energy Insights. Research reports elucidate business strategy, best practices, technology selection, and vendor assessment, along with short perspectives on topical issues. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("76352896-e547-485f-ae20-73b4f05b664f"), new Guid("cf20fa7b-5e0e-4f63-a608-d17a31057370"), null, null, null, new Guid("bd529ec1-3ab4-4c28-87d3-5ff76cf85498"), true, false, false, null, "Asia/Pacific Oil and Gas Digital Transformation Procedure", "ASIA/PACIFIC OIL AND GAS DIGITAL TRANSFORMATION PROCEDURE", "Asia/Pacific Oil and Gas Digital Transformation Strategies examines the business environment of the oil and gas industry in the Asia/Pacific region, excluding Japan. The service covers the whole value chain from upstream, midstream, and downstream but is focused on upstream. It seeks to support companies working to deliver against efficiency objectives through technology-led change that will deliver improved decision support and insight and flexibility to adjust to changing technology, markets, and political pressures. The Energy Insights: Asia/Pacific Oil and Gas Digital Transformation Strategies program provides advice on best practices and technology priorities that will support technology decision making, particularly, in relation to innovation, data management, and greater integration across silos and processes. The service is designed to support oil and gas companies with market insights and road mapping advice relating to technology decision making, particularly delivering insight to how increased value from investments can be delivered through better understanding of new metric, organization, talent, and process change. Key technology focus areas of the program include IT/OT integration, operational cybersecurity, IoT strategies, and asset management priorities. This service is built on the foundations of extensive engagement and research by the Energy Insights team across the oil and gas industry in this region. Our analysts work with industry experts, staff from the oil and gas business, and technology vendors to ensure that the research service is relevant and prioritizes areas that are of importance to them and the industry at large." },
                    { new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d"), new Guid("400ef15e-4eca-4d4e-9d96-bdc21028277f"), null, null, null, new Guid("bd529ec1-3ab4-4c28-87d3-5ff76cf85498"), true, false, false, null, "Worldwide Mining Policy", "WORLDWIDE MINING POLICY", "Worldwide Mining Strategies examines the business environment across the mining sector value chain from exploration through to operations, processing, supply chain, and trading globally. Mining companies are operating in an increasingly competitive commodity market environment, driven by pressures relating to accessing funding, assets, and talent. Technology and data are playing an increasingly critical role across the operation to enable decision support, automation, integration, and control. This service provides comprehensive insights into the best practices that show how mining companies are responding and what road maps these companies will need to build the information technology (IT) capabilities required to create integrated, agile, and responsive operations. Mining companies are changing the way they buy technology, how they collaborate to frame the problems to be solved, how they engage with technology suppliers, and how they innovate across their businesses bringing together the capabilities of IT and operational technology (OT). This service tracks the IT investment priorities for organizations seeking to scale value creation across their organization and the impact on decision making and best practices relating to technology, process, and organizational change. This service distills market and industry data into incisive analysis drawn from in-depth interviews with industry experts, mining staff from across the business, and technology vendors. Insight and analysis are further supported and validated through rigorous research methodologies in quantitative market analysis.  Energy Insights' analysts develop unique and comprehensive analyses of this data, focused on providing actionable recommendations. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("11dcaf06-8a35-403a-8262-ebd98344039a"), new Guid("99c3b74a-0e9e-4ebc-b737-a11906b3ffc5"), null, null, null, new Guid("28d170e0-dcea-4d55-abad-3190164dc74c"), true, false, false, null, "Digital Commerce Trends", "DIGITAL COMMERCE TRENDS", "The Digital Commerce subscription marketing intelligence service examines the competitive landscape, key trends, and differentiating factors of digital commerce, PIM, and CPQ application vendors; digital marketplaces; and business commerce networks. This includes the buying behavior of end users while purchasing products and services on digital commerce platforms. The service provides a worldwide perspective by looking at all forms of digital commerce transactions — including B2B, B2C, C2C, G2B, and B2B2C — across all vertical industries and regional markets. Markets and Subjects Analyzed. Digital commerce applications targeting businesses of all sizes and industries. Best-of-breed digital commerce applications in areas such as CPQ, payments and billing, order management, web content management, merchandising, site search, fulfillment, product information management, and inventory management. Business commerce networks that bring together functional applications for enterprise asset management, procurement, financials, sales, and human capital management.Enterprise partnership/integration strategies among digital commerce and marketing/content management vendors. Experience management — across channels (includes content management, commerce platforms, integration, and advanced analytics) for B2B, B2C, and B2B2C. Impact of cloud, social, mobile, and Big Data technologies on vendor strategies for employee, customer, supplier, partner, and asset engagement. Proliferation of public cloud microservice architectures. Mobile commerce (mobile app marketplaces, built-for-mobile commerce capabilities, and differentiating technology). Cognitive technologies and intelligent workflow impact on digital commerce applications" },
                    { new Guid("67966830-655d-42f7-9424-ef5cf441ff8d"), new Guid("cf20fa7b-5e0e-4f63-a608-d17a31057370"), null, null, null, new Guid("28d170e0-dcea-4d55-abad-3190164dc74c"), true, false, false, null, "Canadian Sales Accelerator: Datacenter Infrastructure", "CANADIAN SALES ACCELERATOR: DATACENTER INFRASTRUCTURE", "Canadian Sales Accelerator: Datacenter Infrastructure research program analyzes the Canadian datacenter (DC) infrastructure market in Canada. Designed to provide intelligence and strategic frameworks to technology sales professionals, field marketing teams, and channel managers to take action on key responsibilities related to the sales cycle, this service provides market sizing, vendor performance, forecasts, and market opportunities, as well as a variety of adjacent markets and segmentations from a quantitative standpoint. This advisory service will allow subscribers to identify opportunities tied to the future of infrastructure, tailor go-to-market strategies as well as product and services road maps to meet the growing demand of end users for infrastructure solutions. At the same time, it looks at vendors, partners, and customers from a qualitative standpoint, concerning needs and requirements, pain points and buying intentions, maturity levels, and adoption in and of new technologies. Markets and Subjects Analyzed Datacenter technologies (including software defined); server and storage; converged systems. Infrastructure ecosystem including partners and channels. Datacenter budget trends and dynamics and vendor selection/buying criteria. Technology investment expectations for legacy and next-gen DC infrastructure. In addition to the core research documents, clients will receive briefings and concise sales executive email alerts throughout the year. Every client will have a Sales Accelerator service launch integration meeting to kick-off the program. Core Research. Brand Perceptions on Enterprise Storage and Service Vendors. Market Forecasts. Vendor Dashboard: Market Shares. Infrastructure Ecosystem Barometer. Executive Market Insights" }
                });

            migrationBuilder.InsertData(
                table: "ReportTags",
                columns: new[] { "TagId", "ReportId" },
                values: new object[,]
                {
                    { new Guid("050c2347-2842-44db-bffa-065a874c148a"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") },
                    { new Guid("8c6a0203-ba5f-4f52-8bf2-2639df7c4376"), new Guid("180360e0-6db8-412e-a6f4-3d68a939c583") },
                    { new Guid("d79dc940-e7c4-4299-8f75-7a1951697869"), new Guid("180360e0-6db8-412e-a6f4-3d68a939c583") },
                    { new Guid("8c6a0203-ba5f-4f52-8bf2-2639df7c4376"), new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0") },
                    { new Guid("88def612-ee91-47b5-88d1-572be26187f0"), new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0") },
                    { new Guid("d38f1457-19f5-4e0b-b210-9855754ee9fb"), new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37") },
                    { new Guid("a01b7213-9822-4a2c-9d7c-5ab9a298867b"), new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37") },
                    { new Guid("050c2347-2842-44db-bffa-065a874c148a"), new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82") },
                    { new Guid("f45de922-c57d-4d6c-a011-585d13eb8404"), new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82") },
                    { new Guid("d38f1457-19f5-4e0b-b210-9855754ee9fb"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") },
                    { new Guid("f45de922-c57d-4d6c-a011-585d13eb8404"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") },
                    { new Guid("8411dc1c-c965-4765-9fbf-b861c67ed887"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") },
                    { new Guid("a01b7213-9822-4a2c-9d7c-5ab9a298867b"), new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015") },
                    { new Guid("8411dc1c-c965-4765-9fbf-b861c67ed887"), new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015") },
                    { new Guid("d79dc940-e7c4-4299-8f75-7a1951697869"), new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2") },
                    { new Guid("f45de922-c57d-4d6c-a011-585d13eb8404"), new Guid("d1b04963-e698-41bf-aded-25bb755c2877") },
                    { new Guid("d38f1457-19f5-4e0b-b210-9855754ee9fb"), new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97") },
                    { new Guid("f45de922-c57d-4d6c-a011-585d13eb8404"), new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97") },
                    { new Guid("8411dc1c-c965-4765-9fbf-b861c67ed887"), new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef") },
                    { new Guid("c56df33e-3e01-4025-9a8e-7f802781540f"), new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef") },
                    { new Guid("71d6a5e2-d611-41c4-a110-609ce4ff30da"), new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02") },
                    { new Guid("6a951858-7b08-4a06-b02b-c9b5a69622eb"), new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02") },
                    { new Guid("71d6a5e2-d611-41c4-a110-609ce4ff30da"), new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7") },
                    { new Guid("c56df33e-3e01-4025-9a8e-7f802781540f"), new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7") },
                    { new Guid("8411dc1c-c965-4765-9fbf-b861c67ed887"), new Guid("9062907e-6a91-4737-95f9-8db332df47f4") },
                    { new Guid("71d6a5e2-d611-41c4-a110-609ce4ff30da"), new Guid("9062907e-6a91-4737-95f9-8db332df47f4") },
                    { new Guid("050c2347-2842-44db-bffa-065a874c148a"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") },
                    { new Guid("71d6a5e2-d611-41c4-a110-609ce4ff30da"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") },
                    { new Guid("6a951858-7b08-4a06-b02b-c9b5a69622eb"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") },
                    { new Guid("8411dc1c-c965-4765-9fbf-b861c67ed887"), new Guid("d1b04963-e698-41bf-aded-25bb755c2877") },
                    { new Guid("0262675b-27d3-4a26-b5b1-9bd895effd4a"), new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2") },
                    { new Guid("050c2347-2842-44db-bffa-065a874c148a"), new Guid("0070a7dc-c760-4a34-8c87-d4747824191b") },
                    { new Guid("8c6a0203-ba5f-4f52-8bf2-2639df7c4376"), new Guid("0070a7dc-c760-4a34-8c87-d4747824191b") },
                    { new Guid("5aea1ce4-d3a7-43e5-965c-cf779b903d70"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") },
                    { new Guid("f62be2ad-e86b-417f-9c90-13e38e4d86aa"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") },
                    { new Guid("ce9e44c6-dafb-4389-aea9-84c2b5967f92"), new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d") },
                    { new Guid("1cc2c117-df96-4b62-b17a-bdff62e9c09e"), new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d") },
                    { new Guid("050c2347-2842-44db-bffa-065a874c148a"), new Guid("76352896-e547-485f-ae20-73b4f05b664f") },
                    { new Guid("ce9e44c6-dafb-4389-aea9-84c2b5967f92"), new Guid("76352896-e547-485f-ae20-73b4f05b664f") },
                    { new Guid("f62be2ad-e86b-417f-9c90-13e38e4d86aa"), new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a") },
                    { new Guid("1cc2c117-df96-4b62-b17a-bdff62e9c09e"), new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a") },
                    { new Guid("5aea1ce4-d3a7-43e5-965c-cf779b903d70"), new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069") },
                    { new Guid("ce9e44c6-dafb-4389-aea9-84c2b5967f92"), new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069") },
                    { new Guid("5aea1ce4-d3a7-43e5-965c-cf779b903d70"), new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0") },
                    { new Guid("f62be2ad-e86b-417f-9c90-13e38e4d86aa"), new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0") },
                    { new Guid("30f56b58-45a7-4f98-a523-7813de98325f"), new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd") },
                    { new Guid("cb0bd7f4-3134-494b-89cc-a5d2a01e0ade"), new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd") },
                    { new Guid("5d97d07c-9c8b-465f-affc-87b1ce805cf4"), new Guid("aba89aa6-e637-4518-83c1-125b625add12") },
                    { new Guid("cb0bd7f4-3134-494b-89cc-a5d2a01e0ade"), new Guid("aba89aa6-e637-4518-83c1-125b625add12") },
                    { new Guid("718be14d-318c-4f66-afa9-995e1d66ff1c"), new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193") },
                    { new Guid("cb0bd7f4-3134-494b-89cc-a5d2a01e0ade"), new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193") },
                    { new Guid("30f56b58-45a7-4f98-a523-7813de98325f"), new Guid("4a817d53-485b-4634-9c09-22eb56d367fc") },
                    { new Guid("cb0bd7f4-3134-494b-89cc-a5d2a01e0ade"), new Guid("4a817d53-485b-4634-9c09-22eb56d367fc") },
                    { new Guid("5d97d07c-9c8b-465f-affc-87b1ce805cf4"), new Guid("cffed078-aa51-4fe2-913e-59e6e2624828") },
                    { new Guid("718be14d-318c-4f66-afa9-995e1d66ff1c"), new Guid("cffed078-aa51-4fe2-913e-59e6e2624828") },
                    { new Guid("5d97d07c-9c8b-465f-affc-87b1ce805cf4"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") },
                    { new Guid("30f56b58-45a7-4f98-a523-7813de98325f"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") },
                    { new Guid("cb0bd7f4-3134-494b-89cc-a5d2a01e0ade"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") },
                    { new Guid("050c2347-2842-44db-bffa-065a874c148a"), new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806") },
                    { new Guid("88def612-ee91-47b5-88d1-572be26187f0"), new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806") },
                    { new Guid("050c2347-2842-44db-bffa-065a874c148a"), new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244") },
                    { new Guid("0262675b-27d3-4a26-b5b1-9bd895effd4a"), new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244") },
                    { new Guid("6a951858-7b08-4a06-b02b-c9b5a69622eb"), new Guid("67966830-655d-42f7-9424-ef5cf441ff8d") },
                    { new Guid("c56df33e-3e01-4025-9a8e-7f802781540f"), new Guid("67966830-655d-42f7-9424-ef5cf441ff8d") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("0262675b-27d3-4a26-b5b1-9bd895effd4a"), new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("0262675b-27d3-4a26-b5b1-9bd895effd4a"), new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("050c2347-2842-44db-bffa-065a874c148a"), new Guid("0070a7dc-c760-4a34-8c87-d4747824191b") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("050c2347-2842-44db-bffa-065a874c148a"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("050c2347-2842-44db-bffa-065a874c148a"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("050c2347-2842-44db-bffa-065a874c148a"), new Guid("76352896-e547-485f-ae20-73b4f05b664f") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("050c2347-2842-44db-bffa-065a874c148a"), new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("050c2347-2842-44db-bffa-065a874c148a"), new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("050c2347-2842-44db-bffa-065a874c148a"), new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("1cc2c117-df96-4b62-b17a-bdff62e9c09e"), new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("1cc2c117-df96-4b62-b17a-bdff62e9c09e"), new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("30f56b58-45a7-4f98-a523-7813de98325f"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("30f56b58-45a7-4f98-a523-7813de98325f"), new Guid("4a817d53-485b-4634-9c09-22eb56d367fc") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("30f56b58-45a7-4f98-a523-7813de98325f"), new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5aea1ce4-d3a7-43e5-965c-cf779b903d70"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5aea1ce4-d3a7-43e5-965c-cf779b903d70"), new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5aea1ce4-d3a7-43e5-965c-cf779b903d70"), new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5d97d07c-9c8b-465f-affc-87b1ce805cf4"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5d97d07c-9c8b-465f-affc-87b1ce805cf4"), new Guid("aba89aa6-e637-4518-83c1-125b625add12") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5d97d07c-9c8b-465f-affc-87b1ce805cf4"), new Guid("cffed078-aa51-4fe2-913e-59e6e2624828") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("6a951858-7b08-4a06-b02b-c9b5a69622eb"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("6a951858-7b08-4a06-b02b-c9b5a69622eb"), new Guid("67966830-655d-42f7-9424-ef5cf441ff8d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("6a951858-7b08-4a06-b02b-c9b5a69622eb"), new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("718be14d-318c-4f66-afa9-995e1d66ff1c"), new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("718be14d-318c-4f66-afa9-995e1d66ff1c"), new Guid("cffed078-aa51-4fe2-913e-59e6e2624828") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("71d6a5e2-d611-41c4-a110-609ce4ff30da"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("71d6a5e2-d611-41c4-a110-609ce4ff30da"), new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("71d6a5e2-d611-41c4-a110-609ce4ff30da"), new Guid("9062907e-6a91-4737-95f9-8db332df47f4") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("71d6a5e2-d611-41c4-a110-609ce4ff30da"), new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("8411dc1c-c965-4765-9fbf-b861c67ed887"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("8411dc1c-c965-4765-9fbf-b861c67ed887"), new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("8411dc1c-c965-4765-9fbf-b861c67ed887"), new Guid("9062907e-6a91-4737-95f9-8db332df47f4") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("8411dc1c-c965-4765-9fbf-b861c67ed887"), new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("8411dc1c-c965-4765-9fbf-b861c67ed887"), new Guid("d1b04963-e698-41bf-aded-25bb755c2877") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("88def612-ee91-47b5-88d1-572be26187f0"), new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("88def612-ee91-47b5-88d1-572be26187f0"), new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("8c6a0203-ba5f-4f52-8bf2-2639df7c4376"), new Guid("0070a7dc-c760-4a34-8c87-d4747824191b") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("8c6a0203-ba5f-4f52-8bf2-2639df7c4376"), new Guid("180360e0-6db8-412e-a6f4-3d68a939c583") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("8c6a0203-ba5f-4f52-8bf2-2639df7c4376"), new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a01b7213-9822-4a2c-9d7c-5ab9a298867b"), new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a01b7213-9822-4a2c-9d7c-5ab9a298867b"), new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("c56df33e-3e01-4025-9a8e-7f802781540f"), new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("c56df33e-3e01-4025-9a8e-7f802781540f"), new Guid("67966830-655d-42f7-9424-ef5cf441ff8d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("c56df33e-3e01-4025-9a8e-7f802781540f"), new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("cb0bd7f4-3134-494b-89cc-a5d2a01e0ade"), new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("cb0bd7f4-3134-494b-89cc-a5d2a01e0ade"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("cb0bd7f4-3134-494b-89cc-a5d2a01e0ade"), new Guid("4a817d53-485b-4634-9c09-22eb56d367fc") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("cb0bd7f4-3134-494b-89cc-a5d2a01e0ade"), new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("cb0bd7f4-3134-494b-89cc-a5d2a01e0ade"), new Guid("aba89aa6-e637-4518-83c1-125b625add12") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("ce9e44c6-dafb-4389-aea9-84c2b5967f92"), new Guid("76352896-e547-485f-ae20-73b4f05b664f") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("ce9e44c6-dafb-4389-aea9-84c2b5967f92"), new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("ce9e44c6-dafb-4389-aea9-84c2b5967f92"), new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("d38f1457-19f5-4e0b-b210-9855754ee9fb"), new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("d38f1457-19f5-4e0b-b210-9855754ee9fb"), new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("d38f1457-19f5-4e0b-b210-9855754ee9fb"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("d79dc940-e7c4-4299-8f75-7a1951697869"), new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("d79dc940-e7c4-4299-8f75-7a1951697869"), new Guid("180360e0-6db8-412e-a6f4-3d68a939c583") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f45de922-c57d-4d6c-a011-585d13eb8404"), new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f45de922-c57d-4d6c-a011-585d13eb8404"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f45de922-c57d-4d6c-a011-585d13eb8404"), new Guid("d1b04963-e698-41bf-aded-25bb755c2877") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f45de922-c57d-4d6c-a011-585d13eb8404"), new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f62be2ad-e86b-417f-9c90-13e38e4d86aa"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f62be2ad-e86b-417f-9c90-13e38e4d86aa"), new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f62be2ad-e86b-417f-9c90-13e38e4d86aa"), new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a") });

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("0070a7dc-c760-4a34-8c87-d4747824191b"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("11dcaf06-8a35-403a-8262-ebd98344039a"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("180360e0-6db8-412e-a6f4-3d68a939c583"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("4a817d53-485b-4634-9c09-22eb56d367fc"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("67966830-655d-42f7-9424-ef5cf441ff8d"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("76352896-e547-485f-ae20-73b4f05b664f"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("9062907e-6a91-4737-95f9-8db332df47f4"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("aba89aa6-e637-4518-83c1-125b625add12"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("cffed078-aa51-4fe2-913e-59e6e2624828"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("d1b04963-e698-41bf-aded-25bb755c2877"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("0262675b-27d3-4a26-b5b1-9bd895effd4a"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("050c2347-2842-44db-bffa-065a874c148a"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("1cc2c117-df96-4b62-b17a-bdff62e9c09e"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("30f56b58-45a7-4f98-a523-7813de98325f"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("5aea1ce4-d3a7-43e5-965c-cf779b903d70"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("5d97d07c-9c8b-465f-affc-87b1ce805cf4"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("6a951858-7b08-4a06-b02b-c9b5a69622eb"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("718be14d-318c-4f66-afa9-995e1d66ff1c"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("71d6a5e2-d611-41c4-a110-609ce4ff30da"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("8411dc1c-c965-4765-9fbf-b861c67ed887"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("88def612-ee91-47b5-88d1-572be26187f0"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("8c6a0203-ba5f-4f52-8bf2-2639df7c4376"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("a01b7213-9822-4a2c-9d7c-5ab9a298867b"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("c56df33e-3e01-4025-9a8e-7f802781540f"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("cb0bd7f4-3134-494b-89cc-a5d2a01e0ade"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("ce9e44c6-dafb-4389-aea9-84c2b5967f92"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("d38f1457-19f5-4e0b-b210-9855754ee9fb"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("d79dc940-e7c4-4299-8f75-7a1951697869"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("f45de922-c57d-4d6c-a011-585d13eb8404"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("f62be2ad-e86b-417f-9c90-13e38e4d86aa"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("400ef15e-4eca-4d4e-9d96-bdc21028277f"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("99c3b74a-0e9e-4ebc-b737-a11906b3ffc5"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("cf20fa7b-5e0e-4f63-a608-d17a31057370"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("e0821c76-d991-4df7-abdd-5654e28cc872"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("ebaf1989-5a9d-4dfc-979c-7bd7347fce9e"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("1d86b2df-8961-40d1-9806-57bf1e69ddd8"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("28d170e0-dcea-4d55-abad-3190164dc74c"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("882bda0d-578d-44d5-b725-2abc36712488"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("abc595f0-84cc-493c-9792-b4b40cbd5900"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("bd529ec1-3ab4-4c28-87d3-5ff76cf85498"));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("297d06e6-c058-486f-a18a-06a971ebfcd7"),
                column: "ConcurrencyStamp",
                value: "265084db-0fe4-4d01-9159-c3336a240c60");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("6c8fcd7e-62f6-4f3e-a73d-acbfd60b97ab"),
                column: "ConcurrencyStamp",
                value: "ee65b0e5-ae8a-4d89-afab-29356d234288");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7f66990c-36bc-4381-9f81-32e06e168319"),
                column: "ConcurrencyStamp",
                value: "858fe15e-3874-4c19-8808-0f681907eadc");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb93"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "Fullname", "PasswordHash" },
                values: new object[] { "7a8649da-c7a2-49ec-a193-31f6f6a5d463", new DateTime(2020, 6, 1, 10, 55, 13, 157, DateTimeKind.Utc).AddTicks(5811), null, "AQAAAAEAACcQAAAAENBK3grsctEAq+mIgw4hVHd2B9OFfO8fCi8B+SRjs+GxNd/Mnz2URIPSO4h9sgxVtw==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb94"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "Fullname", "PasswordHash" },
                values: new object[] { "78b77cf1-ca20-4a60-84ad-2b1fb38c8b89", new DateTime(2020, 6, 1, 10, 55, 13, 157, DateTimeKind.Utc).AddTicks(8493), null, "AQAAAAEAACcQAAAAEFKNkx4nLfRXkyu8m2nUhLiNoCe3Gu338rfc5ns/vizDa8ii3Gj9mpCj0UJXkGnyag==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb95"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "Fullname", "PasswordHash" },
                values: new object[] { "be538579-3c07-4e98-8e35-99e782b6ff7a", new DateTime(2020, 6, 1, 10, 55, 13, 157, DateTimeKind.Utc).AddTicks(8521), null, "AQAAAAEAACcQAAAAED9KbGhlqwaw0figqrAOjRHs5WgHW8SoKdV7Eh+VpchMAcKuXPWm+qufC/q3N2E7Ag==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb96"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "Fullname", "PasswordHash" },
                values: new object[] { "ff5be634-fc8b-42ab-a30b-c4734534ebd8", new DateTime(2020, 6, 1, 10, 55, 13, 157, DateTimeKind.Utc).AddTicks(8528), null, "AQAAAAEAACcQAAAAEO+SVepoMZV80l4TXL/YbQjaTQZGb5IybFaddVUfDZeFW9LBW3P52e4n7OasqGAE3w==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb97"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "Fullname", "PasswordHash" },
                values: new object[] { "d2fe18b8-644b-4da6-a237-a66ac8b7e58e", new DateTime(2020, 6, 1, 10, 55, 13, 157, DateTimeKind.Utc).AddTicks(8538), null, "AQAAAAEAACcQAAAAEM2bsBHy5oelnPQ+lENS1d/TL1bIIiLVxRbwhE83FdPWmadFksJK6sV3gmI3597TnA==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb98"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "Fullname", "PasswordHash" },
                values: new object[] { "469949ab-cda7-4f8f-8299-8609d7ba82da", new DateTime(2020, 6, 1, 10, 55, 13, 157, DateTimeKind.Utc).AddTicks(8560), null, "AQAAAAEAACcQAAAAEDViMY+/1hXmlsHOGnO40ye2eed6L3GRPeCmCHhKdyFQXK8MuJXU1EHxFnBtUKF4TA==" });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "Bio", "ImagePath", "UserId" },
                values: new object[,]
                {
                    { new Guid("b3f88e39-6e58-4f50-a539-02780f00aeec"), "Jim Doe has been involved with ICT research in Africa since 1997 and has participated in diverse research projects in 14 African countries. He started his career in 1994 as a public relations officer with Software Technologies Limited (an East African Oracle distributor), before serving as a research analyst/project officer for Telecom Forum Africa in 1997.", "3.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb94") },
                    { new Guid("c0f3e2d5-33b1-454c-a7aa-f1ba51684e0a"), "Rosa James has a background in journalism and has been published in various local, regional, and international magazines and newspapers. She has spoken at various industry events across sub-Saharan Africa including countries like Ethiopia, Ghana, Kenya, Nigeria, Rwanda, Tanzania and Zambia as well as leading and facilitating at IDC’s events in the region. Rose is also a published author of a children’s novel.", "1.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb95") },
                    { new Guid("332ac8b8-a6b0-49ef-8007-dba8a8ad25ab"), "Cate Williams has been an analyst InsightHub for several years. Early on as an analyst she covered CRM applications, but more recently her work has been in integration middleware covering markets such as API management, file sync and share, and B2B integration.", "2.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb96") },
                    { new Guid("4ed92a05-a299-4b37-8040-7f9a29b9795b"), "Max Upton has more than 15 years of experience in business research, focusing on financial services and business innovation. His consulting work for InsightHub has allowed him to work closely with leading banks and regulators in the region for their technology and innovation strategies. Mr. Upton is concurrently head of InsightHub's business and operations in Thailand.", "7.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb97") },
                    { new Guid("4f06865f-a9d7-428a-956b-9a72268403ec"), "Previously, David Scott served for ten years as Enterprise Architect at CareFirst Blue Cross/Blue Shield, responsible for building business and technology alignment roadmaps for the ACA/HIX Health Reform, Mandated Implementations, PCMH, and Provider Network domains to ensure alignment of annual and tactical IT project planning with business goals.", "8.jpg", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb98") }
                });

            migrationBuilder.InsertData(
                table: "Industries",
                columns: new[] { "Id", "DeletedOn", "ImagePath", "IsDeleted", "ModifiedOn", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("0b3a1500-1c80-45da-9ea0-b054df73b99f"), null, null, false, null, "Energy", "ENERGY" },
                    { new Guid("d0a91bed-717c-4906-a230-b791e8119932"), null, null, false, null, "Information Technology", "INFORMATION TECHNOLOGY" },
                    { new Guid("ea56d76a-dedd-419f-946e-71f114733aff"), null, null, false, null, "Healthcare", "HEALTHCARE" },
                    { new Guid("426b4ff5-6787-4b81-abeb-f25d8a3fad83"), null, null, false, null, "Finance", "FINANCE" },
                    { new Guid("edf9d9bb-e1bb-42fa-b8e5-e8e1f88ecfb0"), null, null, false, null, "Marketing", "MARKETING" }
                });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "Id", "DeletedOn", "IsDeleted", "ModifiedOn", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("6879d04c-61a0-4195-96ab-0ec54585c1cd"), null, false, null, "computers", "COMPUTERS" },
                    { new Guid("92a409a6-5571-4b19-8cc5-3091ebb1d477"), null, false, null, "hospital", "HOSPITAL" },
                    { new Guid("962db5bb-b7dd-4b12-b992-0e91e9cc4a11"), null, false, null, "influence", "INFLUENCE" },
                    { new Guid("00150dd5-5388-456f-85d8-52af27a1ea0b"), null, false, null, "trend", "TREND" },
                    { new Guid("e0ec89ca-3408-4c23-b418-993f7f01d6f6"), null, false, null, "sales", "SALES" },
                    { new Guid("b2af9ae6-be5b-45d7-9df3-45cb1f8cbf6d"), null, false, null, "profit", "PROFIT" },
                    { new Guid("3e4b5376-5e4a-405f-a5e9-450d3cd63619"), null, false, null, "insurance", "INSURANCE" },
                    { new Guid("5e441759-d367-446f-8456-34d118b05561"), null, false, null, "stock", "STOCK" },
                    { new Guid("a6213e7c-8058-4fc8-999b-e071bc460520"), null, false, null, "nuclear", "NUCLEAR" },
                    { new Guid("737fb1e5-534a-4a4c-8f2a-2137c2e13c56"), null, false, null, "solar", "SOLAR" },
                    { new Guid("010fe3d8-635b-4335-b92b-8d748733ac3a"), null, false, null, "oil", "OIL" },
                    { new Guid("828fa2c8-26be-4cc7-9a67-f420e86b3330"), null, false, null, "laboratory", "LABORATORY" },
                    { new Guid("07d2b39c-f0cf-4112-a55a-8e5f5fd427b8"), null, false, null, "vaccine", "VACCINE" },
                    { new Guid("66358a59-4f29-4967-9aef-bc0e559e82e8"), null, false, null, "research", "RESEARCH" },
                    { new Guid("7cc33bae-61dd-4359-8b7c-7a11afd05c88"), null, false, null, "medicine", "MEDICINE" },
                    { new Guid("0af3dad4-8d94-47b4-85d4-62f8e18ef8ca"), null, false, null, "web", "WEB" },
                    { new Guid("b77ea49d-d372-4a0b-8b1c-3aab566a4216"), null, false, null, "hardware", "HARDWARE" },
                    { new Guid("776d42cb-e955-43dc-9a36-cc355d4309d3"), null, false, null, "software", "SOFTWARE" },
                    { new Guid("36833c8e-dd9b-4132-9459-7b61081c1600"), null, false, null, "investment", "INVESTMENT" },
                    { new Guid("1396511b-76e7-4f36-983e-1ebf5a1698dc"), null, false, null, "gas", "GAS" }
                });

            migrationBuilder.InsertData(
                table: "Reports",
                columns: new[] { "Id", "AuthorId", "Content", "DeletedOn", "ImagePath", "IndustryId", "IsApproved", "IsDeleted", "IsFeatured", "ModifiedOn", "Name", "NormalizedName", "Summary" },
                values: new object[,]
                {
                    { new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb"), new Guid("4f06865f-a9d7-428a-956b-9a72268403ec"), null, null, null, new Guid("0b3a1500-1c80-45da-9ea0-b054df73b99f"), true, false, false, null, "Worldwide Utilities Connected Asset", "WORLDWIDE UTILITIES CONNECTED ASSET", "Energy and digital technologies are radically transforming utilities asset operations and strategies. Renewables and decentralized power generation, energy delivery networks (electricity, gas, and heat), and water and waste water management benefit from the developments and adoption of IoT, AI, and digital twins.  Energy Insights: Worldwide Utilities Connected Asset Strategies service focuses on all these. It provides guidance to end users in terms of digital transformation (DX) use cases road map, analysis of emerging IT trends, and evaluation of technology providers in this space. It looks into the future of work in the utilities value chain context. At the same time, it provides technology vendors a view on utilities' operational excellence asset strategies addressing their go to market.This service develops unique analysis and comprehensive data through  Energy Insights' proprietary research projects, along with ongoing communications with industry experts, utility management, ICT vendors, and service providers. With decades of experience in the utility industry, our analysts leverage a broad spectrum of expertise and intellectual property from both  and  Energy Insights. Research documents elucidate business strategy, best practices, technology selection, and vendor assessment, along with short perspectives on topical issues. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research documents." },
                    { new Guid("9062907e-6a91-4737-95f9-8db332df47f4"), new Guid("4ed92a05-a299-4b37-8040-7f9a29b9795b"), null, null, null, new Guid("edf9d9bb-e1bb-42fa-b8e5-e8e1f88ecfb0"), true, false, false, null, "CMO Advisory Service", "CMO ADVISORY SERVICE", "CMO Advisory Service guides marketing leaders as they master the science of marketing. Digital transformation offers CMOs the opportunity to become revenue drivers and architects of the customer experience. Leaders leverage 's deep industry knowledge, powerful quantitative models, peer-tested practices, and personalized guidance to advance their operations. Whether seeking fresh perspectives on core challenges or counsel on emerging developments, offers a trusted source of insight.Markets and Subjects Analyzed. Marketing Investment and Transformational Operations. Planning and budgeting investments in the marketing mix, marketing technology, and marketing operations, accountability, and attribution. Application of New Technology for Marketing. Guidance on the implications of new technologies such as artificial intelligence (AI), collaboration, and marketing automation solutions. Delivering the New Customer Experience. Mapping and responding to the B2B customer decision journey. The Future Marketing Organization. Organizational design, staff allocation benchmarks, role definition, talent development, shared services, organizational alignment, and change management. Developing core competencies: Content marketing, customer intelligence and analytics, integrated digital and social engagement, sales enablement, and loyalty and advocacy" },
                    { new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7"), new Guid("4f06865f-a9d7-428a-956b-9a72268403ec"), null, null, null, new Guid("edf9d9bb-e1bb-42fa-b8e5-e8e1f88ecfb0"), true, false, false, null, "Marketing and Sales Solutions", "MARKETING AND SALES SOLUTIONS", "Marketing and sales technologies are driving forces for all companies as customers move to online, mobile-first, and collaborative relationship models. In reaching and selling to customers, it is essential that organizations aggressively develop the necessary operational and analytic skills, collaborative cultures, and creative problem solving needed to truly add value to the customer relationship. 's Marketing and Sales Solutions service provides strategic frameworks for thinking about the individual and aligned areas of marketing and sales technology as parts of a holistic business strategy. This program delivers insight, information, and data on the main drivers for the adoption of these technologies in the broader context of customer experience (CX) and networked business strategies.Markets and Subjects Analyzed. Marketing automation, campaign management, and go to market execution applications. Sales force automation applications. Predictive analytics and business KPIs. Mobile and digital applications and strategies. Customer data and analytics. CX strategies. Core Research. Reports on how 670 U.S. large enterprises use more than 300 vendors in 15 martech categories across 5 vertical markets:Consumer banking; Retail; CPG manufacturing; Securities and investment services; Travel and hospitality; MarketScape(s) on related solutions areas such as marketing clouds and artificial intelligence; TechScapes and PlanScapes on various topics such as GDPR, account-based marketing, personalization, and mobile marketing; Marketing software forecast and vendor shares; Sales force automation forecast and vendor shares. In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment. " },
                    { new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02"), new Guid("4ed92a05-a299-4b37-8040-7f9a29b9795b"), null, null, null, new Guid("edf9d9bb-e1bb-42fa-b8e5-e8e1f88ecfb0"), true, false, false, null, "Customer Experience Management Strategies", "CUSTOMER EXPERIENCE MANAGEMENT STRATEGIES", "Customer Experience Management Strategies SIS provides a framework and critical knowledge for understanding the changing nature of the customer experience (CX) and guides chief experience officers and their organizations as they master the digital transformation of the customer experience. This product covers the concepts of experience management and customer experience, looking at how digital transformation is driving change to customer expectations, preferences, and behavior and how enterprises must adopt new technologies to meet these urgent challenges. Markets and Subjects Analyzed: Experience management; Customer-centric engagement Customer-centric operations; Customer-centric solution design; Impact of 3rd Platform technologies on customer experience; Intelligence and analytics-driven customer experience; Customer experience along the customer journey; Customer experience benchmarks Core Research: Customer Experience Taxonomy; Digital Transformation of the Customer Experience; Redefining Experience Management; PeerScape: Customer Experience; Customer Experience in an Algorithm Economy; Technology-Enabled Storytelling; MaturityScape: Customer Experience; Customer Intelligence and Analytics; Innovation Accelerators in Customer Experience: Artificial Intelligence; In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef"), new Guid("4f06865f-a9d7-428a-956b-9a72268403ec"), null, null, null, new Guid("edf9d9bb-e1bb-42fa-b8e5-e8e1f88ecfb0"), true, false, false, null, "Retail Insights: European Retail Digital Transformation Strategies", "RETAIL INSIGHTS: EUROPEAN RETAIL DIGITAL TRANSFORMATION STRATEGIES", "European retailing is in a state of change since digital technologies have become so pervasive in the retail journey and consumer expectations, in terms of customer experience, with their preferred brands rapidly evolving. European retailers are adapting to these changes through the adoption of commerce everywhere business models and technology. Retailers from across Europe are in the process of determining how digital impacts them and what their digital transformation approach and strategy should be. This reveals a more and more complex and highly differentiated European region, where differences exist and persist across countries as well as subsegments (fashion and apparel, department stores, food and grocery, etc.). Retail Insights is witnessing among European retailers, at varying degrees of maturity, a race to digitize because of the potential new revenue streams and retail operational efficiencies that can be derived. Retail Insights: European Retail Digital Transformation Strategies advisory service examines the impact of digital transformation on the European retailers' business, technology, and organizational areas. Specific coverage is given to provide valuable insights into the European retail industry, with a specific focus on digital transformation strategies applied by retail companies to improve the omni-channel customer experience. This advisory service develops unique analysis and comprehensive data through Retail Insights' proprietary research projects, along with ongoing communications with industry experts, retail CIOs, and line-of-business executives, and ICT product and service vendors. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports. Our analysts are also available to provide personalized advice for retail executives and ICT vendors to help them make better-informed decisions." },
                    { new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97"), new Guid("c0f3e2d5-33b1-454c-a7aa-f1ba51684e0a"), null, null, null, new Guid("426b4ff5-6787-4b81-abeb-f25d8a3fad83"), true, false, false, null, "Consumer Banking Engagement Strategies", "CONSUMER BANKING ENGAGEMENT STRATEGIES", "Being successful in banking will be determined by how well institutions manage the transformation in both digital and physical channels. Customers have seemingly ubiquitous access to their accounts on their terms and on their devices as banks continue to strategize about the future of the branch network and new channels emerge. Unfortunately, many banks are still looking at their channel strategy in a silo without fully understanding that customer engagement is the key to a profitable relationship. Today’s technology has fostered this customer-led revolution, yet there are many more changes yet to be realized as new technology is introduced. Advances in how we engage the customer are pushing the limits and skill sets of business units, marketers, and IT personnel as customers demand more from their retail bank. The Financial Insights: Consumer Banking Engagement Strategies provides critical analysis of the opportunities and options facing banks as they wrestle with their technology plans and investment decisions in alignment with their strategic goals. This research delivers key insights regarding the business drivers of and value delivered from customer-facing banking technology investments. Topics Addressed Throughout the year, this service will address the following topics: Digital transformation: Strategies and use cases are developing as banks transform all channels, including account opening and onboarding, ATM and ITM, augmented and virtual reality, branch banking, call center, chatbot services, contextualized marketing, conversational banking, digital banking (online and mobile), and social business. Whether these are first-generation offerings or have been around for decades, strategies need to be developed to implement, support, and upgrade these channels to stay with the times. Engagement strategies: Financial institutions are realizing that the number of engagements a customer has is an important factor in profitability. Using big data and analytics to properly measure the number of engagements is a start, but most institutions need to go beyond a prescriptive approach to customer behavior to a more cognitive approach. Omni-experience: The customer life-cycle process offers multiple channels that define the experience and dictate current and future relationships. Customer trends and strategies: This includes topics from level of interaction today to what is likely to be future behavior." },
                    { new Guid("d1b04963-e698-41bf-aded-25bb755c2877"), new Guid("4f06865f-a9d7-428a-956b-9a72268403ec"), null, null, null, new Guid("426b4ff5-6787-4b81-abeb-f25d8a3fad83"), true, false, false, null, "Asia/Pacific Financial Services IT", "ASIA/PACIFIC FINANCIAL SERVICES IT", "Financial Insights: Asia/Pacific Banking Customer Centricity program provides insights into the evolving needs of Asia/Pacific retail/consumer banking retail customers and guidelines on how banks are to respond to these trends. The program will give advice to technology buyers on the technology that supports the customer centricity agenda of the financial institution – particularly in the areas of customer relationship management (CRM), omni-experience and omni-channel solutions, and in loyalty management. The service will be backed by a comprehensive consumer survey on the preferences of Asia/Pacific retail/consumer banking customers in various aspects of customer experience. Financial Insights will then undertake on how financial institutions are to develop an effective customer management agenda, delving into the concept, approach, strategy, use cases, and enabling technologies for customer centricity. Throughout the year, this service will address the following topics: Customer Experience Trends in Asia/Pacific Retail Banking How Asia/Pacific Consumers Pay - Evolving Trends in Retail Payments Customer Centricity in Retail Banking IT and Operations Customer Centricity in Marketing and Product Development Customer Centricity Technology Providers for Asia/Pacific Retail Banking." },
                    { new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015"), new Guid("c0f3e2d5-33b1-454c-a7aa-f1ba51684e0a"), null, null, "", new Guid("426b4ff5-6787-4b81-abeb-f25d8a3fad83"), true, false, false, null, "Universal Payment Strategies", "UNIVERSAL PAYMENT STRATEGIES", "The payment industry has seen drastic changes in the past decade. New technologies, entrants, and business models have forced incumbent vendors and their financial institution customers to rethink how they move money. Stakeholders across the payment value chain — card-issuing banks, merchant acquirers, payment networks, and payment processors — face increasingly complex decisions. In this turbulent market, the players need more than facts and figures; they need critical analysis and insightful opinions. Markets and Subjects Analyzed Throughout the year, this service will address the following topics: Developing trends in payments such as omni-channel and alternative payment networks Evaluation and integration of new payment channels like voice commerce and IoT Enterprise risk, compliance, and fraud issues affecting payment products Legal and regulatory issues around the world that will affect how payments develop Middle- and back-office technologies that will affect the payment strategies of financial institutions Emerging technologies such as blockchain, AI, and next-generation security and their potential for altering the payment landscape Core Research MarketScape: Gateways for Integrated Payments B2B Payments: Digital Transformation in Transactions Retail Revolution: Beyond Card Payments Blockchain Payments: Short-Term Realities Persistent Payments: Automated Transactions in a Connected World Real-Time Payment Productization: Overlays in Action Retail Fraud: Protecting a Multi-Payment Environment" },
                    { new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a"), new Guid("4f06865f-a9d7-428a-956b-9a72268403ec"), null, null, null, new Guid("426b4ff5-6787-4b81-abeb-f25d8a3fad83"), true, false, false, null, "Worldwide Banking IT Spending Guide", "WORLDWIDE BANKING IT SPENDING GUIDE", "The Financial Insights: Worldwide Banking IT Spending Guide examines the banking industry opportunity from a technology, functional process, and geography perspective. This comprehensive database delivered via 's Customer Insights query tool allows the user to easily extract meaningful information about the banking technology market by viewing data trends and relationships and making data comparisons. Markets Covered This product covers the following segments of the banking market: 9 regions: USA, Canada, Japan, Western Europe, Central and Eastern Europe, Middle East and Africa, Latin America, PRC, and Asia/Pacific 4 technologies: Hardware, software, services, and internal IT spend 5 banking segments: Consumer banking, corporate and institutional banking, corporate administration, enterprise utilities, and shared services 30+ functional processes: Channels, payments, core processing, and more 4 company size tiers: Institution size by tier 1 through tier 4 2 institution types: Banks and credit unions 6 years of data Data Deliverables This spending guide is delivered on a semiannual basis via a web-based interface for online querying and downloads. For a complete delivery schedule, please contact an sales representative. The following are the deliverables for this spending guide: Annual five-year forecasts by regions, technologies, banking segments, functional processes, tiers, and institution types; delivered twice a year About This Spending Guide Financial Insights: Worldwide Banking IT Spending Guide provides guidance on the expected technology opportunity around this market at a regional and total worldwide level. Segmented by functional process, institution type, company size tier, region, and technology component, this guide provides IT vendors with insights into both large and rapidly growing segments of the banking technology market and how the market will develop over the coming years." },
                    { new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82"), new Guid("332ac8b8-a6b0-49ef-8007-dba8a8ad25ab"), null, null, null, new Guid("426b4ff5-6787-4b81-abeb-f25d8a3fad83"), true, false, false, null, "Insurance Digital Transformation Approach", "INSURANCE DIGITAL TRANSFORMATION APPROACH", "Customer experience is high on the agenda for most insurers and intermediaries as customers expect true value for the premiums they pay, above and beyond the traditional products and services delivered to them. To continue to be relevant in a changing marketplace, insurance organizations across the globe need to deliver contextual and value-centric insurance that is rooted on the principles of proactive risk management and secure, transparent, seamless, and contextual engagements across the customer journey. To achieve this, the industry needs to accelerate its effort to transform from a traditional, product-centric mindset to a customer-centric mindset enabled by digital technologies and the power of data and analytics. Insurance organizations need to increasingly play the role of true risk advisors rather than mere product sellers. As they progress in their transformation journey, they need advice and guidance to understand the opportunities and possibilities with the new technologies and to build and deploy digital capabilities while also tackling existing barriers to change. The Financial Insights: Worldwide Insurance Digital Transformation Strategies advisory service provides clients with insightful information and analysis of global insurance trends. It also provides coverage of how digital technologies like Big Data, analytics, cloud, mobility, IoT, blockchain, and cognitive technologies impact the life and annuity, accident and health, and property and casualty insurance markets. The service offers timely, strategic, and actionable business IT and application advice to guide our clients as they undertake digital investment decisions, plan for future technology needs, and benchmark themselves against competitors. It provides ongoing research reports, analyst access, and admission to content-rich conferences and webcasts for the benefit of insurance organizations as well as technology vendors." },
                    { new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37"), new Guid("b3f88e39-6e58-4f50-a539-02780f00aeec"), null, null, null, new Guid("426b4ff5-6787-4b81-abeb-f25d8a3fad83"), true, false, false, null, "International Corporate Banking Digital Transformation", "INTERNATIONAL CORPORATE BANKING DIGITAL TRANSFORMATION", "With growing competition from nonbank platforms and the commoditization of products, the ability to onboard, connect, and deliver services to corporate customers is becoming paramount. With PSD2 in Europe democratizing data access and payment services, corporate banks have to transform their value proposition to deliver new data-driven services to their clients and add value, rather than their traditional, commoditized product proposition. Reducing the connectivity cost, increasing speed of data delivery toward real time, and integrating bank systems with corporate enterprise resource planning (ERP), treasury management systems (TSM), payment factories, and in-house banks will be paramount to help corporate clients manage capital, monitor cash flows, and optimize their liquidity. These infrastructure upgrades will also be crucial to exploit the evolving ecosystems brought about by distributed ledger technology (DLT) and the internet of things (IoT), where data discovery, analysis, and sharing will accelerate trade and reduce risk. Sensors attached to goods in transit — from the manufacturing plant to the retail outlet — could offer opportunities to banks' cash management and trade services businesses, better matching flows of payments and goods between seller and buyer. The Financial Insights: Worldwide Corporate Banking Digital Transformation Strategies advisory service provides clients with insightful information and analysis of corporate and commercial banking trends, including cash and treasury, trade finance, commercial lending, and payments. It also provides coverage of how the impact of emerging 3rd Platform technologies like big data, analytics, cloud, mobility, IoT, blockchain, and cognitive technologies transforms the sector and how their convergence unlocks new business and operating models. The service offers timely, strategic, and actionable business IT and application advice to guide our clients as they undertake digital investment decisions, plan for future technology needs, and benchmark themselves against competitors. It provides ongoing research reports, analyst access, and admission to content-rich conferences and webcasts for the benefit of financial institutions as well as technology vendors." },
                    { new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0"), new Guid("c0f3e2d5-33b1-454c-a7aa-f1ba51684e0a"), null, null, null, new Guid("ea56d76a-dedd-419f-946e-71f114733aff"), true, false, false, null, "Health systems and universal health coverage", "HEALTH SYSTEMS AND UNIVERSAL HEALTH COVERAGE", "In the SDG monitoring framework, progress towards universal health coverage (UHC) is tracked with two indicators: (i) a service coverage index (which measures coverage of selected essential health services on a scale of 0 to 100); and (ii) the proportion of the population with large out-of-pocket expenditures on health care (which measures the incidence of catastrophic health spending, rendered as percentage). The service coverage index improved from 45 globally in 2000 to 66 in 2017, with the strongest increase in low-and lower-middle-income countries, where the baseline at 2000 was lowest. However, the pace of that progress has slowed since 2010. The improvements are especially notable for infectious disease interventions and, to a lesser extent, for reproductive, maternal and child health services. Within countries, coverage of the latter services is typically lower in poorer households than in richer households. Overall, between one third and one half the world’s population (33% to 49%) was covered by essential health services in 2017 . Service coverage continued to be lower in low- and middle-income countries than in wealthier ones; the same held for health workforce densities and immunization coverage (Figure 1.2). Available data indicate that over 40% of all countries have fewer than 10 medical doctors per 10 000 people, over 55% have fewer." },
                    { new Guid("180360e0-6db8-412e-a6f4-3d68a939c583"), new Guid("b3f88e39-6e58-4f50-a539-02780f00aeec"), null, null, null, new Guid("ea56d76a-dedd-419f-946e-71f114733aff"), true, false, false, null, "Investing in strengthening country health information systems", "INVESTING IN STRENGTHENING COUNTRY HEALTH INFORMATION SYSTEMS", "Accurate, timely, and comparable health-related statistics are essential for understanding population health trends. Decision-makers need the information to develop appropriate policies, allocate resources and prioritize interventions. For almost a fifth of countries, over half of the indicators have no recent primary or direct underlying data. Data gaps and lags prevent from truly understanding who is being included or left aside and take timely and appropriate action. The existing SDG indicators address a broad range of health aspects but do not capture the breadth of population health outcomes and determinants. Monitoring and evaluating population health thus goes beyond the indicators covered in this report and often requires additional and improved measurements. WHO is committed to supporting Member States to make improvements in surveillance and health information systems. These improvements will enhance the scope and quality of health information and standardize processes to generate comparable estimates at the global level. Getting accurate data on COVID-19 related deaths has been a challenge. The COVID-19 pandemic underscores the serious gaps in timely, reliable, accessible and actionable data and measurements that compromise preparedness, prevention and response to health emergencies. The International Health Regulations (IHR) (2005) monitoring framework is one of the data collection tools that have demonstrated value in evaluating and building country capacities to prevent, detect, assess, report and respond to public health emergencies. From self-assessment of the 13 core capacities in 2019, countries have shown steady progress across almost all capacities including surveillance, laboratory and coordination. As the pandemic progresses, objective and comparable data are crucial to determine the effectiveness of different national strategies used to mitigate and suppress, and thus to better prepare for the probable continuation of the epidemic over the next year or more." },
                    { new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2"), new Guid("c0f3e2d5-33b1-454c-a7aa-f1ba51684e0a"), null, null, null, new Guid("ea56d76a-dedd-419f-946e-71f114733aff"), true, false, false, null, "Maternal mortality has declined but progress is uneven across regions", "MATERNAL MORTALITY HAS DECLINED BUT PROGRESS IS UNEVEN ACROSS REGIONS", "A total of 295 000 [UI 1 80%: 279 000–340 000] women worldwide lost their lives during and following pregnancy and childbirth in 2017, with sub-Saharan Africa and South Asia accounting for approximately 86% of all maternal deaths worldwide. The global maternal mortality ratio (MMR, the number of maternal deaths per 100 000 live births) was estimated at 211 [UI 80%: 199–243], representing a 38% reduction since 2000. On average, global MMR declined by 2.9% every year between 2000 1 UI = uncertainty interval. and 2017. If the pace of progress accelerates enough to achieve the SDG target (reducing global MMR to less than 70 per 100 000 live births), it would save the lives of at least one million women. The majority of maternal deaths are preventable through appropriate management of pregnancy and care at birth, including antenatal care by trained health providers, assistance during childbirth by skilled health personnel, and care and support in the weeks after childbirth. Data from 2014 to 2019 indicate that approximately 81% of all births globally took place in the presence of skilled health personnel, an increase from 64% in the 2000–2006 period. In sub-Saharan Africa, where roughly 66% of the world’s maternal deaths occur, only 60% of births were assisted by skilled health personnel during the 2014–2019 period. " },
                    { new Guid("0070a7dc-c760-4a34-8c87-d4747824191b"), new Guid("b3f88e39-6e58-4f50-a539-02780f00aeec"), null, null, null, new Guid("ea56d76a-dedd-419f-946e-71f114733aff"), true, false, false, null, "European Value-Based Healthcare Digital Transformation", "EUROPEAN VALUE-BASED HEALTHCARE DIGITAL TRANSFORMATION", " With the advent of the value-based paradigm, healthcare organizations are today tasked to ensure equitable access and financial sustainability while improving and reducing the variation of clinical outcomes in healthcare systems. Increasing collaboration across the healthcare value chain, adopting a more personalized approach to treatment, championing the patient experience, and adopting outcome-based business models are fast emerging as necessary for the healthcare industry to succeed. The IDC Health Insights: European Value-Based Healthcare Digital Transformation Strategies service focuses on analyzing European healthcare providers, payers, and public health policy makers' digital strategy and best practices in supporting the adoption of value-based healthcare to deliver the maximum value to patients. Value-based healthcare is a key driver of digital transformation as it requires an unprecedented level of availability of patient information. Key solutions as HIE, population health management, smart patient engagement tools, advanced analytics and cognitive, and cloud and mobility will fundamentally impact care providers and payers' value-based healthcare strategies execution and success. Approach. This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244"), new Guid("c0f3e2d5-33b1-454c-a7aa-f1ba51684e0a"), null, null, null, new Guid("ea56d76a-dedd-419f-946e-71f114733aff"), true, false, false, null, "European Digital Hospital Evolution", "EUROPEAN DIGITAL HOSPITAL EVOLUTION", "European Digital Hospital service provides detailed information about the evolution of digital hospital transformation and technology adoption. It offers valuable insights into IT solution trends within the hospital sector, including healthcare-specific technologies such as electronic health records; admission, discharge, and transfer; digital medical imaging systems; VNA and archiving; and business intelligence and patient management systems. This service focuses on a vital component of healthcare — the digital hospital. Healthcare systems across the world are facing care quality and sustainability challenges that require a new approach to care delivery and reimbursement models. The fully digital hospital is in the center of that journey, being a key enabler of change. The hospital CIOs are facing new technology but, at the same time, a growing legacy IT burden that must be managed alongside of new digital initiatives. In healthcare, management of legacy systems is therefore a growing challenge — when looking at not only obsolete digital applications but also infrastructure and, in the end, how healthcare providers purchase digital applications and platforms. When IT fails to deliver because of legacy and lack of agility, the business model and clinical processes become legacy as well. Key solutions such as EHR, RIS, PACS, VNA, ECM, advanced analytics, cloud, and mobility will fundamentally impact hospitals' digital strategies execution and success.This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806"), new Guid("b3f88e39-6e58-4f50-a539-02780f00aeec"), null, null, null, new Guid("ea56d76a-dedd-419f-946e-71f114733aff"), true, false, false, null, "Health Insights: European Life Science and Pharma", "HEALTH INSIGHTS: EUROPEAN LIFE SCIENCE AND PHARMA", "Digital transformation is key in the European life science industry's ongoing efforts to transform itself as it seeks to find new paths to innovation, optimize operational efficiency and effectiveness, and regain long-term sustainability. The transition toward new care delivery ecosystems and outcome-based reimbursement models enhances life science organizations' needs to implement new approaches to information management and use. The digital mission in the life sciences is to integrate new and existing data, information, and knowledge into a more knowledge-centric approach to new drug and medical devices development, to product commercialization and management, and to treatment delivery. The European life science digital transformation service provides a forward-looking analysis of IT and technologies that are being adopted all along the value chain of the European life science industry. The service focuses on how European life science organizations can leverage the new range of technologies underpinned by the 3rd Platform technologies and innovation accelerators to support efforts to improve the operational efficiency of the industry and better engage with the ultimate end user of life science innovations, namely patients. This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae"), new Guid("b3f88e39-6e58-4f50-a539-02780f00aeec"), null, null, null, new Guid("d0a91bed-717c-4906-a230-b791e8119932"), true, false, false, null, "Canadian Internet of Things Ecosystem and Trends", "CANADIAN INTERNET OF THINGS ECOSYSTEM AND TRENDS", "The Internet of Things (IoT) market is poised for exponential growth over the next several years. However, the IoT ecosystem is a complex segment with multiple layers and hundreds of players, including device and module manufacturers, communication service providers (CSPs), IoT platform players, applications, analytics and security software vendors, and professional services providers. 's Canadian Internet of Things Ecosystem and Trends service analyzes the regionally specific growth of this market from the autonomously connected units in the enterprise world to the applications and services that will demonstrate the power of a world of connected .Markets and Subjects Analyzed Market size and forecast for the Canadian IoT market Canadian IoT revenue forecast by industry Canadian IoT revenue forecast by use cases Taxonomy of the IoT ecosystem: How is the market organized, and what are the key segments and technologies? Canadian-specific demand-side data from various segments (e.g., IT and LOB) Analysis of Canadian-specific vendor readiness and go-to-market strategies for IoT Vertical use cases/case studies of IoT Core Research Taxonomy of the IoT Ecosystem Forecast for the Canadian IoT Market by Industry Forecast for the Canadian IoT Market by Use Cases Profiles of Canadian Vendors Leading IoT Evolution Case Studies of Successful Canadian IoT Implementations Canadian Internet of Things Decision-Maker Survey In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("cffed078-aa51-4fe2-913e-59e6e2624828"), new Guid("c0f3e2d5-33b1-454c-a7aa-f1ba51684e0a"), null, null, null, new Guid("d0a91bed-717c-4906-a230-b791e8119932"), true, false, false, null, "Agile Application Life-Cycle, Quality and Portfolio Strategies", "AGILE APPLICATION LIFE-CYCLE, QUALITY AND PORTFOLIO STRATEGIES", "Agile Application Life-Cycle, Quality and Portfolio Strategies service provides insight into business and IT alignment through end-to-end application life-cycle management (ALM) and DevOps strategies including agile, IT portfolio management, software quality, testing and verification tools, software change and configuration, and continuous releases for digital transformation. This service provides insights on how product, organizational, and process transitions can help address regulatory compliance, security, and licensing issues covering cloud, SaaS, and open source software. It also analyzes trends related to ALM and software deployment with applications that leverage and target artificial intelligence (AI) and machine learning (ML), collaboration, mobile and embedded apps, IoT, virtualization, and complex sourcing and evolve to adaptive work, project and portfolio management (PPM), and agile adoption with organizational and process strategies (e.g., Scaled Agile Framework [SAFe]). Markets and Subjects Analyzed Software life-cycle process, requirements, configuration management and collaboration, and agile development and projects Software quality, including mobile, cloud, and embedded strategies PPM and work management software trends and strategies IT project and portfolio management software Developer, business, and operations trends Tools enabling DevOps and governance solutions to align complex deployment, financial management, infrastructure, and business priorities Trends in software configuration management (SCM), including connecting business to IT via requirements management and web services demands on agile life-cycle management solutions Automated software quality (ASQ) tools evolution: Hosted testing, web services, API and cloud testing, service virtualization, vulnerability, risk and value-based testing, and other emerging technologies (e.g., mobile, analysis, and metrics) Trends in collaborative software development with social media" },
                    { new Guid("4a817d53-485b-4634-9c09-22eb56d367fc"), new Guid("332ac8b8-a6b0-49ef-8007-dba8a8ad25ab"), null, null, null, new Guid("d0a91bed-717c-4906-a230-b791e8119932"), true, false, false, null, "Software Channels and Ecosystems", "SOFTWARE CHANNELS AND ECOSYSTEMS", "Software Channels and Ecosystems research service offers intelligence and expertise to help channel executives and program managers develop, implement, support, and manage effective channel strategies and programs to drive successful relationships with the spectrum of partner activities (e.g., resale, managed/cloud services, consulting professional services, and software development). In addition, it provides a comprehensive view of the value of the indirect software market and its leading vendor proponents. This service also identifies and analyzes key industry trends and their impact on channel relationship drivers and channel partner business models. Subscribers are invited to 's semiannual Software Channel Leadership Council where and channel executives present and discuss key industry issues. Markets and Subjects Analyzed Multinational software vendors Digital transformation in the partner ecosystem Software and SaaS channel revenue flow Digital marketplaces Cloud and channels Partner-to-partner networking Key channel trends Software partner program profiles and innovative practices Core Research Channel Revenue Forecast and Analysis Partner Marketing and Communications Ecosystem Digital Transformation Cloud and Channels Emerging Partner Business Models Partner Collaboration." },
                    { new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193"), new Guid("332ac8b8-a6b0-49ef-8007-dba8a8ad25ab"), null, null, null, new Guid("d0a91bed-717c-4906-a230-b791e8119932"), true, false, false, null, "Multicloud Data Management and Protection", "MULTICLOUD DATA MANAGEMENT AND PROTECTION", "Multicloud Data Management and Protection service enables storage vendors and IT users to have a more comprehensive view of the evolving data protection, availability, and recovery market. Integrating cloud technology with traditional hardware and software components, the report series will include market forecasts and provide a combination of timely tactical market information and long-term strategic analysis. Markets and Subjects Analyzed Cloud-based solutions, including backup as a service (BaaS) and disaster recovery as a service (DRaaS) Public, private, and hybrid cloud data protection technologies Disk-based data protection and recovery solutions packaged as software, appliance, or gateway system with a focus on technology evolution Market forecast based on total terabytes shipped and revenue for disk-based data protection and recovery (No vendor shares will be published.) End-user adoption of different data protection and recovery technologies and solutions Cloud service providers delivering cloud-based data protection Data protection for nontraditional data types (e.g., Mongo DB, Cassandra, Hadoop) Vendors delivering disk-based data protection and recovery solutions with focus on architecture based on use cases and applications Technologies and processes, including backup, snapshots, replication, continuous data protection, and data deduplication Impact of purpose-built backup appliances (PBBAs) and hyperconverged systems on overall market growth and customer adoption Implications of copy data management and impact on data availability, capacity management, cost, governance, and security The evolutionary impact of virtual infrastructure and object storage on data protection schemes Implications of flash technology for data protection Data protection best practice guidelines and adoption Core Research Market Analysis, Sizing, and Vendor Shares of Backup as a Service and Disaster Recovery as a Service Use of Disk-Based Technologies for Protection and Recovery Adoption Patterns and Role of PBBAs in Customer Environments End-User Needs and Requirements Data Protection and Recovery Software Market Size, Shares, and Forecasts Disk-Based Data Protection Market Size and Forecast Virtual Backup Appliance Solutions Intersection of Data Protection Software and Management with Cloud Services In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("aba89aa6-e637-4518-83c1-125b625add12"), new Guid("4ed92a05-a299-4b37-8040-7f9a29b9795b"), null, null, null, new Guid("d0a91bed-717c-4906-a230-b791e8119932"), true, false, false, null, "DevOps Analytics, Automation and Security", "DEVOPS ANALYTICS, AUTOMATION AND SECURITY", "DevOps is a powerful modern approach to unifying the business strategy, development, testing, deployment, and life-cycle operation of software. This approach is accomplished by improving business, IT, and development collaboration while taking full advantage of automation technologies, end-to-end processes, microservices architecture, and cloud infrastructure to accelerate development and delivery and enable innovation. This subscription service provides insight, forecasts, and thought leadership to assist IT management, IT professionals, IT vendors, and service providers in creating compelling DevOps strategies and solutions. Markets and Subjects Analyzed DevOps adoption drivers, benefits, and use cases Identification of DevOps innovators and best practices Identification of critical DevOps tools enabling automation, open source technologies, and market leaders Analysis of the impact DevOps is having on IT infrastructure hardware and software purchasing and deployment priorities across on-premises and cloud service platforms Major transformation and impact DevOps is having on staffing, skills, and internal processes Core Research Worldwide DevOps Software Forecast, 2018-2022 Worldwide DevOps Software Market Share, 2018-2022 The Role of Cloud-Based IDEs to Drive DevOps Adoption Changing Drivers of DevOps: Role of Serverless and Microservices/Cloud-Native Architectures Market Analysis Perspective: Worldwide Developer, Operations, and DevOps Evolving Approaches and Practices in DevOps The Shift to Continuous Delivery and Deployment Tools and Frameworks for Supporting DevOps Workflows In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd"), new Guid("4f06865f-a9d7-428a-956b-9a72268403ec"), null, null, null, new Guid("d0a91bed-717c-4906-a230-b791e8119932"), true, false, false, null, "Cybersecurity Analytics, Intelligence, Response and Orchestration", "CYBERSECURITY ANALYTICS, INTELLIGENCE, RESPONSE AND ORCHESTRATION", "Cybersecurity Analytics, Intelligence, Response and Orchestration service covers security software and hardware products related to analytic security platforms, security and vulnerability management (SVM), and security orchestration platforms. Specific functions covered include vulnerability management and intelligence, SIEM, security analytics, threat hunting, incident detection and response, and orchestration. The service is designed to create in-depth coverage of the analytic-based and platform security markets. Markets and Subjects Analyzed Vulnerability management SIEM Security analytics Incident detection and response Threat analytics Automation Orchestration Core Research Security AIRO Forecast Security AIRO Market Share SIEM Market Share and Forecast Market MAP In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment. Key Questions Answered What is the size and market opportunity for security analytics solutions? Who are the major players in the security analytics space? What is the size and market opportunity for security orchestration solutions? What is the size and market opportunity for threat analytics solutions? How has the competitive landscape changed through digital transformation and adoption of cloud and enabling technologies?" },
                    { new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0"), new Guid("332ac8b8-a6b0-49ef-8007-dba8a8ad25ab"), null, null, null, new Guid("0b3a1500-1c80-45da-9ea0-b054df73b99f"), true, false, false, null, "North America Utilities Digital Transformation Tactics", "ENERGY INSIGHTS: NORTH AMERICA UTILITIES DIGITAL TRANSFORMATION TACTICS", "Digital transformation is driving utilities to change their business and operating and information models. The Energy Insights: North America Utilities Digital Transformation Strategies service is designed to help North American utility IT and business management understand the disruptions that are transforming the energy and utility value chains and develop the strategies and programs to capitalize on the evolving opportunities. The service provides deep research and insight on the key topics that matter to North American utility decision makers and IT executives affected by the digital transformation. Through an integrated set of deliverables, subscribers gain access to analysis and insights on the impact of new technologies, IT strategies and best practices, and the industry, governmental, and regulatory forces that are shaping the evolution to new industry business models in the industry's generation, transmission and distribution/pipelines, and retail sectors. This service delivers research, insight, and guidance on IT strategy and investments to meet the utility's most critical corporate objectives." },
                    { new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069"), new Guid("b3f88e39-6e58-4f50-a539-02780f00aeec"), null, null, null, new Guid("0b3a1500-1c80-45da-9ea0-b054df73b99f"), true, false, false, null, "Worldwide Oil and Gas Downstream IT Blueprint", "WORLDWIDE OIL AND GAS DOWNSTREAM IT BLUEPRINT", "Energy Insights: Worldwide Oil and Gas Downstream IT Strategies advisory research service takes the worldwide oil and gas IT strategies research and applies it to the downstream operations in oil and gas. The research focuses on companies that are involved in the production for sale of all petroleum products, from incoming crude to outgoing product. It will also encompass the technology companies that provide the technology foundation for the operation of the downstream business. Energy Insights: Worldwide Oil and Gas Downstream IT Strategies will focus its analysis on the digital transformation (DX) use cases, priorities, innovation technology, and road map for all aspect of companies that produce and distribute petroleum products. It will provide technology vendors a road map for product development and messaging in oil and gas downstream. Throughout the year, this service will address the following topics: Digital transformation – Investment priorities and road maps for transformation of the downstream business operation Operations technology — IT/OT convergence investments that help downstream organizations transform how they operate the production lines Asset management – Technology changes driving asset management organization and transformation Transformation to scale – Development of a transformed infrastructure to scale to market volatility Our research addresses the following issues that are critical to your success: What is the impact of transformational technologies on the oil and gas downstream DX road map? How does an oil and gas downstream company organize itself around the Future of Work? What are the market trends in oil and gas downstream that affect the use case prioritization for DX? How should trading partners be integrated into the DX road map? Who are the technology vendors that can support DX in oil and gas downstream?" },
                    { new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a"), new Guid("332ac8b8-a6b0-49ef-8007-dba8a8ad25ab"), null, null, null, new Guid("0b3a1500-1c80-45da-9ea0-b054df73b99f"), true, false, false, null, "Global Utilities Customer Experience Scheme", "GLOBAL UTILITIES CUSTOMER EXPERIENCE SCHEME", "With the evolution of energy and water markets and the possibilities offered by digital transformation, utilities are developing new business models and are focusing on providing customers more interactive experiences. Smart metering, analytics, social media, mobility, cloud, and IoT are fundamentally impacting customer operation practices in both competitive and regulated markets. The Energy Insights: Worldwide Utilities Customer Experience Strategies service is designed to help utilities and energy retailers servicing customers in competitive and regulated markets at the worldwide level (including electricity, gas, and water). The service provides exclusive research and direct access to experts providing guidance to make the right IT investments and meet corporate objectives of customer satisfaction, reduction of cost to serve, and innovation. This service develops unique analysis and comprehensive data through Energy Insights' proprietary research projects, along with ongoing communications with industry experts, utility management, ICT vendors, and service providers. With decades of experience in the utility industry, our analysts leverage a broad spectrum of expertise and intellectual property from Energy Insights. Research reports elucidate business strategy, best practices, technology selection, and vendor assessment, along with short perspectives on topical issues. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("76352896-e547-485f-ae20-73b4f05b664f"), new Guid("4ed92a05-a299-4b37-8040-7f9a29b9795b"), null, null, null, new Guid("0b3a1500-1c80-45da-9ea0-b054df73b99f"), true, false, false, null, "Asia/Pacific Oil and Gas Digital Transformation Procedure", "ASIA/PACIFIC OIL AND GAS DIGITAL TRANSFORMATION PROCEDURE", "Asia/Pacific Oil and Gas Digital Transformation Strategies examines the business environment of the oil and gas industry in the Asia/Pacific region, excluding Japan. The service covers the whole value chain from upstream, midstream, and downstream but is focused on upstream. It seeks to support companies working to deliver against efficiency objectives through technology-led change that will deliver improved decision support and insight and flexibility to adjust to changing technology, markets, and political pressures. The Energy Insights: Asia/Pacific Oil and Gas Digital Transformation Strategies program provides advice on best practices and technology priorities that will support technology decision making, particularly, in relation to innovation, data management, and greater integration across silos and processes. The service is designed to support oil and gas companies with market insights and road mapping advice relating to technology decision making, particularly delivering insight to how increased value from investments can be delivered through better understanding of new metric, organization, talent, and process change. Key technology focus areas of the program include IT/OT integration, operational cybersecurity, IoT strategies, and asset management priorities. This service is built on the foundations of extensive engagement and research by the Energy Insights team across the oil and gas industry in this region. Our analysts work with industry experts, staff from the oil and gas business, and technology vendors to ensure that the research service is relevant and prioritizes areas that are of importance to them and the industry at large." },
                    { new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d"), new Guid("332ac8b8-a6b0-49ef-8007-dba8a8ad25ab"), null, null, null, new Guid("0b3a1500-1c80-45da-9ea0-b054df73b99f"), true, false, false, null, "Worldwide Mining Policy", "WORLDWIDE MINING POLICY", "Worldwide Mining Strategies examines the business environment across the mining sector value chain from exploration through to operations, processing, supply chain, and trading globally. Mining companies are operating in an increasingly competitive commodity market environment, driven by pressures relating to accessing funding, assets, and talent. Technology and data are playing an increasingly critical role across the operation to enable decision support, automation, integration, and control. This service provides comprehensive insights into the best practices that show how mining companies are responding and what road maps these companies will need to build the information technology (IT) capabilities required to create integrated, agile, and responsive operations. Mining companies are changing the way they buy technology, how they collaborate to frame the problems to be solved, how they engage with technology suppliers, and how they innovate across their businesses bringing together the capabilities of IT and operational technology (OT). This service tracks the IT investment priorities for organizations seeking to scale value creation across their organization and the impact on decision making and best practices relating to technology, process, and organizational change. This service distills market and industry data into incisive analysis drawn from in-depth interviews with industry experts, mining staff from across the business, and technology vendors. Insight and analysis are further supported and validated through rigorous research methodologies in quantitative market analysis.  Energy Insights' analysts develop unique and comprehensive analyses of this data, focused on providing actionable recommendations. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("11dcaf06-8a35-403a-8262-ebd98344039a"), new Guid("4f06865f-a9d7-428a-956b-9a72268403ec"), null, null, null, new Guid("edf9d9bb-e1bb-42fa-b8e5-e8e1f88ecfb0"), true, false, false, null, "Digital Commerce Trends", "DIGITAL COMMERCE TRENDS", "The Digital Commerce subscription marketing intelligence service examines the competitive landscape, key trends, and differentiating factors of digital commerce, PIM, and CPQ application vendors; digital marketplaces; and business commerce networks. This includes the buying behavior of end users while purchasing products and services on digital commerce platforms. The service provides a worldwide perspective by looking at all forms of digital commerce transactions — including B2B, B2C, C2C, G2B, and B2B2C — across all vertical industries and regional markets. Markets and Subjects Analyzed. Digital commerce applications targeting businesses of all sizes and industries. Best-of-breed digital commerce applications in areas such as CPQ, payments and billing, order management, web content management, merchandising, site search, fulfillment, product information management, and inventory management. Business commerce networks that bring together functional applications for enterprise asset management, procurement, financials, sales, and human capital management.Enterprise partnership/integration strategies among digital commerce and marketing/content management vendors. Experience management — across channels (includes content management, commerce platforms, integration, and advanced analytics) for B2B, B2C, and B2B2C. Impact of cloud, social, mobile, and Big Data technologies on vendor strategies for employee, customer, supplier, partner, and asset engagement. Proliferation of public cloud microservice architectures. Mobile commerce (mobile app marketplaces, built-for-mobile commerce capabilities, and differentiating technology). Cognitive technologies and intelligent workflow impact on digital commerce applications" },
                    { new Guid("67966830-655d-42f7-9424-ef5cf441ff8d"), new Guid("4ed92a05-a299-4b37-8040-7f9a29b9795b"), null, null, null, new Guid("edf9d9bb-e1bb-42fa-b8e5-e8e1f88ecfb0"), true, false, false, null, "Canadian Sales Accelerator: Datacenter Infrastructure", "CANADIAN SALES ACCELERATOR: DATACENTER INFRASTRUCTURE", "Canadian Sales Accelerator: Datacenter Infrastructure research program analyzes the Canadian datacenter (DC) infrastructure market in Canada. Designed to provide intelligence and strategic frameworks to technology sales professionals, field marketing teams, and channel managers to take action on key responsibilities related to the sales cycle, this service provides market sizing, vendor performance, forecasts, and market opportunities, as well as a variety of adjacent markets and segmentations from a quantitative standpoint. This advisory service will allow subscribers to identify opportunities tied to the future of infrastructure, tailor go-to-market strategies as well as product and services road maps to meet the growing demand of end users for infrastructure solutions. At the same time, it looks at vendors, partners, and customers from a qualitative standpoint, concerning needs and requirements, pain points and buying intentions, maturity levels, and adoption in and of new technologies. Markets and Subjects Analyzed Datacenter technologies (including software defined); server and storage; converged systems. Infrastructure ecosystem including partners and channels. Datacenter budget trends and dynamics and vendor selection/buying criteria. Technology investment expectations for legacy and next-gen DC infrastructure. In addition to the core research documents, clients will receive briefings and concise sales executive email alerts throughout the year. Every client will have a Sales Accelerator service launch integration meeting to kick-off the program. Core Research. Brand Perceptions on Enterprise Storage and Service Vendors. Market Forecasts. Vendor Dashboard: Market Shares. Infrastructure Ecosystem Barometer. Executive Market Insights" }
                });

            migrationBuilder.InsertData(
                table: "ReportTags",
                columns: new[] { "TagId", "ReportId" },
                values: new object[,]
                {
                    { new Guid("66358a59-4f29-4967-9aef-bc0e559e82e8"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") },
                    { new Guid("7cc33bae-61dd-4359-8b7c-7a11afd05c88"), new Guid("180360e0-6db8-412e-a6f4-3d68a939c583") },
                    { new Guid("92a409a6-5571-4b19-8cc5-3091ebb1d477"), new Guid("180360e0-6db8-412e-a6f4-3d68a939c583") },
                    { new Guid("7cc33bae-61dd-4359-8b7c-7a11afd05c88"), new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0") },
                    { new Guid("07d2b39c-f0cf-4112-a55a-8e5f5fd427b8"), new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0") },
                    { new Guid("5e441759-d367-446f-8456-34d118b05561"), new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37") },
                    { new Guid("3e4b5376-5e4a-405f-a5e9-450d3cd63619"), new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37") },
                    { new Guid("66358a59-4f29-4967-9aef-bc0e559e82e8"), new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82") },
                    { new Guid("36833c8e-dd9b-4132-9459-7b61081c1600"), new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82") },
                    { new Guid("5e441759-d367-446f-8456-34d118b05561"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") },
                    { new Guid("36833c8e-dd9b-4132-9459-7b61081c1600"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") },
                    { new Guid("b2af9ae6-be5b-45d7-9df3-45cb1f8cbf6d"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") },
                    { new Guid("3e4b5376-5e4a-405f-a5e9-450d3cd63619"), new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015") },
                    { new Guid("b2af9ae6-be5b-45d7-9df3-45cb1f8cbf6d"), new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015") },
                    { new Guid("92a409a6-5571-4b19-8cc5-3091ebb1d477"), new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2") },
                    { new Guid("36833c8e-dd9b-4132-9459-7b61081c1600"), new Guid("d1b04963-e698-41bf-aded-25bb755c2877") },
                    { new Guid("5e441759-d367-446f-8456-34d118b05561"), new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97") },
                    { new Guid("36833c8e-dd9b-4132-9459-7b61081c1600"), new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97") },
                    { new Guid("b2af9ae6-be5b-45d7-9df3-45cb1f8cbf6d"), new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef") },
                    { new Guid("962db5bb-b7dd-4b12-b992-0e91e9cc4a11"), new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef") },
                    { new Guid("e0ec89ca-3408-4c23-b418-993f7f01d6f6"), new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02") },
                    { new Guid("00150dd5-5388-456f-85d8-52af27a1ea0b"), new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02") },
                    { new Guid("e0ec89ca-3408-4c23-b418-993f7f01d6f6"), new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7") },
                    { new Guid("962db5bb-b7dd-4b12-b992-0e91e9cc4a11"), new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7") },
                    { new Guid("b2af9ae6-be5b-45d7-9df3-45cb1f8cbf6d"), new Guid("9062907e-6a91-4737-95f9-8db332df47f4") },
                    { new Guid("e0ec89ca-3408-4c23-b418-993f7f01d6f6"), new Guid("9062907e-6a91-4737-95f9-8db332df47f4") },
                    { new Guid("66358a59-4f29-4967-9aef-bc0e559e82e8"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") },
                    { new Guid("e0ec89ca-3408-4c23-b418-993f7f01d6f6"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") },
                    { new Guid("00150dd5-5388-456f-85d8-52af27a1ea0b"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") },
                    { new Guid("b2af9ae6-be5b-45d7-9df3-45cb1f8cbf6d"), new Guid("d1b04963-e698-41bf-aded-25bb755c2877") },
                    { new Guid("828fa2c8-26be-4cc7-9a67-f420e86b3330"), new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2") },
                    { new Guid("66358a59-4f29-4967-9aef-bc0e559e82e8"), new Guid("0070a7dc-c760-4a34-8c87-d4747824191b") },
                    { new Guid("7cc33bae-61dd-4359-8b7c-7a11afd05c88"), new Guid("0070a7dc-c760-4a34-8c87-d4747824191b") },
                    { new Guid("010fe3d8-635b-4335-b92b-8d748733ac3a"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") },
                    { new Guid("1396511b-76e7-4f36-983e-1ebf5a1698dc"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") },
                    { new Guid("737fb1e5-534a-4a4c-8f2a-2137c2e13c56"), new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d") },
                    { new Guid("a6213e7c-8058-4fc8-999b-e071bc460520"), new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d") },
                    { new Guid("66358a59-4f29-4967-9aef-bc0e559e82e8"), new Guid("76352896-e547-485f-ae20-73b4f05b664f") },
                    { new Guid("737fb1e5-534a-4a4c-8f2a-2137c2e13c56"), new Guid("76352896-e547-485f-ae20-73b4f05b664f") },
                    { new Guid("1396511b-76e7-4f36-983e-1ebf5a1698dc"), new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a") },
                    { new Guid("a6213e7c-8058-4fc8-999b-e071bc460520"), new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a") },
                    { new Guid("010fe3d8-635b-4335-b92b-8d748733ac3a"), new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069") },
                    { new Guid("737fb1e5-534a-4a4c-8f2a-2137c2e13c56"), new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069") },
                    { new Guid("010fe3d8-635b-4335-b92b-8d748733ac3a"), new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0") },
                    { new Guid("1396511b-76e7-4f36-983e-1ebf5a1698dc"), new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0") },
                    { new Guid("776d42cb-e955-43dc-9a36-cc355d4309d3"), new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd") },
                    { new Guid("0af3dad4-8d94-47b4-85d4-62f8e18ef8ca"), new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd") },
                    { new Guid("6879d04c-61a0-4195-96ab-0ec54585c1cd"), new Guid("aba89aa6-e637-4518-83c1-125b625add12") },
                    { new Guid("0af3dad4-8d94-47b4-85d4-62f8e18ef8ca"), new Guid("aba89aa6-e637-4518-83c1-125b625add12") },
                    { new Guid("b77ea49d-d372-4a0b-8b1c-3aab566a4216"), new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193") },
                    { new Guid("0af3dad4-8d94-47b4-85d4-62f8e18ef8ca"), new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193") },
                    { new Guid("776d42cb-e955-43dc-9a36-cc355d4309d3"), new Guid("4a817d53-485b-4634-9c09-22eb56d367fc") },
                    { new Guid("0af3dad4-8d94-47b4-85d4-62f8e18ef8ca"), new Guid("4a817d53-485b-4634-9c09-22eb56d367fc") },
                    { new Guid("6879d04c-61a0-4195-96ab-0ec54585c1cd"), new Guid("cffed078-aa51-4fe2-913e-59e6e2624828") },
                    { new Guid("b77ea49d-d372-4a0b-8b1c-3aab566a4216"), new Guid("cffed078-aa51-4fe2-913e-59e6e2624828") },
                    { new Guid("6879d04c-61a0-4195-96ab-0ec54585c1cd"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") },
                    { new Guid("776d42cb-e955-43dc-9a36-cc355d4309d3"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") },
                    { new Guid("0af3dad4-8d94-47b4-85d4-62f8e18ef8ca"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") },
                    { new Guid("66358a59-4f29-4967-9aef-bc0e559e82e8"), new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806") },
                    { new Guid("07d2b39c-f0cf-4112-a55a-8e5f5fd427b8"), new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806") },
                    { new Guid("66358a59-4f29-4967-9aef-bc0e559e82e8"), new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244") },
                    { new Guid("828fa2c8-26be-4cc7-9a67-f420e86b3330"), new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244") },
                    { new Guid("00150dd5-5388-456f-85d8-52af27a1ea0b"), new Guid("67966830-655d-42f7-9424-ef5cf441ff8d") },
                    { new Guid("962db5bb-b7dd-4b12-b992-0e91e9cc4a11"), new Guid("67966830-655d-42f7-9424-ef5cf441ff8d") }
                });
        }
    }
}
