﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace InsightHub.Data.Migrations
{
    public partial class Fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("14f125e6-0aba-4f86-9c58-f725026e945e"), new Guid("5133194c-2c5f-415f-afae-04ce148762a4") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("14f125e6-0aba-4f86-9c58-f725026e945e"), new Guid("7ceb649a-9fc2-409f-afe8-8d747c3a80ba") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("14f125e6-0aba-4f86-9c58-f725026e945e"), new Guid("91f95a71-4116-4064-a831-c492d033207a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("14f125e6-0aba-4f86-9c58-f725026e945e"), new Guid("99ac8caf-4841-4efc-90a6-5d3f58587f43") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("18113347-addf-4b30-a8c3-5dda7ef744a9"), new Guid("6f308b37-d97a-478f-88c0-3256c8b123c5") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("18113347-addf-4b30-a8c3-5dda7ef744a9"), new Guid("9c6ad87d-b16a-4773-8e9f-af71088d848d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("256d5428-6baa-408a-98b7-80abffb6d05f"), new Guid("3262945f-2b98-4e9c-aae5-7f2b5ec193de") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("256d5428-6baa-408a-98b7-80abffb6d05f"), new Guid("3b480a1c-ae0e-4efe-8083-19d1635ca2c1") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("256d5428-6baa-408a-98b7-80abffb6d05f"), new Guid("deb360a4-728a-44a3-9450-13999832128c") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("26c4bcfc-935a-4a3e-bc39-e995aa117a4a"), new Guid("39e9d969-1364-4c71-a72b-af5b3f1d6da8") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("26c4bcfc-935a-4a3e-bc39-e995aa117a4a"), new Guid("56223693-ff5a-4b25-9233-e65856cab234") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("26c4bcfc-935a-4a3e-bc39-e995aa117a4a"), new Guid("e33025cd-dbc8-4ae2-9d64-a16d04a65896") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("2bf7759f-8e07-4415-b620-182f259237c6"), new Guid("1c0c7738-81ea-46b6-9c4e-240fa6f1cf5a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("2bf7759f-8e07-4415-b620-182f259237c6"), new Guid("41d7ceee-fc64-4227-b531-2f6946ad3431") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("2bf7759f-8e07-4415-b620-182f259237c6"), new Guid("5b4c3342-64a2-467c-83da-1d2874185c4c") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("2ef11211-e751-4d9f-885e-8071bdd22e48"), new Guid("025e9288-b2de-47e3-ab30-5ccff81025d0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("2ef11211-e751-4d9f-885e-8071bdd22e48"), new Guid("24ba7d9f-ef1a-4bdb-b966-44b09c01c244") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("2ef11211-e751-4d9f-885e-8071bdd22e48"), new Guid("70123bf4-a702-402c-9ff3-cb2ea8eba45d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("2ef11211-e751-4d9f-885e-8071bdd22e48"), new Guid("cc7e2d5e-8fbe-44b7-b46d-f7c953f36453") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("4454473e-9573-4454-900e-6415ffbc6fb8"), new Guid("7ceb649a-9fc2-409f-afe8-8d747c3a80ba") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("4454473e-9573-4454-900e-6415ffbc6fb8"), new Guid("8440a849-2935-4c53-b6e1-77db5bf134ae") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("4454473e-9573-4454-900e-6415ffbc6fb8"), new Guid("91f95a71-4116-4064-a831-c492d033207a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("4f9e14aa-3fb5-4235-9066-d9ab835a891a"), new Guid("56223693-ff5a-4b25-9233-e65856cab234") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("4f9e14aa-3fb5-4235-9066-d9ab835a891a"), new Guid("f8ac51be-bafe-4f7a-bcaa-28c18691d057") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("52ae14e3-33bc-4d5c-a9bc-09c0afb88e00"), new Guid("315002c7-fff8-4aa1-8f4c-e75617ec2c1d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("52ae14e3-33bc-4d5c-a9bc-09c0afb88e00"), new Guid("39e9d969-1364-4c71-a72b-af5b3f1d6da8") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("60f2f2cf-8052-47ab-ad8a-948a4b60a1d8"), new Guid("093713ed-0a47-456f-a7cf-7b56a64c2042") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("60f2f2cf-8052-47ab-ad8a-948a4b60a1d8"), new Guid("24ba7d9f-ef1a-4bdb-b966-44b09c01c244") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("60f2f2cf-8052-47ab-ad8a-948a4b60a1d8"), new Guid("70123bf4-a702-402c-9ff3-cb2ea8eba45d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("60f2f2cf-8052-47ab-ad8a-948a4b60a1d8"), new Guid("99ac8caf-4841-4efc-90a6-5d3f58587f43") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("60f2f2cf-8052-47ab-ad8a-948a4b60a1d8"), new Guid("9c6ad87d-b16a-4773-8e9f-af71088d848d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("87e2c0e3-e7b3-4659-9725-bd48052fe187"), new Guid("074d940b-da31-42d9-9aea-de5a6c402887") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("87e2c0e3-e7b3-4659-9725-bd48052fe187"), new Guid("fb9ae4bc-e6d6-4c8d-a0be-eba5496c77cd") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("946c72f1-2bfc-423b-a43b-92ca35669b85"), new Guid("27f99c7d-31d9-4350-bb41-aa804e44150a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("946c72f1-2bfc-423b-a43b-92ca35669b85"), new Guid("5b4c3342-64a2-467c-83da-1d2874185c4c") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("946c72f1-2bfc-423b-a43b-92ca35669b85"), new Guid("bcc8b91e-78c7-449f-98fe-702c3ce0680e") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b0987c84-5552-4b87-ae09-0400a1e86b4d"), new Guid("a9ad77fe-9096-44ae-b239-da190229ff92") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b0987c84-5552-4b87-ae09-0400a1e86b4d"), new Guid("bcc8b91e-78c7-449f-98fe-702c3ce0680e") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b6d5154e-4db9-4b1d-b33c-16f766fafc2f"), new Guid("315002c7-fff8-4aa1-8f4c-e75617ec2c1d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b6d5154e-4db9-4b1d-b33c-16f766fafc2f"), new Guid("aa5997b6-e887-43be-bcd5-3285a2a5edaa") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b70c6f3b-4b1a-47e1-98de-24c133a33f62"), new Guid("074d940b-da31-42d9-9aea-de5a6c402887") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b70c6f3b-4b1a-47e1-98de-24c133a33f62"), new Guid("3262945f-2b98-4e9c-aae5-7f2b5ec193de") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b70c6f3b-4b1a-47e1-98de-24c133a33f62"), new Guid("9ae87fe8-9be5-488d-9e80-d1f9c546a4e3") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("d38ab07a-c1c8-4ead-a8fc-6aa64767866c"), new Guid("093713ed-0a47-456f-a7cf-7b56a64c2042") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("d38ab07a-c1c8-4ead-a8fc-6aa64767866c"), new Guid("5133194c-2c5f-415f-afae-04ce148762a4") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("d38ab07a-c1c8-4ead-a8fc-6aa64767866c"), new Guid("8440a849-2935-4c53-b6e1-77db5bf134ae") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("ddfb2078-91ef-469e-a019-ae8a5a6b95a5"), new Guid("025e9288-b2de-47e3-ab30-5ccff81025d0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("ddfb2078-91ef-469e-a019-ae8a5a6b95a5"), new Guid("24ba7d9f-ef1a-4bdb-b966-44b09c01c244") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("ddfb2078-91ef-469e-a019-ae8a5a6b95a5"), new Guid("6f308b37-d97a-478f-88c0-3256c8b123c5") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f1b152e3-7ba8-4c85-8d0b-03f7266a00b4"), new Guid("1c0c7738-81ea-46b6-9c4e-240fa6f1cf5a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f1b152e3-7ba8-4c85-8d0b-03f7266a00b4"), new Guid("41d7ceee-fc64-4227-b531-2f6946ad3431") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f1b152e3-7ba8-4c85-8d0b-03f7266a00b4"), new Guid("a9ad77fe-9096-44ae-b239-da190229ff92") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f7f4c29f-e67f-4a5a-ade8-5e65f72b005a"), new Guid("1c0c7738-81ea-46b6-9c4e-240fa6f1cf5a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f7f4c29f-e67f-4a5a-ade8-5e65f72b005a"), new Guid("27f99c7d-31d9-4350-bb41-aa804e44150a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f7f4c29f-e67f-4a5a-ade8-5e65f72b005a"), new Guid("7ceb649a-9fc2-409f-afe8-8d747c3a80ba") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f7f4c29f-e67f-4a5a-ade8-5e65f72b005a"), new Guid("aa5997b6-e887-43be-bcd5-3285a2a5edaa") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f7f4c29f-e67f-4a5a-ade8-5e65f72b005a"), new Guid("cc7e2d5e-8fbe-44b7-b46d-f7c953f36453") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f7f4c29f-e67f-4a5a-ade8-5e65f72b005a"), new Guid("e33025cd-dbc8-4ae2-9d64-a16d04a65896") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f7f4c29f-e67f-4a5a-ade8-5e65f72b005a"), new Guid("f8ac51be-bafe-4f7a-bcaa-28c18691d057") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("fcb8d5a3-818c-4f3f-9241-3907d826794e"), new Guid("3262945f-2b98-4e9c-aae5-7f2b5ec193de") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("fcb8d5a3-818c-4f3f-9241-3907d826794e"), new Guid("3b480a1c-ae0e-4efe-8083-19d1635ca2c1") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("fcb8d5a3-818c-4f3f-9241-3907d826794e"), new Guid("9ae87fe8-9be5-488d-9e80-d1f9c546a4e3") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("fcb8d5a3-818c-4f3f-9241-3907d826794e"), new Guid("deb360a4-728a-44a3-9450-13999832128c") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("fcb8d5a3-818c-4f3f-9241-3907d826794e"), new Guid("fb9ae4bc-e6d6-4c8d-a0be-eba5496c77cd") });

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("025e9288-b2de-47e3-ab30-5ccff81025d0"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("074d940b-da31-42d9-9aea-de5a6c402887"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("093713ed-0a47-456f-a7cf-7b56a64c2042"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("1c0c7738-81ea-46b6-9c4e-240fa6f1cf5a"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("24ba7d9f-ef1a-4bdb-b966-44b09c01c244"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("27f99c7d-31d9-4350-bb41-aa804e44150a"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("315002c7-fff8-4aa1-8f4c-e75617ec2c1d"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3262945f-2b98-4e9c-aae5-7f2b5ec193de"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("39e9d969-1364-4c71-a72b-af5b3f1d6da8"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3b480a1c-ae0e-4efe-8083-19d1635ca2c1"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("41d7ceee-fc64-4227-b531-2f6946ad3431"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("5133194c-2c5f-415f-afae-04ce148762a4"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("56223693-ff5a-4b25-9233-e65856cab234"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("5b4c3342-64a2-467c-83da-1d2874185c4c"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("6f308b37-d97a-478f-88c0-3256c8b123c5"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("70123bf4-a702-402c-9ff3-cb2ea8eba45d"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("7ceb649a-9fc2-409f-afe8-8d747c3a80ba"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("8440a849-2935-4c53-b6e1-77db5bf134ae"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("91f95a71-4116-4064-a831-c492d033207a"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("99ac8caf-4841-4efc-90a6-5d3f58587f43"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("9ae87fe8-9be5-488d-9e80-d1f9c546a4e3"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("9c6ad87d-b16a-4773-8e9f-af71088d848d"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("a9ad77fe-9096-44ae-b239-da190229ff92"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("aa5997b6-e887-43be-bcd5-3285a2a5edaa"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("bcc8b91e-78c7-449f-98fe-702c3ce0680e"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("cc7e2d5e-8fbe-44b7-b46d-f7c953f36453"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("deb360a4-728a-44a3-9450-13999832128c"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("e33025cd-dbc8-4ae2-9d64-a16d04a65896"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("f8ac51be-bafe-4f7a-bcaa-28c18691d057"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("fb9ae4bc-e6d6-4c8d-a0be-eba5496c77cd"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("14f125e6-0aba-4f86-9c58-f725026e945e"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("18113347-addf-4b30-a8c3-5dda7ef744a9"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("256d5428-6baa-408a-98b7-80abffb6d05f"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("26c4bcfc-935a-4a3e-bc39-e995aa117a4a"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("2bf7759f-8e07-4415-b620-182f259237c6"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("2ef11211-e751-4d9f-885e-8071bdd22e48"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("4454473e-9573-4454-900e-6415ffbc6fb8"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("4f9e14aa-3fb5-4235-9066-d9ab835a891a"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("52ae14e3-33bc-4d5c-a9bc-09c0afb88e00"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("60f2f2cf-8052-47ab-ad8a-948a4b60a1d8"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("87e2c0e3-e7b3-4659-9725-bd48052fe187"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("946c72f1-2bfc-423b-a43b-92ca35669b85"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("b0987c84-5552-4b87-ae09-0400a1e86b4d"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("b6d5154e-4db9-4b1d-b33c-16f766fafc2f"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("b70c6f3b-4b1a-47e1-98de-24c133a33f62"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("d38ab07a-c1c8-4ead-a8fc-6aa64767866c"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("ddfb2078-91ef-469e-a019-ae8a5a6b95a5"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("f1b152e3-7ba8-4c85-8d0b-03f7266a00b4"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("f7f4c29f-e67f-4a5a-ade8-5e65f72b005a"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("fcb8d5a3-818c-4f3f-9241-3907d826794e"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("84b25c90-772b-4eb9-a6b6-c80b22217fc7"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("941d0105-2b53-4f34-a1ed-b524f9ace923"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("afa94f84-fd29-466c-af20-9a3dc59046e6"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("bf262f56-2f19-47a8-8c0b-cefbbd055d2f"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("e7492b0c-6e9d-471e-91bb-1f274af037ca"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("2042dbce-edbb-4923-914f-e2d485cedb87"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("2e707290-83df-4b8e-8419-7c425bc444be"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("80a66b63-61f3-4968-afa2-1eaf50dd2b7d"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("c042988d-f630-4e8e-bd3f-e3ccb3e39158"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("d0132dbf-3235-429e-846a-3eec0d72a88a"));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("297d06e6-c058-486f-a18a-06a971ebfcd7"),
                column: "ConcurrencyStamp",
                value: "abca1492-3078-4563-a6db-80f2dce6efba");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("6c8fcd7e-62f6-4f3e-a73d-acbfd60b97ab"),
                column: "ConcurrencyStamp",
                value: "b9379f73-715e-4946-9210-9ea21dc76da4");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7f66990c-36bc-4381-9f81-32e06e168319"),
                column: "ConcurrencyStamp",
                value: "d7629922-756f-4dad-89f2-418c5ed14dff");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb93"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "91efa28c-2d24-4dcc-a40a-c257d70728c3", new DateTime(2020, 6, 1, 9, 4, 57, 669, DateTimeKind.Utc).AddTicks(1347), "AQAAAAEAACcQAAAAENxsl9VaY00Flto0mYPbvu86wjcO0zlnCcJHtBlY+SrXFoDBwUPg7KT6rcgPwCY+TA==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb94"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "587b7adf-386d-44ce-a38f-737a7f538625", new DateTime(2020, 6, 1, 9, 4, 57, 669, DateTimeKind.Utc).AddTicks(3609), "AQAAAAEAACcQAAAAELDW27nnvdn2eI9zarFZyuAmitQBX1IXOb53Eo9DMGNIgqK1NwVpMCR8WJdUpInIDQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb95"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "be287279-5b06-4d39-9986-d5dec37de862", new DateTime(2020, 6, 1, 9, 4, 57, 669, DateTimeKind.Utc).AddTicks(3639), "AQAAAAEAACcQAAAAELJMA04fStLlSdRyvDlwOIyK/bFxyFk4wDNM0h2n9XtNe1z6RZfC9Dyq6D2vuCXqmw==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb96"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "3fb411f5-a298-410b-ad95-26faebee551c", new DateTime(2020, 6, 1, 9, 4, 57, 669, DateTimeKind.Utc).AddTicks(3647), "AQAAAAEAACcQAAAAEKNLvv6nPGoEXBWjQ163s0+ZvThLxzn6xfr9Sezv41oV68588mENQFf/4+gSJ9sv0w==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb97"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "028f4c1c-bb8f-49eb-84b3-d189bafded76", new DateTime(2020, 6, 1, 9, 4, 57, 669, DateTimeKind.Utc).AddTicks(3654), "AQAAAAEAACcQAAAAEMDBtpCoWb72ozDBhxSkYoDftRX5aXa6XcFgrn7QUVx339joLxznwD235qy4tPP7Uw==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb98"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "e2b9e8db-ce84-4c6b-bead-dc452211fd53", new DateTime(2020, 6, 1, 9, 4, 57, 669, DateTimeKind.Utc).AddTicks(3676), "AQAAAAEAACcQAAAAEI44gtrdGIGUzoWQY3OY25yQ+AEdCITRMTL5/TtBYVSDNPtqgPkQq0WdgeiV3AuJ+Q==" });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "Bio", "ImagePath", "UserId" },
                values: new object[,]
                {
                    { new Guid("7ea3ac4c-54a2-4976-900b-46109a855bca"), "Jim Doe has been involved with ICT research in Africa since 1997 and has participated in diverse research projects in 14 African countries. He started his career in 1994 as a public relations officer with Software Technologies Limited (an East African Oracle distributor), before serving as a research analyst/project officer for Telecom Forum Africa in 1997.", "3.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb94") },
                    { new Guid("87328894-95df-4b0c-a06c-4e94af259d53"), "Rosa James has a background in journalism and has been published in various local, regional, and international magazines and newspapers. She has spoken at various industry events across sub-Saharan Africa including countries like Ethiopia, Ghana, Kenya, Nigeria, Rwanda, Tanzania and Zambia as well as leading and facilitating at IDC’s events in the region. Rose is also a published author of a children’s novel.", "1.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb95") },
                    { new Guid("61671f8b-807e-4456-be06-023a370300ce"), "Cate Williams has been an analyst InsightHub for several years. Early on as an analyst she covered CRM applications, but more recently her work has been in integration middleware covering markets such as API management, file sync and share, and B2B integration.", "2.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb96") },
                    { new Guid("eaa27e0f-57e2-4742-86c6-a1d29bb4836a"), "Max Upton has more than 15 years of experience in business research, focusing on financial services and business innovation. His consulting work for InsightHub has allowed him to work closely with leading banks and regulators in the region for their technology and innovation strategies. Mr. Upton is concurrently head of InsightHub's business and operations in Thailand.", "7.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb97") },
                    { new Guid("600cd1da-f44b-4565-8af9-e71570fe3eeb"), "Previously, David Scott served for ten years as Enterprise Architect at CareFirst Blue Cross/Blue Shield, responsible for building business and technology alignment roadmaps for the ACA/HIX Health Reform, Mandated Implementations, PCMH, and Provider Network domains to ensure alignment of annual and tactical IT project planning with business goals.", "8.jpg", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb98") }
                });

            migrationBuilder.InsertData(
                table: "Industries",
                columns: new[] { "Id", "DeletedOn", "ImagePath", "IsDeleted", "ModifiedOn", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("4f90452e-996a-46b5-8c2e-456c96d0fe4e"), null, null, false, null, "Energy", "ENERGY" },
                    { new Guid("3b514467-a845-44fe-b377-8ddd06b2441e"), null, null, false, null, "Information Technology", "INFORMATION TECHNOLOGY" },
                    { new Guid("620e6d15-4136-41c6-a557-639b1cd7b2ba"), null, null, false, null, "Healthcare", "HEALTHCARE" },
                    { new Guid("754dcfe5-a1d9-405b-aafd-2a3487b4567d"), null, null, false, null, "Finance", "FINANCE" },
                    { new Guid("834a162a-03e7-49a7-843f-5a0f8d1f03a1"), null, null, false, null, "Marketing", "MARKETING" }
                });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "Id", "DeletedOn", "IsDeleted", "ModifiedOn", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("54c18cc5-5f76-46a2-ae1a-fd3f1a024e8a"), null, false, null, "computers", "COMPUTERS" },
                    { new Guid("333f73a6-08d1-4e4d-9a1a-22269c998ba1"), null, false, null, "hospital", "HOSPITAL" },
                    { new Guid("b7a6689d-f714-4dea-b9b9-257b33dfcafc"), null, false, null, "influence", "INFLUENCE" },
                    { new Guid("7bc6d929-4740-4cfe-8814-fd53431e92c0"), null, false, null, "trend", "TREND" },
                    { new Guid("384e72b3-a935-4bd4-a5c2-7ac7e3863454"), null, false, null, "sales", "SALES" },
                    { new Guid("25602b6f-ed60-45dc-9e43-3a00327bb71f"), null, false, null, "profit", "PROFIT" },
                    { new Guid("ba91ddc9-4dcd-44c4-8cda-fdcd8f686675"), null, false, null, "insurance", "INSURANCE" },
                    { new Guid("5f25be49-108e-4a63-bff8-40155801017e"), null, false, null, "stock", "STOCK" },
                    { new Guid("2767c5f8-c6d0-4767-adac-dd343abbf2e5"), null, false, null, "nuclear", "NUCLEAR" },
                    { new Guid("a64cff24-7e56-44b1-8b9e-40a5b070d26c"), null, false, null, "solar", "SOLAR" },
                    { new Guid("40b86e5e-e45a-486f-b453-45dc0d2cebfc"), null, false, null, "oil", "OIL" },
                    { new Guid("1f84fff5-bd7a-412e-b3b6-a419d7b1eaff"), null, false, null, "laboratory", "LABORATORY" },
                    { new Guid("c5289719-be84-4bc3-8698-c760d6db441c"), null, false, null, "vaccine", "VACCINE" },
                    { new Guid("cfbce58e-e6af-4354-aca9-d88e478ac314"), null, false, null, "research", "RESEARCH" },
                    { new Guid("389cedc2-c792-4873-b472-ed551f3b9aad"), null, false, null, "medicine", "MEDICINE" },
                    { new Guid("f1280acc-4bcb-4b79-b4ff-a5f7500e8fc3"), null, false, null, "web", "WEB" },
                    { new Guid("fce9a92b-500e-4fb1-9592-bed874da3728"), null, false, null, "hardware", "HARDWARE" },
                    { new Guid("404a15c9-7429-4eb7-9db4-21b2491e51e8"), null, false, null, "software", "SOFTWARE" },
                    { new Guid("2ee72174-c156-4922-aa11-0eff2e380093"), null, false, null, "investment", "INVESTMENT" },
                    { new Guid("aea7fa6b-9efd-485a-ba76-7a037b1086af"), null, false, null, "gas", "GAS" }
                });

            migrationBuilder.InsertData(
                table: "Reports",
                columns: new[] { "Id", "AuthorId", "Content", "DeletedOn", "ImagePath", "IndustryId", "IsApproved", "IsDeleted", "IsFeatured", "ModifiedOn", "Name", "NormalizedName", "Summary" },
                values: new object[,]
                {
                    { new Guid("355a5520-1d12-41c5-9143-1321c2af841b"), new Guid("61671f8b-807e-4456-be06-023a370300ce"), null, null, null, new Guid("4f90452e-996a-46b5-8c2e-456c96d0fe4e"), true, false, false, null, "North America Utilities Digital Transformation Tactics", "ENERGY INSIGHTS: NORTH AMERICA UTILITIES DIGITAL TRANSFORMATION TACTICS", "Digital transformation is driving utilities to change their business and operating and information models. The Energy Insights: North America Utilities Digital Transformation Strategies service is designed to help North American utility IT and business management understand the disruptions that are transforming the energy and utility value chains and develop the strategies and programs to capitalize on the evolving opportunities. The service provides deep research and insight on the key topics that matter to North American utility decision makers and IT executives affected by the digital transformation. Through an integrated set of deliverables, subscribers gain access to analysis and insights on the impact of new technologies, IT strategies and best practices, and the industry, governmental, and regulatory forces that are shaping the evolution to new industry business models in the industry's generation, transmission and distribution/pipelines, and retail sectors. This service delivers research, insight, and guidance on IT strategy and investments to meet the utility's most critical corporate objectives." },
                    { new Guid("68bb4739-51da-4e62-83c6-b5642930ee0f"), new Guid("600cd1da-f44b-4565-8af9-e71570fe3eeb"), null, null, null, new Guid("834a162a-03e7-49a7-843f-5a0f8d1f03a1"), true, false, false, null, "Marketing and Sales Solutions", "MARKETING AND SALES SOLUTIONS", "Marketing and sales technologies are driving forces for all companies as customers move to online, mobile-first, and collaborative relationship models. In reaching and selling to customers, it is essential that organizations aggressively develop the necessary operational and analytic skills, collaborative cultures, and creative problem solving needed to truly add value to the customer relationship. 's Marketing and Sales Solutions service provides strategic frameworks for thinking about the individual and aligned areas of marketing and sales technology as parts of a holistic business strategy. This program delivers insight, information, and data on the main drivers for the adoption of these technologies in the broader context of customer experience (CX) and networked business strategies.Markets and Subjects Analyzed. Marketing automation, campaign management, and go to market execution applications. Sales force automation applications. Predictive analytics and business KPIs. Mobile and digital applications and strategies. Customer data and analytics. CX strategies. Core Research. Reports on how 670 U.S. large enterprises use more than 300 vendors in 15 martech categories across 5 vertical markets:Consumer banking; Retail; CPG manufacturing; Securities and investment services; Travel and hospitality; MarketScape(s) on related solutions areas such as marketing clouds and artificial intelligence; TechScapes and PlanScapes on various topics such as GDPR, account-based marketing, personalization, and mobile marketing; Marketing software forecast and vendor shares; Sales force automation forecast and vendor shares. In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment. " },
                    { new Guid("75eebe6b-5b26-42b1-848e-b60d3095b483"), new Guid("eaa27e0f-57e2-4742-86c6-a1d29bb4836a"), null, null, null, new Guid("834a162a-03e7-49a7-843f-5a0f8d1f03a1"), true, false, false, null, "CMO Advisory Service", "CMO ADVISORY SERVICE", "CMO Advisory Service guides marketing leaders as they master the science of marketing. Digital transformation offers CMOs the opportunity to become revenue drivers and architects of the customer experience. Leaders leverage 's deep industry knowledge, powerful quantitative models, peer-tested practices, and personalized guidance to advance their operations. Whether seeking fresh perspectives on core challenges or counsel on emerging developments, offers a trusted source of insight.Markets and Subjects Analyzed. Marketing Investment and Transformational Operations. Planning and budgeting investments in the marketing mix, marketing technology, and marketing operations, accountability, and attribution. Application of New Technology for Marketing. Guidance on the implications of new technologies such as artificial intelligence (AI), collaboration, and marketing automation solutions. Delivering the New Customer Experience. Mapping and responding to the B2B customer decision journey. The Future Marketing Organization. Organizational design, staff allocation benchmarks, role definition, talent development, shared services, organizational alignment, and change management. Developing core competencies: Content marketing, customer intelligence and analytics, integrated digital and social engagement, sales enablement, and loyalty and advocacy" },
                    { new Guid("d928b814-73d2-4393-ad87-3d030635c42e"), new Guid("600cd1da-f44b-4565-8af9-e71570fe3eeb"), null, null, null, new Guid("834a162a-03e7-49a7-843f-5a0f8d1f03a1"), true, false, false, null, "Digital Commerce Trends", "DIGITAL COMMERCE TRENDS", "The Digital Commerce subscription marketing intelligence service examines the competitive landscape, key trends, and differentiating factors of digital commerce, PIM, and CPQ application vendors; digital marketplaces; and business commerce networks. This includes the buying behavior of end users while purchasing products and services on digital commerce platforms. The service provides a worldwide perspective by looking at all forms of digital commerce transactions — including B2B, B2C, C2C, G2B, and B2B2C — across all vertical industries and regional markets. Markets and Subjects Analyzed. Digital commerce applications targeting businesses of all sizes and industries. Best-of-breed digital commerce applications in areas such as CPQ, payments and billing, order management, web content management, merchandising, site search, fulfillment, product information management, and inventory management. Business commerce networks that bring together functional applications for enterprise asset management, procurement, financials, sales, and human capital management.Enterprise partnership/integration strategies among digital commerce and marketing/content management vendors. Experience management — across channels (includes content management, commerce platforms, integration, and advanced analytics) for B2B, B2C, and B2B2C. Impact of cloud, social, mobile, and Big Data technologies on vendor strategies for employee, customer, supplier, partner, and asset engagement. Proliferation of public cloud microservice architectures. Mobile commerce (mobile app marketplaces, built-for-mobile commerce capabilities, and differentiating technology). Cognitive technologies and intelligent workflow impact on digital commerce applications" },
                    { new Guid("bccee8f7-cee6-4ef8-8a41-c61a248f038f"), new Guid("eaa27e0f-57e2-4742-86c6-a1d29bb4836a"), null, null, null, new Guid("834a162a-03e7-49a7-843f-5a0f8d1f03a1"), true, false, false, null, "Canadian Sales Accelerator: Datacenter Infrastructure", "CANADIAN SALES ACCELERATOR: DATACENTER INFRASTRUCTURE", "Canadian Sales Accelerator: Datacenter Infrastructure research program analyzes the Canadian datacenter (DC) infrastructure market in Canada. Designed to provide intelligence and strategic frameworks to technology sales professionals, field marketing teams, and channel managers to take action on key responsibilities related to the sales cycle, this service provides market sizing, vendor performance, forecasts, and market opportunities, as well as a variety of adjacent markets and segmentations from a quantitative standpoint. This advisory service will allow subscribers to identify opportunities tied to the future of infrastructure, tailor go-to-market strategies as well as product and services road maps to meet the growing demand of end users for infrastructure solutions. At the same time, it looks at vendors, partners, and customers from a qualitative standpoint, concerning needs and requirements, pain points and buying intentions, maturity levels, and adoption in and of new technologies. Markets and Subjects Analyzed Datacenter technologies (including software defined); server and storage; converged systems. Infrastructure ecosystem including partners and channels. Datacenter budget trends and dynamics and vendor selection/buying criteria. Technology investment expectations for legacy and next-gen DC infrastructure. In addition to the core research documents, clients will receive briefings and concise sales executive email alerts throughout the year. Every client will have a Sales Accelerator service launch integration meeting to kick-off the program. Core Research. Brand Perceptions on Enterprise Storage and Service Vendors. Market Forecasts. Vendor Dashboard: Market Shares. Infrastructure Ecosystem Barometer. Executive Market Insights" },
                    { new Guid("01d4afbe-6ab7-4a7b-ac17-36e8e6b198d4"), new Guid("7ea3ac4c-54a2-4976-900b-46109a855bca"), null, null, null, new Guid("754dcfe5-a1d9-405b-aafd-2a3487b4567d"), true, false, false, null, "International Corporate Banking Digital Transformation", "INTERNATIONAL CORPORATE BANKING DIGITAL TRANSFORMATION", "With growing competition from nonbank platforms and the commoditization of products, the ability to onboard, connect, and deliver services to corporate customers is becoming paramount. With PSD2 in Europe democratizing data access and payment services, corporate banks have to transform their value proposition to deliver new data-driven services to their clients and add value, rather than their traditional, commoditized product proposition. Reducing the connectivity cost, increasing speed of data delivery toward real time, and integrating bank systems with corporate enterprise resource planning (ERP), treasury management systems (TSM), payment factories, and in-house banks will be paramount to help corporate clients manage capital, monitor cash flows, and optimize their liquidity. These infrastructure upgrades will also be crucial to exploit the evolving ecosystems brought about by distributed ledger technology (DLT) and the internet of things (IoT), where data discovery, analysis, and sharing will accelerate trade and reduce risk. Sensors attached to goods in transit — from the manufacturing plant to the retail outlet — could offer opportunities to banks' cash management and trade services businesses, better matching flows of payments and goods between seller and buyer. The Financial Insights: Worldwide Corporate Banking Digital Transformation Strategies advisory service provides clients with insightful information and analysis of corporate and commercial banking trends, including cash and treasury, trade finance, commercial lending, and payments. It also provides coverage of how the impact of emerging 3rd Platform technologies like big data, analytics, cloud, mobility, IoT, blockchain, and cognitive technologies transforms the sector and how their convergence unlocks new business and operating models. The service offers timely, strategic, and actionable business IT and application advice to guide our clients as they undertake digital investment decisions, plan for future technology needs, and benchmark themselves against competitors. It provides ongoing research reports, analyst access, and admission to content-rich conferences and webcasts for the benefit of financial institutions as well as technology vendors." },
                    { new Guid("c6c70893-ca68-43f4-b467-01f68b0641f0"), new Guid("61671f8b-807e-4456-be06-023a370300ce"), null, null, null, new Guid("754dcfe5-a1d9-405b-aafd-2a3487b4567d"), true, false, false, null, "Insurance Digital Transformation Approach", "INSURANCE DIGITAL TRANSFORMATION APPROACH", "Customer experience is high on the agenda for most insurers and intermediaries as customers expect true value for the premiums they pay, above and beyond the traditional products and services delivered to them. To continue to be relevant in a changing marketplace, insurance organizations across the globe need to deliver contextual and value-centric insurance that is rooted on the principles of proactive risk management and secure, transparent, seamless, and contextual engagements across the customer journey. To achieve this, the industry needs to accelerate its effort to transform from a traditional, product-centric mindset to a customer-centric mindset enabled by digital technologies and the power of data and analytics. Insurance organizations need to increasingly play the role of true risk advisors rather than mere product sellers. As they progress in their transformation journey, they need advice and guidance to understand the opportunities and possibilities with the new technologies and to build and deploy digital capabilities while also tackling existing barriers to change. The Financial Insights: Worldwide Insurance Digital Transformation Strategies advisory service provides clients with insightful information and analysis of global insurance trends. It also provides coverage of how digital technologies like Big Data, analytics, cloud, mobility, IoT, blockchain, and cognitive technologies impact the life and annuity, accident and health, and property and casualty insurance markets. The service offers timely, strategic, and actionable business IT and application advice to guide our clients as they undertake digital investment decisions, plan for future technology needs, and benchmark themselves against competitors. It provides ongoing research reports, analyst access, and admission to content-rich conferences and webcasts for the benefit of insurance organizations as well as technology vendors." },
                    { new Guid("de583aae-c56b-47af-97a4-f429db0d81d4"), new Guid("600cd1da-f44b-4565-8af9-e71570fe3eeb"), null, null, null, new Guid("754dcfe5-a1d9-405b-aafd-2a3487b4567d"), true, false, false, null, "Worldwide Banking IT Spending Guide", "WORLDWIDE BANKING IT SPENDING GUIDE", "The Financial Insights: Worldwide Banking IT Spending Guide examines the banking industry opportunity from a technology, functional process, and geography perspective. This comprehensive database delivered via 's Customer Insights query tool allows the user to easily extract meaningful information about the banking technology market by viewing data trends and relationships and making data comparisons. Markets Covered This product covers the following segments of the banking market: 9 regions: USA, Canada, Japan, Western Europe, Central and Eastern Europe, Middle East and Africa, Latin America, PRC, and Asia/Pacific 4 technologies: Hardware, software, services, and internal IT spend 5 banking segments: Consumer banking, corporate and institutional banking, corporate administration, enterprise utilities, and shared services 30+ functional processes: Channels, payments, core processing, and more 4 company size tiers: Institution size by tier 1 through tier 4 2 institution types: Banks and credit unions 6 years of data Data Deliverables This spending guide is delivered on a semiannual basis via a web-based interface for online querying and downloads. For a complete delivery schedule, please contact an sales representative. The following are the deliverables for this spending guide: Annual five-year forecasts by regions, technologies, banking segments, functional processes, tiers, and institution types; delivered twice a year About This Spending Guide Financial Insights: Worldwide Banking IT Spending Guide provides guidance on the expected technology opportunity around this market at a regional and total worldwide level. Segmented by functional process, institution type, company size tier, region, and technology component, this guide provides IT vendors with insights into both large and rapidly growing segments of the banking technology market and how the market will develop over the coming years." },
                    { new Guid("798e41d2-e1a9-4930-aa94-1682e466a70f"), new Guid("87328894-95df-4b0c-a06c-4e94af259d53"), null, null, "", new Guid("754dcfe5-a1d9-405b-aafd-2a3487b4567d"), true, false, false, null, "Universal Payment Strategies", "UNIVERSAL PAYMENT STRATEGIES", "The payment industry has seen drastic changes in the past decade. New technologies, entrants, and business models have forced incumbent vendors and their financial institution customers to rethink how they move money. Stakeholders across the payment value chain — card-issuing banks, merchant acquirers, payment networks, and payment processors — face increasingly complex decisions. In this turbulent market, the players need more than facts and figures; they need critical analysis and insightful opinions. Markets and Subjects Analyzed Throughout the year, this service will address the following topics: Developing trends in payments such as omni-channel and alternative payment networks Evaluation and integration of new payment channels like voice commerce and IoT Enterprise risk, compliance, and fraud issues affecting payment products Legal and regulatory issues around the world that will affect how payments develop Middle- and back-office technologies that will affect the payment strategies of financial institutions Emerging technologies such as blockchain, AI, and next-generation security and their potential for altering the payment landscape Core Research MarketScape: Gateways for Integrated Payments B2B Payments: Digital Transformation in Transactions Retail Revolution: Beyond Card Payments Blockchain Payments: Short-Term Realities Persistent Payments: Automated Transactions in a Connected World Real-Time Payment Productization: Overlays in Action Retail Fraud: Protecting a Multi-Payment Environment" },
                    { new Guid("46f2cb6b-d2b2-4f32-899c-162270dec3c7"), new Guid("600cd1da-f44b-4565-8af9-e71570fe3eeb"), null, null, null, new Guid("754dcfe5-a1d9-405b-aafd-2a3487b4567d"), true, false, false, null, "Asia/Pacific Financial Services IT", "ASIA/PACIFIC FINANCIAL SERVICES IT", "Financial Insights: Asia/Pacific Banking Customer Centricity program provides insights into the evolving needs of Asia/Pacific retail/consumer banking retail customers and guidelines on how banks are to respond to these trends. The program will give advice to technology buyers on the technology that supports the customer centricity agenda of the financial institution – particularly in the areas of customer relationship management (CRM), omni-experience and omni-channel solutions, and in loyalty management. The service will be backed by a comprehensive consumer survey on the preferences of Asia/Pacific retail/consumer banking customers in various aspects of customer experience. Financial Insights will then undertake on how financial institutions are to develop an effective customer management agenda, delving into the concept, approach, strategy, use cases, and enabling technologies for customer centricity. Throughout the year, this service will address the following topics: Customer Experience Trends in Asia/Pacific Retail Banking How Asia/Pacific Consumers Pay - Evolving Trends in Retail Payments Customer Centricity in Retail Banking IT and Operations Customer Centricity in Marketing and Product Development Customer Centricity Technology Providers for Asia/Pacific Retail Banking." },
                    { new Guid("38e67af9-991e-4f71-9bdb-d95a011e0031"), new Guid("87328894-95df-4b0c-a06c-4e94af259d53"), null, null, null, new Guid("754dcfe5-a1d9-405b-aafd-2a3487b4567d"), true, false, false, null, "Consumer Banking Engagement Strategies", "CONSUMER BANKING ENGAGEMENT STRATEGIES", "Being successful in banking will be determined by how well institutions manage the transformation in both digital and physical channels. Customers have seemingly ubiquitous access to their accounts on their terms and on their devices as banks continue to strategize about the future of the branch network and new channels emerge. Unfortunately, many banks are still looking at their channel strategy in a silo without fully understanding that customer engagement is the key to a profitable relationship. Today’s technology has fostered this customer-led revolution, yet there are many more changes yet to be realized as new technology is introduced. Advances in how we engage the customer are pushing the limits and skill sets of business units, marketers, and IT personnel as customers demand more from their retail bank. The Financial Insights: Consumer Banking Engagement Strategies provides critical analysis of the opportunities and options facing banks as they wrestle with their technology plans and investment decisions in alignment with their strategic goals. This research delivers key insights regarding the business drivers of and value delivered from customer-facing banking technology investments. Topics Addressed Throughout the year, this service will address the following topics: Digital transformation: Strategies and use cases are developing as banks transform all channels, including account opening and onboarding, ATM and ITM, augmented and virtual reality, branch banking, call center, chatbot services, contextualized marketing, conversational banking, digital banking (online and mobile), and social business. Whether these are first-generation offerings or have been around for decades, strategies need to be developed to implement, support, and upgrade these channels to stay with the times. Engagement strategies: Financial institutions are realizing that the number of engagements a customer has is an important factor in profitability. Using big data and analytics to properly measure the number of engagements is a start, but most institutions need to go beyond a prescriptive approach to customer behavior to a more cognitive approach. Omni-experience: The customer life-cycle process offers multiple channels that define the experience and dictate current and future relationships. Customer trends and strategies: This includes topics from level of interaction today to what is likely to be future behavior." },
                    { new Guid("39312241-4545-4860-97b4-edf26671b1e9"), new Guid("87328894-95df-4b0c-a06c-4e94af259d53"), null, null, null, new Guid("620e6d15-4136-41c6-a557-639b1cd7b2ba"), true, false, false, null, "Maternal mortality has declined but progress is uneven across regions", "MATERNAL MORTALITY HAS DECLINED BUT PROGRESS IS UNEVEN ACROSS REGIONS", "A total of 295 000 [UI 1 80%: 279 000–340 000] women worldwide lost their lives during and following pregnancy and childbirth in 2017, with sub-Saharan Africa and South Asia accounting for approximately 86% of all maternal deaths worldwide. The global maternal mortality ratio (MMR, the number of maternal deaths per 100 000 live births) was estimated at 211 [UI 80%: 199–243], representing a 38% reduction since 2000. On average, global MMR declined by 2.9% every year between 2000 1 UI = uncertainty interval. and 2017. If the pace of progress accelerates enough to achieve the SDG target (reducing global MMR to less than 70 per 100 000 live births), it would save the lives of at least one million women. The majority of maternal deaths are preventable through appropriate management of pregnancy and care at birth, including antenatal care by trained health providers, assistance during childbirth by skilled health personnel, and care and support in the weeks after childbirth. Data from 2014 to 2019 indicate that approximately 81% of all births globally took place in the presence of skilled health personnel, an increase from 64% in the 2000–2006 period. In sub-Saharan Africa, where roughly 66% of the world’s maternal deaths occur, only 60% of births were assisted by skilled health personnel during the 2014–2019 period. " },
                    { new Guid("81e03de3-bd60-4eb6-bfe9-2f0b6170d311"), new Guid("7ea3ac4c-54a2-4976-900b-46109a855bca"), null, null, null, new Guid("620e6d15-4136-41c6-a557-639b1cd7b2ba"), true, false, false, null, "Investing in strengthening country health information systems", "INVESTING IN STRENGTHENING COUNTRY HEALTH INFORMATION SYSTEMS", "Accurate, timely, and comparable health-related statistics are essential for understanding population health trends. Decision-makers need the information to develop appropriate policies, allocate resources and prioritize interventions. For almost a fifth of countries, over half of the indicators have no recent primary or direct underlying data. Data gaps and lags prevent from truly understanding who is being included or left aside and take timely and appropriate action. The existing SDG indicators address a broad range of health aspects but do not capture the breadth of population health outcomes and determinants. Monitoring and evaluating population health thus goes beyond the indicators covered in this report and often requires additional and improved measurements. WHO is committed to supporting Member States to make improvements in surveillance and health information systems. These improvements will enhance the scope and quality of health information and standardize processes to generate comparable estimates at the global level. Getting accurate data on COVID-19 related deaths has been a challenge. The COVID-19 pandemic underscores the serious gaps in timely, reliable, accessible and actionable data and measurements that compromise preparedness, prevention and response to health emergencies. The International Health Regulations (IHR) (2005) monitoring framework is one of the data collection tools that have demonstrated value in evaluating and building country capacities to prevent, detect, assess, report and respond to public health emergencies. From self-assessment of the 13 core capacities in 2019, countries have shown steady progress across almost all capacities including surveillance, laboratory and coordination. As the pandemic progresses, objective and comparable data are crucial to determine the effectiveness of different national strategies used to mitigate and suppress, and thus to better prepare for the probable continuation of the epidemic over the next year or more." },
                    { new Guid("6911bcf0-6152-4282-a842-cd5814a27cd3"), new Guid("87328894-95df-4b0c-a06c-4e94af259d53"), null, null, null, new Guid("620e6d15-4136-41c6-a557-639b1cd7b2ba"), true, false, false, null, "Health systems and universal health coverage", "HEALTH SYSTEMS AND UNIVERSAL HEALTH COVERAGE", "In the SDG monitoring framework, progress towards universal health coverage (UHC) is tracked with two indicators: (i) a service coverage index (which measures coverage of selected essential health services on a scale of 0 to 100); and (ii) the proportion of the population with large out-of-pocket expenditures on health care (which measures the incidence of catastrophic health spending, rendered as percentage). The service coverage index improved from 45 globally in 2000 to 66 in 2017, with the strongest increase in low-and lower-middle-income countries, where the baseline at 2000 was lowest. However, the pace of that progress has slowed since 2010. The improvements are especially notable for infectious disease interventions and, to a lesser extent, for reproductive, maternal and child health services. Within countries, coverage of the latter services is typically lower in poorer households than in richer households. Overall, between one third and one half the world’s population (33% to 49%) was covered by essential health services in 2017 . Service coverage continued to be lower in low- and middle-income countries than in wealthier ones; the same held for health workforce densities and immunization coverage (Figure 1.2). Available data indicate that over 40% of all countries have fewer than 10 medical doctors per 10 000 people, over 55% have fewer." },
                    { new Guid("05157698-c500-4605-991a-b555a3f16ccf"), new Guid("7ea3ac4c-54a2-4976-900b-46109a855bca"), null, null, null, new Guid("620e6d15-4136-41c6-a557-639b1cd7b2ba"), true, false, false, null, "Health Insights: European Life Science and Pharma", "HEALTH INSIGHTS: EUROPEAN LIFE SCIENCE AND PHARMA", "Digital transformation is key in the European life science industry's ongoing efforts to transform itself as it seeks to find new paths to innovation, optimize operational efficiency and effectiveness, and regain long-term sustainability. The transition toward new care delivery ecosystems and outcome-based reimbursement models enhances life science organizations' needs to implement new approaches to information management and use. The digital mission in the life sciences is to integrate new and existing data, information, and knowledge into a more knowledge-centric approach to new drug and medical devices development, to product commercialization and management, and to treatment delivery. The European life science digital transformation service provides a forward-looking analysis of IT and technologies that are being adopted all along the value chain of the European life science industry. The service focuses on how European life science organizations can leverage the new range of technologies underpinned by the 3rd Platform technologies and innovation accelerators to support efforts to improve the operational efficiency of the industry and better engage with the ultimate end user of life science innovations, namely patients. This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("b60fcff9-4680-4d7e-b2fd-8236cdc48137"), new Guid("87328894-95df-4b0c-a06c-4e94af259d53"), null, null, null, new Guid("620e6d15-4136-41c6-a557-639b1cd7b2ba"), true, false, false, null, "European Digital Hospital Evolution", "EUROPEAN DIGITAL HOSPITAL EVOLUTION", "European Digital Hospital service provides detailed information about the evolution of digital hospital transformation and technology adoption. It offers valuable insights into IT solution trends within the hospital sector, including healthcare-specific technologies such as electronic health records; admission, discharge, and transfer; digital medical imaging systems; VNA and archiving; and business intelligence and patient management systems. This service focuses on a vital component of healthcare — the digital hospital. Healthcare systems across the world are facing care quality and sustainability challenges that require a new approach to care delivery and reimbursement models. The fully digital hospital is in the center of that journey, being a key enabler of change. The hospital CIOs are facing new technology but, at the same time, a growing legacy IT burden that must be managed alongside of new digital initiatives. In healthcare, management of legacy systems is therefore a growing challenge — when looking at not only obsolete digital applications but also infrastructure and, in the end, how healthcare providers purchase digital applications and platforms. When IT fails to deliver because of legacy and lack of agility, the business model and clinical processes become legacy as well. Key solutions such as EHR, RIS, PACS, VNA, ECM, advanced analytics, cloud, and mobility will fundamentally impact hospitals' digital strategies execution and success.This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("140b844b-fb58-419c-a189-27298f026ad8"), new Guid("7ea3ac4c-54a2-4976-900b-46109a855bca"), null, null, null, new Guid("620e6d15-4136-41c6-a557-639b1cd7b2ba"), true, false, false, null, "European Value-Based Healthcare Digital Transformation", "EUROPEAN VALUE-BASED HEALTHCARE DIGITAL TRANSFORMATION", " With the advent of the value-based paradigm, healthcare organizations are today tasked to ensure equitable access and financial sustainability while improving and reducing the variation of clinical outcomes in healthcare systems. Increasing collaboration across the healthcare value chain, adopting a more personalized approach to treatment, championing the patient experience, and adopting outcome-based business models are fast emerging as necessary for the healthcare industry to succeed. The IDC Health Insights: European Value-Based Healthcare Digital Transformation Strategies service focuses on analyzing European healthcare providers, payers, and public health policy makers' digital strategy and best practices in supporting the adoption of value-based healthcare to deliver the maximum value to patients. Value-based healthcare is a key driver of digital transformation as it requires an unprecedented level of availability of patient information. Key solutions as HIE, population health management, smart patient engagement tools, advanced analytics and cognitive, and cloud and mobility will fundamentally impact care providers and payers' value-based healthcare strategies execution and success. Approach. This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("8126ebac-1fb2-463f-8450-dff360719ac9"), new Guid("600cd1da-f44b-4565-8af9-e71570fe3eeb"), null, null, null, new Guid("3b514467-a845-44fe-b377-8ddd06b2441e"), true, false, false, null, "Cybersecurity Analytics, Intelligence, Response and Orchestration", "CYBERSECURITY ANALYTICS, INTELLIGENCE, RESPONSE AND ORCHESTRATION", "Cybersecurity Analytics, Intelligence, Response and Orchestration service covers security software and hardware products related to analytic security platforms, security and vulnerability management (SVM), and security orchestration platforms. Specific functions covered include vulnerability management and intelligence, SIEM, security analytics, threat hunting, incident detection and response, and orchestration. The service is designed to create in-depth coverage of the analytic-based and platform security markets. Markets and Subjects Analyzed Vulnerability management SIEM Security analytics Incident detection and response Threat analytics Automation Orchestration Core Research Security AIRO Forecast Security AIRO Market Share SIEM Market Share and Forecast Market MAP In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment. Key Questions Answered What is the size and market opportunity for security analytics solutions? Who are the major players in the security analytics space? What is the size and market opportunity for security orchestration solutions? What is the size and market opportunity for threat analytics solutions? How has the competitive landscape changed through digital transformation and adoption of cloud and enabling technologies?" },
                    { new Guid("473e2f86-911b-4d81-b9d0-847dc26a1ed2"), new Guid("eaa27e0f-57e2-4742-86c6-a1d29bb4836a"), null, null, null, new Guid("3b514467-a845-44fe-b377-8ddd06b2441e"), true, false, false, null, "DevOps Analytics, Automation and Security", "DEVOPS ANALYTICS, AUTOMATION AND SECURITY", "DevOps is a powerful modern approach to unifying the business strategy, development, testing, deployment, and life-cycle operation of software. This approach is accomplished by improving business, IT, and development collaboration while taking full advantage of automation technologies, end-to-end processes, microservices architecture, and cloud infrastructure to accelerate development and delivery and enable innovation. This subscription service provides insight, forecasts, and thought leadership to assist IT management, IT professionals, IT vendors, and service providers in creating compelling DevOps strategies and solutions. Markets and Subjects Analyzed DevOps adoption drivers, benefits, and use cases Identification of DevOps innovators and best practices Identification of critical DevOps tools enabling automation, open source technologies, and market leaders Analysis of the impact DevOps is having on IT infrastructure hardware and software purchasing and deployment priorities across on-premises and cloud service platforms Major transformation and impact DevOps is having on staffing, skills, and internal processes Core Research Worldwide DevOps Software Forecast, 2018-2022 Worldwide DevOps Software Market Share, 2018-2022 The Role of Cloud-Based IDEs to Drive DevOps Adoption Changing Drivers of DevOps: Role of Serverless and Microservices/Cloud-Native Architectures Market Analysis Perspective: Worldwide Developer, Operations, and DevOps Evolving Approaches and Practices in DevOps The Shift to Continuous Delivery and Deployment Tools and Frameworks for Supporting DevOps Workflows In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("ee80d6ae-6818-4d92-bad1-e2e8a4595817"), new Guid("61671f8b-807e-4456-be06-023a370300ce"), null, null, null, new Guid("3b514467-a845-44fe-b377-8ddd06b2441e"), true, false, false, null, "Multicloud Data Management and Protection", "MULTICLOUD DATA MANAGEMENT AND PROTECTION", "Multicloud Data Management and Protection service enables storage vendors and IT users to have a more comprehensive view of the evolving data protection, availability, and recovery market. Integrating cloud technology with traditional hardware and software components, the report series will include market forecasts and provide a combination of timely tactical market information and long-term strategic analysis. Markets and Subjects Analyzed Cloud-based solutions, including backup as a service (BaaS) and disaster recovery as a service (DRaaS) Public, private, and hybrid cloud data protection technologies Disk-based data protection and recovery solutions packaged as software, appliance, or gateway system with a focus on technology evolution Market forecast based on total terabytes shipped and revenue for disk-based data protection and recovery (No vendor shares will be published.) End-user adoption of different data protection and recovery technologies and solutions Cloud service providers delivering cloud-based data protection Data protection for nontraditional data types (e.g., Mongo DB, Cassandra, Hadoop) Vendors delivering disk-based data protection and recovery solutions with focus on architecture based on use cases and applications Technologies and processes, including backup, snapshots, replication, continuous data protection, and data deduplication Impact of purpose-built backup appliances (PBBAs) and hyperconverged systems on overall market growth and customer adoption Implications of copy data management and impact on data availability, capacity management, cost, governance, and security The evolutionary impact of virtual infrastructure and object storage on data protection schemes Implications of flash technology for data protection Data protection best practice guidelines and adoption Core Research Market Analysis, Sizing, and Vendor Shares of Backup as a Service and Disaster Recovery as a Service Use of Disk-Based Technologies for Protection and Recovery Adoption Patterns and Role of PBBAs in Customer Environments End-User Needs and Requirements Data Protection and Recovery Software Market Size, Shares, and Forecasts Disk-Based Data Protection Market Size and Forecast Virtual Backup Appliance Solutions Intersection of Data Protection Software and Management with Cloud Services In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("1ad9cd50-7bdb-4189-a945-1377819579f7"), new Guid("61671f8b-807e-4456-be06-023a370300ce"), null, null, null, new Guid("3b514467-a845-44fe-b377-8ddd06b2441e"), true, false, false, null, "Software Channels and Ecosystems", "SOFTWARE CHANNELS AND ECOSYSTEMS", "Software Channels and Ecosystems research service offers intelligence and expertise to help channel executives and program managers develop, implement, support, and manage effective channel strategies and programs to drive successful relationships with the spectrum of partner activities (e.g., resale, managed/cloud services, consulting professional services, and software development). In addition, it provides a comprehensive view of the value of the indirect software market and its leading vendor proponents. This service also identifies and analyzes key industry trends and their impact on channel relationship drivers and channel partner business models. Subscribers are invited to 's semiannual Software Channel Leadership Council where and channel executives present and discuss key industry issues. Markets and Subjects Analyzed Multinational software vendors Digital transformation in the partner ecosystem Software and SaaS channel revenue flow Digital marketplaces Cloud and channels Partner-to-partner networking Key channel trends Software partner program profiles and innovative practices Core Research Channel Revenue Forecast and Analysis Partner Marketing and Communications Ecosystem Digital Transformation Cloud and Channels Emerging Partner Business Models Partner Collaboration." },
                    { new Guid("ddd61f39-65ae-4e4a-b30a-1e16be57a388"), new Guid("87328894-95df-4b0c-a06c-4e94af259d53"), null, null, null, new Guid("3b514467-a845-44fe-b377-8ddd06b2441e"), true, false, false, null, "Agile Application Life-Cycle, Quality and Portfolio Strategies", "AGILE APPLICATION LIFE-CYCLE, QUALITY AND PORTFOLIO STRATEGIES", "Agile Application Life-Cycle, Quality and Portfolio Strategies service provides insight into business and IT alignment through end-to-end application life-cycle management (ALM) and DevOps strategies including agile, IT portfolio management, software quality, testing and verification tools, software change and configuration, and continuous releases for digital transformation. This service provides insights on how product, organizational, and process transitions can help address regulatory compliance, security, and licensing issues covering cloud, SaaS, and open source software. It also analyzes trends related to ALM and software deployment with applications that leverage and target artificial intelligence (AI) and machine learning (ML), collaboration, mobile and embedded apps, IoT, virtualization, and complex sourcing and evolve to adaptive work, project and portfolio management (PPM), and agile adoption with organizational and process strategies (e.g., Scaled Agile Framework [SAFe]). Markets and Subjects Analyzed Software life-cycle process, requirements, configuration management and collaboration, and agile development and projects Software quality, including mobile, cloud, and embedded strategies PPM and work management software trends and strategies IT project and portfolio management software Developer, business, and operations trends Tools enabling DevOps and governance solutions to align complex deployment, financial management, infrastructure, and business priorities Trends in software configuration management (SCM), including connecting business to IT via requirements management and web services demands on agile life-cycle management solutions Automated software quality (ASQ) tools evolution: Hosted testing, web services, API and cloud testing, service virtualization, vulnerability, risk and value-based testing, and other emerging technologies (e.g., mobile, analysis, and metrics) Trends in collaborative software development with social media" },
                    { new Guid("4b9e68b5-fe3f-479a-a7b8-a1663a2ce1ed"), new Guid("7ea3ac4c-54a2-4976-900b-46109a855bca"), null, null, null, new Guid("3b514467-a845-44fe-b377-8ddd06b2441e"), true, false, false, null, "Canadian Internet of Things Ecosystem and Trends", "CANADIAN INTERNET OF THINGS ECOSYSTEM AND TRENDS", "The Internet of Things (IoT) market is poised for exponential growth over the next several years. However, the IoT ecosystem is a complex segment with multiple layers and hundreds of players, including device and module manufacturers, communication service providers (CSPs), IoT platform players, applications, analytics and security software vendors, and professional services providers. 's Canadian Internet of Things Ecosystem and Trends service analyzes the regionally specific growth of this market from the autonomously connected units in the enterprise world to the applications and services that will demonstrate the power of a world of connected .Markets and Subjects Analyzed Market size and forecast for the Canadian IoT market Canadian IoT revenue forecast by industry Canadian IoT revenue forecast by use cases Taxonomy of the IoT ecosystem: How is the market organized, and what are the key segments and technologies? Canadian-specific demand-side data from various segments (e.g., IT and LOB) Analysis of Canadian-specific vendor readiness and go-to-market strategies for IoT Vertical use cases/case studies of IoT Core Research Taxonomy of the IoT Ecosystem Forecast for the Canadian IoT Market by Industry Forecast for the Canadian IoT Market by Use Cases Profiles of Canadian Vendors Leading IoT Evolution Case Studies of Successful Canadian IoT Implementations Canadian Internet of Things Decision-Maker Survey In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("007294a4-a7f5-4595-b0ae-f9d561a739ab"), new Guid("600cd1da-f44b-4565-8af9-e71570fe3eeb"), null, null, null, new Guid("4f90452e-996a-46b5-8c2e-456c96d0fe4e"), true, false, false, null, "Worldwide Utilities Connected Asset", "WORLDWIDE UTILITIES CONNECTED ASSET", "Energy and digital technologies are radically transforming utilities asset operations and strategies. Renewables and decentralized power generation, energy delivery networks (electricity, gas, and heat), and water and waste water management benefit from the developments and adoption of IoT, AI, and digital twins.  Energy Insights: Worldwide Utilities Connected Asset Strategies service focuses on all these. It provides guidance to end users in terms of digital transformation (DX) use cases road map, analysis of emerging IT trends, and evaluation of technology providers in this space. It looks into the future of work in the utilities value chain context. At the same time, it provides technology vendors a view on utilities' operational excellence asset strategies addressing their go to market.This service develops unique analysis and comprehensive data through  Energy Insights' proprietary research projects, along with ongoing communications with industry experts, utility management, ICT vendors, and service providers. With decades of experience in the utility industry, our analysts leverage a broad spectrum of expertise and intellectual property from both  and  Energy Insights. Research documents elucidate business strategy, best practices, technology selection, and vendor assessment, along with short perspectives on topical issues. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research documents." },
                    { new Guid("829daa92-8582-4e90-a3e5-2cd5bee42561"), new Guid("61671f8b-807e-4456-be06-023a370300ce"), null, null, null, new Guid("4f90452e-996a-46b5-8c2e-456c96d0fe4e"), true, false, false, null, "Worldwide Mining Policy", "WORLDWIDE MINING POLICY", "Worldwide Mining Strategies examines the business environment across the mining sector value chain from exploration through to operations, processing, supply chain, and trading globally. Mining companies are operating in an increasingly competitive commodity market environment, driven by pressures relating to accessing funding, assets, and talent. Technology and data are playing an increasingly critical role across the operation to enable decision support, automation, integration, and control. This service provides comprehensive insights into the best practices that show how mining companies are responding and what road maps these companies will need to build the information technology (IT) capabilities required to create integrated, agile, and responsive operations. Mining companies are changing the way they buy technology, how they collaborate to frame the problems to be solved, how they engage with technology suppliers, and how they innovate across their businesses bringing together the capabilities of IT and operational technology (OT). This service tracks the IT investment priorities for organizations seeking to scale value creation across their organization and the impact on decision making and best practices relating to technology, process, and organizational change. This service distills market and industry data into incisive analysis drawn from in-depth interviews with industry experts, mining staff from across the business, and technology vendors. Insight and analysis are further supported and validated through rigorous research methodologies in quantitative market analysis.  Energy Insights' analysts develop unique and comprehensive analyses of this data, focused on providing actionable recommendations. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("da58dd14-40d6-44e4-b561-29f75f3efe4e"), new Guid("eaa27e0f-57e2-4742-86c6-a1d29bb4836a"), null, null, null, new Guid("4f90452e-996a-46b5-8c2e-456c96d0fe4e"), true, false, false, null, "Asia/Pacific Oil and Gas Digital Transformation Procedure", "ASIA/PACIFIC OIL AND GAS DIGITAL TRANSFORMATION PROCEDURE", "Asia/Pacific Oil and Gas Digital Transformation Strategies examines the business environment of the oil and gas industry in the Asia/Pacific region, excluding Japan. The service covers the whole value chain from upstream, midstream, and downstream but is focused on upstream. It seeks to support companies working to deliver against efficiency objectives through technology-led change that will deliver improved decision support and insight and flexibility to adjust to changing technology, markets, and political pressures. The Energy Insights: Asia/Pacific Oil and Gas Digital Transformation Strategies program provides advice on best practices and technology priorities that will support technology decision making, particularly, in relation to innovation, data management, and greater integration across silos and processes. The service is designed to support oil and gas companies with market insights and road mapping advice relating to technology decision making, particularly delivering insight to how increased value from investments can be delivered through better understanding of new metric, organization, talent, and process change. Key technology focus areas of the program include IT/OT integration, operational cybersecurity, IoT strategies, and asset management priorities. This service is built on the foundations of extensive engagement and research by the Energy Insights team across the oil and gas industry in this region. Our analysts work with industry experts, staff from the oil and gas business, and technology vendors to ensure that the research service is relevant and prioritizes areas that are of importance to them and the industry at large." },
                    { new Guid("3e6a559c-761c-444a-9cff-662b496fa01d"), new Guid("61671f8b-807e-4456-be06-023a370300ce"), null, null, null, new Guid("4f90452e-996a-46b5-8c2e-456c96d0fe4e"), true, false, false, null, "Global Utilities Customer Experience Scheme", "GLOBAL UTILITIES CUSTOMER EXPERIENCE SCHEME", "With the evolution of energy and water markets and the possibilities offered by digital transformation, utilities are developing new business models and are focusing on providing customers more interactive experiences. Smart metering, analytics, social media, mobility, cloud, and IoT are fundamentally impacting customer operation practices in both competitive and regulated markets. The Energy Insights: Worldwide Utilities Customer Experience Strategies service is designed to help utilities and energy retailers servicing customers in competitive and regulated markets at the worldwide level (including electricity, gas, and water). The service provides exclusive research and direct access to experts providing guidance to make the right IT investments and meet corporate objectives of customer satisfaction, reduction of cost to serve, and innovation. This service develops unique analysis and comprehensive data through Energy Insights' proprietary research projects, along with ongoing communications with industry experts, utility management, ICT vendors, and service providers. With decades of experience in the utility industry, our analysts leverage a broad spectrum of expertise and intellectual property from Energy Insights. Research reports elucidate business strategy, best practices, technology selection, and vendor assessment, along with short perspectives on topical issues. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("23faeea3-095f-4402-af66-a1b9b036662e"), new Guid("7ea3ac4c-54a2-4976-900b-46109a855bca"), null, null, null, new Guid("4f90452e-996a-46b5-8c2e-456c96d0fe4e"), true, false, false, null, "Worldwide Oil and Gas Downstream IT Blueprint", "WORLDWIDE OIL AND GAS DOWNSTREAM IT BLUEPRINT", "Energy Insights: Worldwide Oil and Gas Downstream IT Strategies advisory research service takes the worldwide oil and gas IT strategies research and applies it to the downstream operations in oil and gas. The research focuses on companies that are involved in the production for sale of all petroleum products, from incoming crude to outgoing product. It will also encompass the technology companies that provide the technology foundation for the operation of the downstream business. Energy Insights: Worldwide Oil and Gas Downstream IT Strategies will focus its analysis on the digital transformation (DX) use cases, priorities, innovation technology, and road map for all aspect of companies that produce and distribute petroleum products. It will provide technology vendors a road map for product development and messaging in oil and gas downstream. Throughout the year, this service will address the following topics: Digital transformation – Investment priorities and road maps for transformation of the downstream business operation Operations technology — IT/OT convergence investments that help downstream organizations transform how they operate the production lines Asset management – Technology changes driving asset management organization and transformation Transformation to scale – Development of a transformed infrastructure to scale to market volatility Our research addresses the following issues that are critical to your success: What is the impact of transformational technologies on the oil and gas downstream DX road map? How does an oil and gas downstream company organize itself around the Future of Work? What are the market trends in oil and gas downstream that affect the use case prioritization for DX? How should trading partners be integrated into the DX road map? Who are the technology vendors that can support DX in oil and gas downstream?" },
                    { new Guid("fc61b9b9-4278-4283-b5ce-78b349add12c"), new Guid("eaa27e0f-57e2-4742-86c6-a1d29bb4836a"), null, null, null, new Guid("834a162a-03e7-49a7-843f-5a0f8d1f03a1"), true, false, false, null, "Customer Experience Management Strategies", "CUSTOMER EXPERIENCE MANAGEMENT STRATEGIES", "Customer Experience Management Strategies SIS provides a framework and critical knowledge for understanding the changing nature of the customer experience (CX) and guides chief experience officers and their organizations as they master the digital transformation of the customer experience. This product covers the concepts of experience management and customer experience, looking at how digital transformation is driving change to customer expectations, preferences, and behavior and how enterprises must adopt new technologies to meet these urgent challenges. Markets and Subjects Analyzed: Experience management; Customer-centric engagement Customer-centric operations; Customer-centric solution design; Impact of 3rd Platform technologies on customer experience; Intelligence and analytics-driven customer experience; Customer experience along the customer journey; Customer experience benchmarks Core Research: Customer Experience Taxonomy; Digital Transformation of the Customer Experience; Redefining Experience Management; PeerScape: Customer Experience; Customer Experience in an Algorithm Economy; Technology-Enabled Storytelling; MaturityScape: Customer Experience; Customer Intelligence and Analytics; Innovation Accelerators in Customer Experience: Artificial Intelligence; In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("309e54ad-abee-4c35-885b-8cfb0c167a4a"), new Guid("600cd1da-f44b-4565-8af9-e71570fe3eeb"), null, null, null, new Guid("834a162a-03e7-49a7-843f-5a0f8d1f03a1"), true, false, false, null, "Retail Insights: European Retail Digital Transformation Strategies", "RETAIL INSIGHTS: EUROPEAN RETAIL DIGITAL TRANSFORMATION STRATEGIES", "European retailing is in a state of change since digital technologies have become so pervasive in the retail journey and consumer expectations, in terms of customer experience, with their preferred brands rapidly evolving. European retailers are adapting to these changes through the adoption of commerce everywhere business models and technology. Retailers from across Europe are in the process of determining how digital impacts them and what their digital transformation approach and strategy should be. This reveals a more and more complex and highly differentiated European region, where differences exist and persist across countries as well as subsegments (fashion and apparel, department stores, food and grocery, etc.). Retail Insights is witnessing among European retailers, at varying degrees of maturity, a race to digitize because of the potential new revenue streams and retail operational efficiencies that can be derived. Retail Insights: European Retail Digital Transformation Strategies advisory service examines the impact of digital transformation on the European retailers' business, technology, and organizational areas. Specific coverage is given to provide valuable insights into the European retail industry, with a specific focus on digital transformation strategies applied by retail companies to improve the omni-channel customer experience. This advisory service develops unique analysis and comprehensive data through Retail Insights' proprietary research projects, along with ongoing communications with industry experts, retail CIOs, and line-of-business executives, and ICT product and service vendors. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports. Our analysts are also available to provide personalized advice for retail executives and ICT vendors to help them make better-informed decisions." }
                });

            migrationBuilder.InsertData(
                table: "ReportTags",
                columns: new[] { "TagId", "ReportId" },
                values: new object[,]
                {
                    { new Guid("40b86e5e-e45a-486f-b453-45dc0d2cebfc"), new Guid("355a5520-1d12-41c5-9143-1321c2af841b") },
                    { new Guid("389cedc2-c792-4873-b472-ed551f3b9aad"), new Guid("81e03de3-bd60-4eb6-bfe9-2f0b6170d311") },
                    { new Guid("333f73a6-08d1-4e4d-9a1a-22269c998ba1"), new Guid("81e03de3-bd60-4eb6-bfe9-2f0b6170d311") },
                    { new Guid("1f84fff5-bd7a-412e-b3b6-a419d7b1eaff"), new Guid("39312241-4545-4860-97b4-edf26671b1e9") },
                    { new Guid("333f73a6-08d1-4e4d-9a1a-22269c998ba1"), new Guid("39312241-4545-4860-97b4-edf26671b1e9") },
                    { new Guid("5f25be49-108e-4a63-bff8-40155801017e"), new Guid("38e67af9-991e-4f71-9bdb-d95a011e0031") },
                    { new Guid("2ee72174-c156-4922-aa11-0eff2e380093"), new Guid("38e67af9-991e-4f71-9bdb-d95a011e0031") },
                    { new Guid("2ee72174-c156-4922-aa11-0eff2e380093"), new Guid("46f2cb6b-d2b2-4f32-899c-162270dec3c7") },
                    { new Guid("25602b6f-ed60-45dc-9e43-3a00327bb71f"), new Guid("46f2cb6b-d2b2-4f32-899c-162270dec3c7") },
                    { new Guid("ba91ddc9-4dcd-44c4-8cda-fdcd8f686675"), new Guid("798e41d2-e1a9-4930-aa94-1682e466a70f") },
                    { new Guid("25602b6f-ed60-45dc-9e43-3a00327bb71f"), new Guid("798e41d2-e1a9-4930-aa94-1682e466a70f") },
                    { new Guid("5f25be49-108e-4a63-bff8-40155801017e"), new Guid("de583aae-c56b-47af-97a4-f429db0d81d4") },
                    { new Guid("2ee72174-c156-4922-aa11-0eff2e380093"), new Guid("de583aae-c56b-47af-97a4-f429db0d81d4") },
                    { new Guid("25602b6f-ed60-45dc-9e43-3a00327bb71f"), new Guid("de583aae-c56b-47af-97a4-f429db0d81d4") },
                    { new Guid("c5289719-be84-4bc3-8698-c760d6db441c"), new Guid("6911bcf0-6152-4282-a842-cd5814a27cd3") },
                    { new Guid("cfbce58e-e6af-4354-aca9-d88e478ac314"), new Guid("c6c70893-ca68-43f4-b467-01f68b0641f0") },
                    { new Guid("5f25be49-108e-4a63-bff8-40155801017e"), new Guid("01d4afbe-6ab7-4a7b-ac17-36e8e6b198d4") },
                    { new Guid("ba91ddc9-4dcd-44c4-8cda-fdcd8f686675"), new Guid("01d4afbe-6ab7-4a7b-ac17-36e8e6b198d4") },
                    { new Guid("7bc6d929-4740-4cfe-8814-fd53431e92c0"), new Guid("bccee8f7-cee6-4ef8-8a41-c61a248f038f") },
                    { new Guid("b7a6689d-f714-4dea-b9b9-257b33dfcafc"), new Guid("bccee8f7-cee6-4ef8-8a41-c61a248f038f") },
                    { new Guid("cfbce58e-e6af-4354-aca9-d88e478ac314"), new Guid("d928b814-73d2-4393-ad87-3d030635c42e") },
                    { new Guid("384e72b3-a935-4bd4-a5c2-7ac7e3863454"), new Guid("d928b814-73d2-4393-ad87-3d030635c42e") },
                    { new Guid("7bc6d929-4740-4cfe-8814-fd53431e92c0"), new Guid("d928b814-73d2-4393-ad87-3d030635c42e") },
                    { new Guid("25602b6f-ed60-45dc-9e43-3a00327bb71f"), new Guid("75eebe6b-5b26-42b1-848e-b60d3095b483") },
                    { new Guid("384e72b3-a935-4bd4-a5c2-7ac7e3863454"), new Guid("75eebe6b-5b26-42b1-848e-b60d3095b483") },
                    { new Guid("384e72b3-a935-4bd4-a5c2-7ac7e3863454"), new Guid("68bb4739-51da-4e62-83c6-b5642930ee0f") },
                    { new Guid("b7a6689d-f714-4dea-b9b9-257b33dfcafc"), new Guid("68bb4739-51da-4e62-83c6-b5642930ee0f") },
                    { new Guid("384e72b3-a935-4bd4-a5c2-7ac7e3863454"), new Guid("fc61b9b9-4278-4283-b5ce-78b349add12c") },
                    { new Guid("7bc6d929-4740-4cfe-8814-fd53431e92c0"), new Guid("fc61b9b9-4278-4283-b5ce-78b349add12c") },
                    { new Guid("2ee72174-c156-4922-aa11-0eff2e380093"), new Guid("c6c70893-ca68-43f4-b467-01f68b0641f0") },
                    { new Guid("389cedc2-c792-4873-b472-ed551f3b9aad"), new Guid("6911bcf0-6152-4282-a842-cd5814a27cd3") },
                    { new Guid("c5289719-be84-4bc3-8698-c760d6db441c"), new Guid("05157698-c500-4605-991a-b555a3f16ccf") },
                    { new Guid("cfbce58e-e6af-4354-aca9-d88e478ac314"), new Guid("05157698-c500-4605-991a-b555a3f16ccf") },
                    { new Guid("aea7fa6b-9efd-485a-ba76-7a037b1086af"), new Guid("355a5520-1d12-41c5-9143-1321c2af841b") },
                    { new Guid("40b86e5e-e45a-486f-b453-45dc0d2cebfc"), new Guid("23faeea3-095f-4402-af66-a1b9b036662e") },
                    { new Guid("a64cff24-7e56-44b1-8b9e-40a5b070d26c"), new Guid("23faeea3-095f-4402-af66-a1b9b036662e") },
                    { new Guid("aea7fa6b-9efd-485a-ba76-7a037b1086af"), new Guid("3e6a559c-761c-444a-9cff-662b496fa01d") },
                    { new Guid("2767c5f8-c6d0-4767-adac-dd343abbf2e5"), new Guid("3e6a559c-761c-444a-9cff-662b496fa01d") },
                    { new Guid("cfbce58e-e6af-4354-aca9-d88e478ac314"), new Guid("da58dd14-40d6-44e4-b561-29f75f3efe4e") },
                    { new Guid("a64cff24-7e56-44b1-8b9e-40a5b070d26c"), new Guid("da58dd14-40d6-44e4-b561-29f75f3efe4e") },
                    { new Guid("a64cff24-7e56-44b1-8b9e-40a5b070d26c"), new Guid("829daa92-8582-4e90-a3e5-2cd5bee42561") },
                    { new Guid("2767c5f8-c6d0-4767-adac-dd343abbf2e5"), new Guid("829daa92-8582-4e90-a3e5-2cd5bee42561") },
                    { new Guid("cfbce58e-e6af-4354-aca9-d88e478ac314"), new Guid("007294a4-a7f5-4595-b0ae-f9d561a739ab") },
                    { new Guid("40b86e5e-e45a-486f-b453-45dc0d2cebfc"), new Guid("007294a4-a7f5-4595-b0ae-f9d561a739ab") },
                    { new Guid("aea7fa6b-9efd-485a-ba76-7a037b1086af"), new Guid("007294a4-a7f5-4595-b0ae-f9d561a739ab") },
                    { new Guid("54c18cc5-5f76-46a2-ae1a-fd3f1a024e8a"), new Guid("4b9e68b5-fe3f-479a-a7b8-a1663a2ce1ed") },
                    { new Guid("404a15c9-7429-4eb7-9db4-21b2491e51e8"), new Guid("4b9e68b5-fe3f-479a-a7b8-a1663a2ce1ed") },
                    { new Guid("f1280acc-4bcb-4b79-b4ff-a5f7500e8fc3"), new Guid("4b9e68b5-fe3f-479a-a7b8-a1663a2ce1ed") },
                    { new Guid("54c18cc5-5f76-46a2-ae1a-fd3f1a024e8a"), new Guid("ddd61f39-65ae-4e4a-b30a-1e16be57a388") },
                    { new Guid("fce9a92b-500e-4fb1-9592-bed874da3728"), new Guid("ddd61f39-65ae-4e4a-b30a-1e16be57a388") },
                    { new Guid("404a15c9-7429-4eb7-9db4-21b2491e51e8"), new Guid("1ad9cd50-7bdb-4189-a945-1377819579f7") },
                    { new Guid("f1280acc-4bcb-4b79-b4ff-a5f7500e8fc3"), new Guid("1ad9cd50-7bdb-4189-a945-1377819579f7") },
                    { new Guid("fce9a92b-500e-4fb1-9592-bed874da3728"), new Guid("ee80d6ae-6818-4d92-bad1-e2e8a4595817") },
                    { new Guid("f1280acc-4bcb-4b79-b4ff-a5f7500e8fc3"), new Guid("ee80d6ae-6818-4d92-bad1-e2e8a4595817") },
                    { new Guid("54c18cc5-5f76-46a2-ae1a-fd3f1a024e8a"), new Guid("473e2f86-911b-4d81-b9d0-847dc26a1ed2") },
                    { new Guid("f1280acc-4bcb-4b79-b4ff-a5f7500e8fc3"), new Guid("473e2f86-911b-4d81-b9d0-847dc26a1ed2") },
                    { new Guid("404a15c9-7429-4eb7-9db4-21b2491e51e8"), new Guid("8126ebac-1fb2-463f-8450-dff360719ac9") },
                    { new Guid("f1280acc-4bcb-4b79-b4ff-a5f7500e8fc3"), new Guid("8126ebac-1fb2-463f-8450-dff360719ac9") },
                    { new Guid("389cedc2-c792-4873-b472-ed551f3b9aad"), new Guid("140b844b-fb58-419c-a189-27298f026ad8") },
                    { new Guid("cfbce58e-e6af-4354-aca9-d88e478ac314"), new Guid("140b844b-fb58-419c-a189-27298f026ad8") },
                    { new Guid("cfbce58e-e6af-4354-aca9-d88e478ac314"), new Guid("b60fcff9-4680-4d7e-b2fd-8236cdc48137") },
                    { new Guid("1f84fff5-bd7a-412e-b3b6-a419d7b1eaff"), new Guid("b60fcff9-4680-4d7e-b2fd-8236cdc48137") },
                    { new Guid("25602b6f-ed60-45dc-9e43-3a00327bb71f"), new Guid("309e54ad-abee-4c35-885b-8cfb0c167a4a") },
                    { new Guid("b7a6689d-f714-4dea-b9b9-257b33dfcafc"), new Guid("309e54ad-abee-4c35-885b-8cfb0c167a4a") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("1f84fff5-bd7a-412e-b3b6-a419d7b1eaff"), new Guid("39312241-4545-4860-97b4-edf26671b1e9") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("1f84fff5-bd7a-412e-b3b6-a419d7b1eaff"), new Guid("b60fcff9-4680-4d7e-b2fd-8236cdc48137") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("25602b6f-ed60-45dc-9e43-3a00327bb71f"), new Guid("309e54ad-abee-4c35-885b-8cfb0c167a4a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("25602b6f-ed60-45dc-9e43-3a00327bb71f"), new Guid("46f2cb6b-d2b2-4f32-899c-162270dec3c7") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("25602b6f-ed60-45dc-9e43-3a00327bb71f"), new Guid("75eebe6b-5b26-42b1-848e-b60d3095b483") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("25602b6f-ed60-45dc-9e43-3a00327bb71f"), new Guid("798e41d2-e1a9-4930-aa94-1682e466a70f") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("25602b6f-ed60-45dc-9e43-3a00327bb71f"), new Guid("de583aae-c56b-47af-97a4-f429db0d81d4") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("2767c5f8-c6d0-4767-adac-dd343abbf2e5"), new Guid("3e6a559c-761c-444a-9cff-662b496fa01d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("2767c5f8-c6d0-4767-adac-dd343abbf2e5"), new Guid("829daa92-8582-4e90-a3e5-2cd5bee42561") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("2ee72174-c156-4922-aa11-0eff2e380093"), new Guid("38e67af9-991e-4f71-9bdb-d95a011e0031") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("2ee72174-c156-4922-aa11-0eff2e380093"), new Guid("46f2cb6b-d2b2-4f32-899c-162270dec3c7") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("2ee72174-c156-4922-aa11-0eff2e380093"), new Guid("c6c70893-ca68-43f4-b467-01f68b0641f0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("2ee72174-c156-4922-aa11-0eff2e380093"), new Guid("de583aae-c56b-47af-97a4-f429db0d81d4") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("333f73a6-08d1-4e4d-9a1a-22269c998ba1"), new Guid("39312241-4545-4860-97b4-edf26671b1e9") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("333f73a6-08d1-4e4d-9a1a-22269c998ba1"), new Guid("81e03de3-bd60-4eb6-bfe9-2f0b6170d311") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("384e72b3-a935-4bd4-a5c2-7ac7e3863454"), new Guid("68bb4739-51da-4e62-83c6-b5642930ee0f") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("384e72b3-a935-4bd4-a5c2-7ac7e3863454"), new Guid("75eebe6b-5b26-42b1-848e-b60d3095b483") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("384e72b3-a935-4bd4-a5c2-7ac7e3863454"), new Guid("d928b814-73d2-4393-ad87-3d030635c42e") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("384e72b3-a935-4bd4-a5c2-7ac7e3863454"), new Guid("fc61b9b9-4278-4283-b5ce-78b349add12c") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("389cedc2-c792-4873-b472-ed551f3b9aad"), new Guid("140b844b-fb58-419c-a189-27298f026ad8") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("389cedc2-c792-4873-b472-ed551f3b9aad"), new Guid("6911bcf0-6152-4282-a842-cd5814a27cd3") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("389cedc2-c792-4873-b472-ed551f3b9aad"), new Guid("81e03de3-bd60-4eb6-bfe9-2f0b6170d311") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("404a15c9-7429-4eb7-9db4-21b2491e51e8"), new Guid("1ad9cd50-7bdb-4189-a945-1377819579f7") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("404a15c9-7429-4eb7-9db4-21b2491e51e8"), new Guid("4b9e68b5-fe3f-479a-a7b8-a1663a2ce1ed") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("404a15c9-7429-4eb7-9db4-21b2491e51e8"), new Guid("8126ebac-1fb2-463f-8450-dff360719ac9") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("40b86e5e-e45a-486f-b453-45dc0d2cebfc"), new Guid("007294a4-a7f5-4595-b0ae-f9d561a739ab") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("40b86e5e-e45a-486f-b453-45dc0d2cebfc"), new Guid("23faeea3-095f-4402-af66-a1b9b036662e") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("40b86e5e-e45a-486f-b453-45dc0d2cebfc"), new Guid("355a5520-1d12-41c5-9143-1321c2af841b") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("54c18cc5-5f76-46a2-ae1a-fd3f1a024e8a"), new Guid("473e2f86-911b-4d81-b9d0-847dc26a1ed2") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("54c18cc5-5f76-46a2-ae1a-fd3f1a024e8a"), new Guid("4b9e68b5-fe3f-479a-a7b8-a1663a2ce1ed") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("54c18cc5-5f76-46a2-ae1a-fd3f1a024e8a"), new Guid("ddd61f39-65ae-4e4a-b30a-1e16be57a388") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5f25be49-108e-4a63-bff8-40155801017e"), new Guid("01d4afbe-6ab7-4a7b-ac17-36e8e6b198d4") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5f25be49-108e-4a63-bff8-40155801017e"), new Guid("38e67af9-991e-4f71-9bdb-d95a011e0031") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5f25be49-108e-4a63-bff8-40155801017e"), new Guid("de583aae-c56b-47af-97a4-f429db0d81d4") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("7bc6d929-4740-4cfe-8814-fd53431e92c0"), new Guid("bccee8f7-cee6-4ef8-8a41-c61a248f038f") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("7bc6d929-4740-4cfe-8814-fd53431e92c0"), new Guid("d928b814-73d2-4393-ad87-3d030635c42e") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("7bc6d929-4740-4cfe-8814-fd53431e92c0"), new Guid("fc61b9b9-4278-4283-b5ce-78b349add12c") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a64cff24-7e56-44b1-8b9e-40a5b070d26c"), new Guid("23faeea3-095f-4402-af66-a1b9b036662e") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a64cff24-7e56-44b1-8b9e-40a5b070d26c"), new Guid("829daa92-8582-4e90-a3e5-2cd5bee42561") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a64cff24-7e56-44b1-8b9e-40a5b070d26c"), new Guid("da58dd14-40d6-44e4-b561-29f75f3efe4e") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("aea7fa6b-9efd-485a-ba76-7a037b1086af"), new Guid("007294a4-a7f5-4595-b0ae-f9d561a739ab") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("aea7fa6b-9efd-485a-ba76-7a037b1086af"), new Guid("355a5520-1d12-41c5-9143-1321c2af841b") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("aea7fa6b-9efd-485a-ba76-7a037b1086af"), new Guid("3e6a559c-761c-444a-9cff-662b496fa01d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b7a6689d-f714-4dea-b9b9-257b33dfcafc"), new Guid("309e54ad-abee-4c35-885b-8cfb0c167a4a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b7a6689d-f714-4dea-b9b9-257b33dfcafc"), new Guid("68bb4739-51da-4e62-83c6-b5642930ee0f") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b7a6689d-f714-4dea-b9b9-257b33dfcafc"), new Guid("bccee8f7-cee6-4ef8-8a41-c61a248f038f") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("ba91ddc9-4dcd-44c4-8cda-fdcd8f686675"), new Guid("01d4afbe-6ab7-4a7b-ac17-36e8e6b198d4") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("ba91ddc9-4dcd-44c4-8cda-fdcd8f686675"), new Guid("798e41d2-e1a9-4930-aa94-1682e466a70f") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("c5289719-be84-4bc3-8698-c760d6db441c"), new Guid("05157698-c500-4605-991a-b555a3f16ccf") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("c5289719-be84-4bc3-8698-c760d6db441c"), new Guid("6911bcf0-6152-4282-a842-cd5814a27cd3") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("cfbce58e-e6af-4354-aca9-d88e478ac314"), new Guid("007294a4-a7f5-4595-b0ae-f9d561a739ab") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("cfbce58e-e6af-4354-aca9-d88e478ac314"), new Guid("05157698-c500-4605-991a-b555a3f16ccf") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("cfbce58e-e6af-4354-aca9-d88e478ac314"), new Guid("140b844b-fb58-419c-a189-27298f026ad8") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("cfbce58e-e6af-4354-aca9-d88e478ac314"), new Guid("b60fcff9-4680-4d7e-b2fd-8236cdc48137") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("cfbce58e-e6af-4354-aca9-d88e478ac314"), new Guid("c6c70893-ca68-43f4-b467-01f68b0641f0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("cfbce58e-e6af-4354-aca9-d88e478ac314"), new Guid("d928b814-73d2-4393-ad87-3d030635c42e") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("cfbce58e-e6af-4354-aca9-d88e478ac314"), new Guid("da58dd14-40d6-44e4-b561-29f75f3efe4e") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f1280acc-4bcb-4b79-b4ff-a5f7500e8fc3"), new Guid("1ad9cd50-7bdb-4189-a945-1377819579f7") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f1280acc-4bcb-4b79-b4ff-a5f7500e8fc3"), new Guid("473e2f86-911b-4d81-b9d0-847dc26a1ed2") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f1280acc-4bcb-4b79-b4ff-a5f7500e8fc3"), new Guid("4b9e68b5-fe3f-479a-a7b8-a1663a2ce1ed") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f1280acc-4bcb-4b79-b4ff-a5f7500e8fc3"), new Guid("8126ebac-1fb2-463f-8450-dff360719ac9") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("f1280acc-4bcb-4b79-b4ff-a5f7500e8fc3"), new Guid("ee80d6ae-6818-4d92-bad1-e2e8a4595817") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("fce9a92b-500e-4fb1-9592-bed874da3728"), new Guid("ddd61f39-65ae-4e4a-b30a-1e16be57a388") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("fce9a92b-500e-4fb1-9592-bed874da3728"), new Guid("ee80d6ae-6818-4d92-bad1-e2e8a4595817") });

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("007294a4-a7f5-4595-b0ae-f9d561a739ab"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("01d4afbe-6ab7-4a7b-ac17-36e8e6b198d4"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("05157698-c500-4605-991a-b555a3f16ccf"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("140b844b-fb58-419c-a189-27298f026ad8"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("1ad9cd50-7bdb-4189-a945-1377819579f7"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("23faeea3-095f-4402-af66-a1b9b036662e"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("309e54ad-abee-4c35-885b-8cfb0c167a4a"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("355a5520-1d12-41c5-9143-1321c2af841b"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("38e67af9-991e-4f71-9bdb-d95a011e0031"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("39312241-4545-4860-97b4-edf26671b1e9"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3e6a559c-761c-444a-9cff-662b496fa01d"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("46f2cb6b-d2b2-4f32-899c-162270dec3c7"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("473e2f86-911b-4d81-b9d0-847dc26a1ed2"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("4b9e68b5-fe3f-479a-a7b8-a1663a2ce1ed"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("68bb4739-51da-4e62-83c6-b5642930ee0f"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("6911bcf0-6152-4282-a842-cd5814a27cd3"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("75eebe6b-5b26-42b1-848e-b60d3095b483"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("798e41d2-e1a9-4930-aa94-1682e466a70f"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("8126ebac-1fb2-463f-8450-dff360719ac9"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("81e03de3-bd60-4eb6-bfe9-2f0b6170d311"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("829daa92-8582-4e90-a3e5-2cd5bee42561"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("b60fcff9-4680-4d7e-b2fd-8236cdc48137"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("bccee8f7-cee6-4ef8-8a41-c61a248f038f"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("c6c70893-ca68-43f4-b467-01f68b0641f0"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("d928b814-73d2-4393-ad87-3d030635c42e"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("da58dd14-40d6-44e4-b561-29f75f3efe4e"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("ddd61f39-65ae-4e4a-b30a-1e16be57a388"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("de583aae-c56b-47af-97a4-f429db0d81d4"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("ee80d6ae-6818-4d92-bad1-e2e8a4595817"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("fc61b9b9-4278-4283-b5ce-78b349add12c"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("1f84fff5-bd7a-412e-b3b6-a419d7b1eaff"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("25602b6f-ed60-45dc-9e43-3a00327bb71f"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("2767c5f8-c6d0-4767-adac-dd343abbf2e5"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("2ee72174-c156-4922-aa11-0eff2e380093"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("333f73a6-08d1-4e4d-9a1a-22269c998ba1"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("384e72b3-a935-4bd4-a5c2-7ac7e3863454"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("389cedc2-c792-4873-b472-ed551f3b9aad"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("404a15c9-7429-4eb7-9db4-21b2491e51e8"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("40b86e5e-e45a-486f-b453-45dc0d2cebfc"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("54c18cc5-5f76-46a2-ae1a-fd3f1a024e8a"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("5f25be49-108e-4a63-bff8-40155801017e"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("7bc6d929-4740-4cfe-8814-fd53431e92c0"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("a64cff24-7e56-44b1-8b9e-40a5b070d26c"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("aea7fa6b-9efd-485a-ba76-7a037b1086af"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("b7a6689d-f714-4dea-b9b9-257b33dfcafc"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("ba91ddc9-4dcd-44c4-8cda-fdcd8f686675"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("c5289719-be84-4bc3-8698-c760d6db441c"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("cfbce58e-e6af-4354-aca9-d88e478ac314"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("f1280acc-4bcb-4b79-b4ff-a5f7500e8fc3"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("fce9a92b-500e-4fb1-9592-bed874da3728"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("600cd1da-f44b-4565-8af9-e71570fe3eeb"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("61671f8b-807e-4456-be06-023a370300ce"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("7ea3ac4c-54a2-4976-900b-46109a855bca"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("87328894-95df-4b0c-a06c-4e94af259d53"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("eaa27e0f-57e2-4742-86c6-a1d29bb4836a"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("3b514467-a845-44fe-b377-8ddd06b2441e"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("4f90452e-996a-46b5-8c2e-456c96d0fe4e"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("620e6d15-4136-41c6-a557-639b1cd7b2ba"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("754dcfe5-a1d9-405b-aafd-2a3487b4567d"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("834a162a-03e7-49a7-843f-5a0f8d1f03a1"));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("297d06e6-c058-486f-a18a-06a971ebfcd7"),
                column: "ConcurrencyStamp",
                value: "582d7ba7-9fa0-47f5-b65a-567ca4b6036c");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("6c8fcd7e-62f6-4f3e-a73d-acbfd60b97ab"),
                column: "ConcurrencyStamp",
                value: "921c2259-0f9d-4325-9413-6863767443b4");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7f66990c-36bc-4381-9f81-32e06e168319"),
                column: "ConcurrencyStamp",
                value: "7da9b257-eeaa-4d59-a73f-a82037c90b6e");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb93"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "ce829973-e25b-47bb-9da8-99566094999e", new DateTime(2020, 6, 1, 9, 1, 52, 138, DateTimeKind.Utc).AddTicks(4641), "AQAAAAEAACcQAAAAEGoCjIPXhachBZ4govIejjF5V4mmNfi6rDgfYDsBDp2FXBEkSbc7rcYLw+6IirD4Mg==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb94"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "7d7f9910-4e5d-48a9-b3b6-f16c22b40df0", new DateTime(2020, 6, 1, 9, 1, 52, 138, DateTimeKind.Utc).AddTicks(6805), "AQAAAAEAACcQAAAAEM/MIvodzFDv9m8il35pZ4JI69DRAwofMh0eLJ4QWMaKa/EUu3KkCEewe1ar551mvA==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb95"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "c4dc62aa-4a32-4c59-8e5a-a5a3933baf4d", new DateTime(2020, 6, 1, 9, 1, 52, 138, DateTimeKind.Utc).AddTicks(6829), "AQAAAAEAACcQAAAAENt1VJK0rqtdXOkT4iUsBoeJfC9S5q5i3YunAzUwagPaikq5X96cGPRPyexHhJD+og==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb96"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "f78a43a4-2168-4a2d-9b59-6213cf64474d", new DateTime(2020, 6, 1, 9, 1, 52, 138, DateTimeKind.Utc).AddTicks(6837), "AQAAAAEAACcQAAAAEHXcNOqrZYi7P4vx256ITmDHNH0msbvvuq6emz6ZGG5oyASnivgHw4tesgqefJZ6GQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb97"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "fcd051fd-824c-4760-9c2f-ee03c69ec5ae", new DateTime(2020, 6, 1, 9, 1, 52, 138, DateTimeKind.Utc).AddTicks(6843), "AQAAAAEAACcQAAAAECphryw0x5YygZkqYqLS1aNztjhdel6rqd4oVJRpEVo/N05bosZ3Mp8Q8XLy4Cs3Uw==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb98"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "e8796126-ff73-4791-9cb8-2e9922295775", new DateTime(2020, 6, 1, 9, 1, 52, 138, DateTimeKind.Utc).AddTicks(6859), "AQAAAAEAACcQAAAAED6aq38MXYmUMW4ePAy82WX1BpJciNax4DEM9Ujc9G9Q7OAXPHJGx+Ug2Ube3z/9DQ==" });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "Bio", "ImagePath", "UserId" },
                values: new object[,]
                {
                    { new Guid("afa94f84-fd29-466c-af20-9a3dc59046e6"), "Jim Doe has been involved with ICT research in Africa since 1997 and has participated in diverse research projects in 14 African countries. He started his career in 1994 as a public relations officer with Software Technologies Limited (an East African Oracle distributor), before serving as a research analyst/project officer for Telecom Forum Africa in 1997.", "3.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb94") },
                    { new Guid("e7492b0c-6e9d-471e-91bb-1f274af037ca"), "Rosa James has a background in journalism and has been published in various local, regional, and international magazines and newspapers. She has spoken at various industry events across sub-Saharan Africa including countries like Ethiopia, Ghana, Kenya, Nigeria, Rwanda, Tanzania and Zambia as well as leading and facilitating at IDC’s events in the region. Rose is also a published author of a children’s novel.", "1.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb95") },
                    { new Guid("941d0105-2b53-4f34-a1ed-b524f9ace923"), "Cate Williams has been an analyst InsightHub for several years. Early on as an analyst she covered CRM applications, but more recently her work has been in integration middleware covering markets such as API management, file sync and share, and B2B integration.", "2.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb96") },
                    { new Guid("84b25c90-772b-4eb9-a6b6-c80b22217fc7"), "Max Upton has more than 15 years of experience in business research, focusing on financial services and business innovation. His consulting work for InsightHub has allowed him to work closely with leading banks and regulators in the region for their technology and innovation strategies. Mr. Upton is concurrently head of InsightHub's business and operations in Thailand.", "7.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb97") },
                    { new Guid("bf262f56-2f19-47a8-8c0b-cefbbd055d2f"), "Previously, David Scott served for ten years as Enterprise Architect at CareFirst Blue Cross/Blue Shield, responsible for building business and technology alignment roadmaps for the ACA/HIX Health Reform, Mandated Implementations, PCMH, and Provider Network domains to ensure alignment of annual and tactical IT project planning with business goals.", "8.jpg", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb98") }
                });

            migrationBuilder.InsertData(
                table: "Industries",
                columns: new[] { "Id", "DeletedOn", "ImagePath", "IsDeleted", "ModifiedOn", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("2e707290-83df-4b8e-8419-7c425bc444be"), null, null, false, null, "Energy", "ENERGY" },
                    { new Guid("2042dbce-edbb-4923-914f-e2d485cedb87"), null, null, false, null, "Information Technology", "INFORMATION TECHNOLOGY" },
                    { new Guid("d0132dbf-3235-429e-846a-3eec0d72a88a"), null, null, false, null, "Healthcare", "HEALTHCARE" },
                    { new Guid("80a66b63-61f3-4968-afa2-1eaf50dd2b7d"), null, null, false, null, "Finance", "FINANCE" },
                    { new Guid("c042988d-f630-4e8e-bd3f-e3ccb3e39158"), null, null, false, null, "Marketing", "MARKETING" }
                });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "Id", "DeletedOn", "IsDeleted", "ModifiedOn", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("b70c6f3b-4b1a-47e1-98de-24c133a33f62"), null, false, null, "computers", "COMPUTERS" },
                    { new Guid("52ae14e3-33bc-4d5c-a9bc-09c0afb88e00"), null, false, null, "hospital", "HOSPITAL" },
                    { new Guid("d38ab07a-c1c8-4ead-a8fc-6aa64767866c"), null, false, null, "influence", "INFLUENCE" },
                    { new Guid("4454473e-9573-4454-900e-6415ffbc6fb8"), null, false, null, "trend", "TREND" },
                    { new Guid("14f125e6-0aba-4f86-9c58-f725026e945e"), null, false, null, "sales", "SALES" },
                    { new Guid("60f2f2cf-8052-47ab-ad8a-948a4b60a1d8"), null, false, null, "profit", "PROFIT" },
                    { new Guid("18113347-addf-4b30-a8c3-5dda7ef744a9"), null, false, null, "insurance", "INSURANCE" },
                    { new Guid("ddfb2078-91ef-469e-a019-ae8a5a6b95a5"), null, false, null, "stock", "STOCK" },
                    { new Guid("b0987c84-5552-4b87-ae09-0400a1e86b4d"), null, false, null, "nuclear", "NUCLEAR" },
                    { new Guid("946c72f1-2bfc-423b-a43b-92ca35669b85"), null, false, null, "solar", "SOLAR" },
                    { new Guid("2bf7759f-8e07-4415-b620-182f259237c6"), null, false, null, "oil", "OIL" },
                    { new Guid("b6d5154e-4db9-4b1d-b33c-16f766fafc2f"), null, false, null, "laboratory", "LABORATORY" },
                    { new Guid("4f9e14aa-3fb5-4235-9066-d9ab835a891a"), null, false, null, "vaccine", "VACCINE" },
                    { new Guid("f7f4c29f-e67f-4a5a-ade8-5e65f72b005a"), null, false, null, "research", "RESEARCH" },
                    { new Guid("26c4bcfc-935a-4a3e-bc39-e995aa117a4a"), null, false, null, "medicine", "MEDICINE" },
                    { new Guid("fcb8d5a3-818c-4f3f-9241-3907d826794e"), null, false, null, "web", "WEB" },
                    { new Guid("87e2c0e3-e7b3-4659-9725-bd48052fe187"), null, false, null, "hardware", "HARDWARE" },
                    { new Guid("256d5428-6baa-408a-98b7-80abffb6d05f"), null, false, null, "software", "SOFTWARE" },
                    { new Guid("2ef11211-e751-4d9f-885e-8071bdd22e48"), null, false, null, "investment", "INVESTMENT" },
                    { new Guid("f1b152e3-7ba8-4c85-8d0b-03f7266a00b4"), null, false, null, "gas", "GAS" }
                });

            migrationBuilder.InsertData(
                table: "Reports",
                columns: new[] { "Id", "AuthorId", "Content", "DeletedOn", "ImagePath", "IndustryId", "IsApproved", "IsDeleted", "IsFeatured", "ModifiedOn", "Name", "NormalizedName", "Summary" },
                values: new object[,]
                {
                    { new Guid("41d7ceee-fc64-4227-b531-2f6946ad3431"), new Guid("941d0105-2b53-4f34-a1ed-b524f9ace923"), null, null, null, new Guid("2e707290-83df-4b8e-8419-7c425bc444be"), true, false, false, null, "North America Utilities Digital Transformation Tactics", "ENERGY INSIGHTS: NORTH AMERICA UTILITIES DIGITAL TRANSFORMATION TACTICS", "Digital transformation is driving utilities to change their business and operating and information models. The Energy Insights: North America Utilities Digital Transformation Strategies service is designed to help North American utility IT and business management understand the disruptions that are transforming the energy and utility value chains and develop the strategies and programs to capitalize on the evolving opportunities. The service provides deep research and insight on the key topics that matter to North American utility decision makers and IT executives affected by the digital transformation. Through an integrated set of deliverables, subscribers gain access to analysis and insights on the impact of new technologies, IT strategies and best practices, and the industry, governmental, and regulatory forces that are shaping the evolution to new industry business models in the industry's generation, transmission and distribution/pipelines, and retail sectors. This service delivers research, insight, and guidance on IT strategy and investments to meet the utility's most critical corporate objectives." },
                    { new Guid("5133194c-2c5f-415f-afae-04ce148762a4"), new Guid("bf262f56-2f19-47a8-8c0b-cefbbd055d2f"), null, null, null, new Guid("c042988d-f630-4e8e-bd3f-e3ccb3e39158"), true, false, false, null, "Marketing and Sales Solutions", "MARKETING AND SALES SOLUTIONS", "Marketing and sales technologies are driving forces for all companies as customers move to online, mobile-first, and collaborative relationship models. In reaching and selling to customers, it is essential that organizations aggressively develop the necessary operational and analytic skills, collaborative cultures, and creative problem solving needed to truly add value to the customer relationship. 's Marketing and Sales Solutions service provides strategic frameworks for thinking about the individual and aligned areas of marketing and sales technology as parts of a holistic business strategy. This program delivers insight, information, and data on the main drivers for the adoption of these technologies in the broader context of customer experience (CX) and networked business strategies.Markets and Subjects Analyzed. Marketing automation, campaign management, and go to market execution applications. Sales force automation applications. Predictive analytics and business KPIs. Mobile and digital applications and strategies. Customer data and analytics. CX strategies. Core Research. Reports on how 670 U.S. large enterprises use more than 300 vendors in 15 martech categories across 5 vertical markets:Consumer banking; Retail; CPG manufacturing; Securities and investment services; Travel and hospitality; MarketScape(s) on related solutions areas such as marketing clouds and artificial intelligence; TechScapes and PlanScapes on various topics such as GDPR, account-based marketing, personalization, and mobile marketing; Marketing software forecast and vendor shares; Sales force automation forecast and vendor shares. In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment. " },
                    { new Guid("99ac8caf-4841-4efc-90a6-5d3f58587f43"), new Guid("84b25c90-772b-4eb9-a6b6-c80b22217fc7"), null, null, null, new Guid("c042988d-f630-4e8e-bd3f-e3ccb3e39158"), true, false, false, null, "CMO Advisory Service", "CMO ADVISORY SERVICE", "CMO Advisory Service guides marketing leaders as they master the science of marketing. Digital transformation offers CMOs the opportunity to become revenue drivers and architects of the customer experience. Leaders leverage 's deep industry knowledge, powerful quantitative models, peer-tested practices, and personalized guidance to advance their operations. Whether seeking fresh perspectives on core challenges or counsel on emerging developments, offers a trusted source of insight.Markets and Subjects Analyzed. Marketing Investment and Transformational Operations. Planning and budgeting investments in the marketing mix, marketing technology, and marketing operations, accountability, and attribution. Application of New Technology for Marketing. Guidance on the implications of new technologies such as artificial intelligence (AI), collaboration, and marketing automation solutions. Delivering the New Customer Experience. Mapping and responding to the B2B customer decision journey. The Future Marketing Organization. Organizational design, staff allocation benchmarks, role definition, talent development, shared services, organizational alignment, and change management. Developing core competencies: Content marketing, customer intelligence and analytics, integrated digital and social engagement, sales enablement, and loyalty and advocacy" },
                    { new Guid("7ceb649a-9fc2-409f-afe8-8d747c3a80ba"), new Guid("bf262f56-2f19-47a8-8c0b-cefbbd055d2f"), null, null, null, new Guid("c042988d-f630-4e8e-bd3f-e3ccb3e39158"), true, false, false, null, "Digital Commerce Trends", "DIGITAL COMMERCE TRENDS", "The Digital Commerce subscription marketing intelligence service examines the competitive landscape, key trends, and differentiating factors of digital commerce, PIM, and CPQ application vendors; digital marketplaces; and business commerce networks. This includes the buying behavior of end users while purchasing products and services on digital commerce platforms. The service provides a worldwide perspective by looking at all forms of digital commerce transactions — including B2B, B2C, C2C, G2B, and B2B2C — across all vertical industries and regional markets. Markets and Subjects Analyzed. Digital commerce applications targeting businesses of all sizes and industries. Best-of-breed digital commerce applications in areas such as CPQ, payments and billing, order management, web content management, merchandising, site search, fulfillment, product information management, and inventory management. Business commerce networks that bring together functional applications for enterprise asset management, procurement, financials, sales, and human capital management.Enterprise partnership/integration strategies among digital commerce and marketing/content management vendors. Experience management — across channels (includes content management, commerce platforms, integration, and advanced analytics) for B2B, B2C, and B2B2C. Impact of cloud, social, mobile, and Big Data technologies on vendor strategies for employee, customer, supplier, partner, and asset engagement. Proliferation of public cloud microservice architectures. Mobile commerce (mobile app marketplaces, built-for-mobile commerce capabilities, and differentiating technology). Cognitive technologies and intelligent workflow impact on digital commerce applications" },
                    { new Guid("8440a849-2935-4c53-b6e1-77db5bf134ae"), new Guid("84b25c90-772b-4eb9-a6b6-c80b22217fc7"), null, null, null, new Guid("c042988d-f630-4e8e-bd3f-e3ccb3e39158"), true, false, false, null, "Canadian Sales Accelerator: Datacenter Infrastructure", "CANADIAN SALES ACCELERATOR: DATACENTER INFRASTRUCTURE", "Canadian Sales Accelerator: Datacenter Infrastructure research program analyzes the Canadian datacenter (DC) infrastructure market in Canada. Designed to provide intelligence and strategic frameworks to technology sales professionals, field marketing teams, and channel managers to take action on key responsibilities related to the sales cycle, this service provides market sizing, vendor performance, forecasts, and market opportunities, as well as a variety of adjacent markets and segmentations from a quantitative standpoint. This advisory service will allow subscribers to identify opportunities tied to the future of infrastructure, tailor go-to-market strategies as well as product and services road maps to meet the growing demand of end users for infrastructure solutions. At the same time, it looks at vendors, partners, and customers from a qualitative standpoint, concerning needs and requirements, pain points and buying intentions, maturity levels, and adoption in and of new technologies. Markets and Subjects Analyzed Datacenter technologies (including software defined); server and storage; converged systems. Infrastructure ecosystem including partners and channels. Datacenter budget trends and dynamics and vendor selection/buying criteria. Technology investment expectations for legacy and next-gen DC infrastructure. In addition to the core research documents, clients will receive briefings and concise sales executive email alerts throughout the year. Every client will have a Sales Accelerator service launch integration meeting to kick-off the program. Core Research. Brand Perceptions on Enterprise Storage and Service Vendors. Market Forecasts. Vendor Dashboard: Market Shares. Infrastructure Ecosystem Barometer. Executive Market Insights" },
                    { new Guid("6f308b37-d97a-478f-88c0-3256c8b123c5"), new Guid("afa94f84-fd29-466c-af20-9a3dc59046e6"), null, null, null, new Guid("80a66b63-61f3-4968-afa2-1eaf50dd2b7d"), true, false, false, null, "International Corporate Banking Digital Transformation", "INTERNATIONAL CORPORATE BANKING DIGITAL TRANSFORMATION", "With growing competition from nonbank platforms and the commoditization of products, the ability to onboard, connect, and deliver services to corporate customers is becoming paramount. With PSD2 in Europe democratizing data access and payment services, corporate banks have to transform their value proposition to deliver new data-driven services to their clients and add value, rather than their traditional, commoditized product proposition. Reducing the connectivity cost, increasing speed of data delivery toward real time, and integrating bank systems with corporate enterprise resource planning (ERP), treasury management systems (TSM), payment factories, and in-house banks will be paramount to help corporate clients manage capital, monitor cash flows, and optimize their liquidity. These infrastructure upgrades will also be crucial to exploit the evolving ecosystems brought about by distributed ledger technology (DLT) and the internet of things (IoT), where data discovery, analysis, and sharing will accelerate trade and reduce risk. Sensors attached to goods in transit — from the manufacturing plant to the retail outlet — could offer opportunities to banks' cash management and trade services businesses, better matching flows of payments and goods between seller and buyer. The Financial Insights: Worldwide Corporate Banking Digital Transformation Strategies advisory service provides clients with insightful information and analysis of corporate and commercial banking trends, including cash and treasury, trade finance, commercial lending, and payments. It also provides coverage of how the impact of emerging 3rd Platform technologies like big data, analytics, cloud, mobility, IoT, blockchain, and cognitive technologies transforms the sector and how their convergence unlocks new business and operating models. The service offers timely, strategic, and actionable business IT and application advice to guide our clients as they undertake digital investment decisions, plan for future technology needs, and benchmark themselves against competitors. It provides ongoing research reports, analyst access, and admission to content-rich conferences and webcasts for the benefit of financial institutions as well as technology vendors." },
                    { new Guid("cc7e2d5e-8fbe-44b7-b46d-f7c953f36453"), new Guid("941d0105-2b53-4f34-a1ed-b524f9ace923"), null, null, null, new Guid("80a66b63-61f3-4968-afa2-1eaf50dd2b7d"), true, false, false, null, "Insurance Digital Transformation Approach", "INSURANCE DIGITAL TRANSFORMATION APPROACH", "Customer experience is high on the agenda for most insurers and intermediaries as customers expect true value for the premiums they pay, above and beyond the traditional products and services delivered to them. To continue to be relevant in a changing marketplace, insurance organizations across the globe need to deliver contextual and value-centric insurance that is rooted on the principles of proactive risk management and secure, transparent, seamless, and contextual engagements across the customer journey. To achieve this, the industry needs to accelerate its effort to transform from a traditional, product-centric mindset to a customer-centric mindset enabled by digital technologies and the power of data and analytics. Insurance organizations need to increasingly play the role of true risk advisors rather than mere product sellers. As they progress in their transformation journey, they need advice and guidance to understand the opportunities and possibilities with the new technologies and to build and deploy digital capabilities while also tackling existing barriers to change. The Financial Insights: Worldwide Insurance Digital Transformation Strategies advisory service provides clients with insightful information and analysis of global insurance trends. It also provides coverage of how digital technologies like Big Data, analytics, cloud, mobility, IoT, blockchain, and cognitive technologies impact the life and annuity, accident and health, and property and casualty insurance markets. The service offers timely, strategic, and actionable business IT and application advice to guide our clients as they undertake digital investment decisions, plan for future technology needs, and benchmark themselves against competitors. It provides ongoing research reports, analyst access, and admission to content-rich conferences and webcasts for the benefit of insurance organizations as well as technology vendors." },
                    { new Guid("24ba7d9f-ef1a-4bdb-b966-44b09c01c244"), new Guid("bf262f56-2f19-47a8-8c0b-cefbbd055d2f"), null, null, null, new Guid("80a66b63-61f3-4968-afa2-1eaf50dd2b7d"), true, false, false, null, "Worldwide Banking IT Spending Guide", "WORLDWIDE BANKING IT SPENDING GUIDE", "The Financial Insights: Worldwide Banking IT Spending Guide examines the banking industry opportunity from a technology, functional process, and geography perspective. This comprehensive database delivered via 's Customer Insights query tool allows the user to easily extract meaningful information about the banking technology market by viewing data trends and relationships and making data comparisons. Markets Covered This product covers the following segments of the banking market: 9 regions: USA, Canada, Japan, Western Europe, Central and Eastern Europe, Middle East and Africa, Latin America, PRC, and Asia/Pacific 4 technologies: Hardware, software, services, and internal IT spend 5 banking segments: Consumer banking, corporate and institutional banking, corporate administration, enterprise utilities, and shared services 30+ functional processes: Channels, payments, core processing, and more 4 company size tiers: Institution size by tier 1 through tier 4 2 institution types: Banks and credit unions 6 years of data Data Deliverables This spending guide is delivered on a semiannual basis via a web-based interface for online querying and downloads. For a complete delivery schedule, please contact an sales representative. The following are the deliverables for this spending guide: Annual five-year forecasts by regions, technologies, banking segments, functional processes, tiers, and institution types; delivered twice a year About This Spending Guide Financial Insights: Worldwide Banking IT Spending Guide provides guidance on the expected technology opportunity around this market at a regional and total worldwide level. Segmented by functional process, institution type, company size tier, region, and technology component, this guide provides IT vendors with insights into both large and rapidly growing segments of the banking technology market and how the market will develop over the coming years." },
                    { new Guid("9c6ad87d-b16a-4773-8e9f-af71088d848d"), new Guid("e7492b0c-6e9d-471e-91bb-1f274af037ca"), null, null, "", new Guid("80a66b63-61f3-4968-afa2-1eaf50dd2b7d"), true, false, false, null, "Universal Payment Strategies", "UNIVERSAL PAYMENT STRATEGIES", "The payment industry has seen drastic changes in the past decade. New technologies, entrants, and business models have forced incumbent vendors and their financial institution customers to rethink how they move money. Stakeholders across the payment value chain — card-issuing banks, merchant acquirers, payment networks, and payment processors — face increasingly complex decisions. In this turbulent market, the players need more than facts and figures; they need critical analysis and insightful opinions. Markets and Subjects Analyzed Throughout the year, this service will address the following topics: Developing trends in payments such as omni-channel and alternative payment networks Evaluation and integration of new payment channels like voice commerce and IoT Enterprise risk, compliance, and fraud issues affecting payment products Legal and regulatory issues around the world that will affect how payments develop Middle- and back-office technologies that will affect the payment strategies of financial institutions Emerging technologies such as blockchain, AI, and next-generation security and their potential for altering the payment landscape Core Research MarketScape: Gateways for Integrated Payments B2B Payments: Digital Transformation in Transactions Retail Revolution: Beyond Card Payments Blockchain Payments: Short-Term Realities Persistent Payments: Automated Transactions in a Connected World Real-Time Payment Productization: Overlays in Action Retail Fraud: Protecting a Multi-Payment Environment" },
                    { new Guid("70123bf4-a702-402c-9ff3-cb2ea8eba45d"), new Guid("bf262f56-2f19-47a8-8c0b-cefbbd055d2f"), null, null, null, new Guid("80a66b63-61f3-4968-afa2-1eaf50dd2b7d"), true, false, false, null, "Asia/Pacific Financial Services IT", "ASIA/PACIFIC FINANCIAL SERVICES IT", "Financial Insights: Asia/Pacific Banking Customer Centricity program provides insights into the evolving needs of Asia/Pacific retail/consumer banking retail customers and guidelines on how banks are to respond to these trends. The program will give advice to technology buyers on the technology that supports the customer centricity agenda of the financial institution – particularly in the areas of customer relationship management (CRM), omni-experience and omni-channel solutions, and in loyalty management. The service will be backed by a comprehensive consumer survey on the preferences of Asia/Pacific retail/consumer banking customers in various aspects of customer experience. Financial Insights will then undertake on how financial institutions are to develop an effective customer management agenda, delving into the concept, approach, strategy, use cases, and enabling technologies for customer centricity. Throughout the year, this service will address the following topics: Customer Experience Trends in Asia/Pacific Retail Banking How Asia/Pacific Consumers Pay - Evolving Trends in Retail Payments Customer Centricity in Retail Banking IT and Operations Customer Centricity in Marketing and Product Development Customer Centricity Technology Providers for Asia/Pacific Retail Banking." },
                    { new Guid("025e9288-b2de-47e3-ab30-5ccff81025d0"), new Guid("e7492b0c-6e9d-471e-91bb-1f274af037ca"), null, null, null, new Guid("80a66b63-61f3-4968-afa2-1eaf50dd2b7d"), true, false, false, null, "Consumer Banking Engagement Strategies", "CONSUMER BANKING ENGAGEMENT STRATEGIES", "Being successful in banking will be determined by how well institutions manage the transformation in both digital and physical channels. Customers have seemingly ubiquitous access to their accounts on their terms and on their devices as banks continue to strategize about the future of the branch network and new channels emerge. Unfortunately, many banks are still looking at their channel strategy in a silo without fully understanding that customer engagement is the key to a profitable relationship. Today’s technology has fostered this customer-led revolution, yet there are many more changes yet to be realized as new technology is introduced. Advances in how we engage the customer are pushing the limits and skill sets of business units, marketers, and IT personnel as customers demand more from their retail bank. The Financial Insights: Consumer Banking Engagement Strategies provides critical analysis of the opportunities and options facing banks as they wrestle with their technology plans and investment decisions in alignment with their strategic goals. This research delivers key insights regarding the business drivers of and value delivered from customer-facing banking technology investments. Topics Addressed Throughout the year, this service will address the following topics: Digital transformation: Strategies and use cases are developing as banks transform all channels, including account opening and onboarding, ATM and ITM, augmented and virtual reality, branch banking, call center, chatbot services, contextualized marketing, conversational banking, digital banking (online and mobile), and social business. Whether these are first-generation offerings or have been around for decades, strategies need to be developed to implement, support, and upgrade these channels to stay with the times. Engagement strategies: Financial institutions are realizing that the number of engagements a customer has is an important factor in profitability. Using big data and analytics to properly measure the number of engagements is a start, but most institutions need to go beyond a prescriptive approach to customer behavior to a more cognitive approach. Omni-experience: The customer life-cycle process offers multiple channels that define the experience and dictate current and future relationships. Customer trends and strategies: This includes topics from level of interaction today to what is likely to be future behavior." },
                    { new Guid("315002c7-fff8-4aa1-8f4c-e75617ec2c1d"), new Guid("e7492b0c-6e9d-471e-91bb-1f274af037ca"), null, null, null, new Guid("d0132dbf-3235-429e-846a-3eec0d72a88a"), true, false, false, null, "Maternal mortality has declined but progress is uneven across regions", "MATERNAL MORTALITY HAS DECLINED BUT PROGRESS IS UNEVEN ACROSS REGIONS", "A total of 295 000 [UI 1 80%: 279 000–340 000] women worldwide lost their lives during and following pregnancy and childbirth in 2017, with sub-Saharan Africa and South Asia accounting for approximately 86% of all maternal deaths worldwide. The global maternal mortality ratio (MMR, the number of maternal deaths per 100 000 live births) was estimated at 211 [UI 80%: 199–243], representing a 38% reduction since 2000. On average, global MMR declined by 2.9% every year between 2000 1 UI = uncertainty interval. and 2017. If the pace of progress accelerates enough to achieve the SDG target (reducing global MMR to less than 70 per 100 000 live births), it would save the lives of at least one million women. The majority of maternal deaths are preventable through appropriate management of pregnancy and care at birth, including antenatal care by trained health providers, assistance during childbirth by skilled health personnel, and care and support in the weeks after childbirth. Data from 2014 to 2019 indicate that approximately 81% of all births globally took place in the presence of skilled health personnel, an increase from 64% in the 2000–2006 period. In sub-Saharan Africa, where roughly 66% of the world’s maternal deaths occur, only 60% of births were assisted by skilled health personnel during the 2014–2019 period. " },
                    { new Guid("39e9d969-1364-4c71-a72b-af5b3f1d6da8"), new Guid("afa94f84-fd29-466c-af20-9a3dc59046e6"), null, null, null, new Guid("d0132dbf-3235-429e-846a-3eec0d72a88a"), true, false, false, null, "Investing in strengthening country health information systems", "investing in strengthening country health information systems", "Accurate, timely, and comparable health-related statistics are essential for understanding population health trends. Decision-makers need the information to develop appropriate policies, allocate resources and prioritize interventions. For almost a fifth of countries, over half of the indicators have no recent primary or direct underlying data. Data gaps and lags prevent from truly understanding who is being included or left aside and take timely and appropriate action. The existing SDG indicators address a broad range of health aspects but do not capture the breadth of population health outcomes and determinants. Monitoring and evaluating population health thus goes beyond the indicators covered in this report and often requires additional and improved measurements. WHO is committed to supporting Member States to make improvements in surveillance and health information systems. These improvements will enhance the scope and quality of health information and standardize processes to generate comparable estimates at the global level. Getting accurate data on COVID-19 related deaths has been a challenge. The COVID-19 pandemic underscores the serious gaps in timely, reliable, accessible and actionable data and measurements that compromise preparedness, prevention and response to health emergencies. The International Health Regulations (IHR) (2005) monitoring framework is one of the data collection tools that have demonstrated value in evaluating and building country capacities to prevent, detect, assess, report and respond to public health emergencies. From self-assessment of the 13 core capacities in 2019, countries have shown steady progress across almost all capacities including surveillance, laboratory and coordination. As the pandemic progresses, objective and comparable data are crucial to determine the effectiveness of different national strategies used to mitigate and suppress, and thus to better prepare for the probable continuation of the epidemic over the next year or more." },
                    { new Guid("56223693-ff5a-4b25-9233-e65856cab234"), new Guid("e7492b0c-6e9d-471e-91bb-1f274af037ca"), null, null, null, new Guid("d0132dbf-3235-429e-846a-3eec0d72a88a"), true, false, false, null, "Health systems and universal health coverage", "HEALTH SYSTEMS AND UNIVERSAL HEALTH COVERAGE", "In the SDG monitoring framework, progress towards universal health coverage (UHC) is tracked with two indicators: (i) a service coverage index (which measures coverage of selected essential health services on a scale of 0 to 100); and (ii) the proportion of the population with large out-of-pocket expenditures on health care (which measures the incidence of catastrophic health spending, rendered as percentage). The service coverage index improved from 45 globally in 2000 to 66 in 2017, with the strongest increase in low-and lower-middle-income countries, where the baseline at 2000 was lowest. However, the pace of that progress has slowed since 2010. The improvements are especially notable for infectious disease interventions and, to a lesser extent, for reproductive, maternal and child health services. Within countries, coverage of the latter services is typically lower in poorer households than in richer households. Overall, between one third and one half the world’s population (33% to 49%) was covered by essential health services in 2017 . Service coverage continued to be lower in low- and middle-income countries than in wealthier ones; the same held for health workforce densities and immunization coverage (Figure 1.2). Available data indicate that over 40% of all countries have fewer than 10 medical doctors per 10 000 people, over 55% have fewer." },
                    { new Guid("f8ac51be-bafe-4f7a-bcaa-28c18691d057"), new Guid("afa94f84-fd29-466c-af20-9a3dc59046e6"), null, null, null, new Guid("d0132dbf-3235-429e-846a-3eec0d72a88a"), true, false, false, null, "Health Insights: European Life Science and Pharma", "HEALTH INSIGHTS: EUROPEAN LIFE SCIENCE AND PHARMA", "Digital transformation is key in the European life science industry's ongoing efforts to transform itself as it seeks to find new paths to innovation, optimize operational efficiency and effectiveness, and regain long-term sustainability. The transition toward new care delivery ecosystems and outcome-based reimbursement models enhances life science organizations' needs to implement new approaches to information management and use. The digital mission in the life sciences is to integrate new and existing data, information, and knowledge into a more knowledge-centric approach to new drug and medical devices development, to product commercialization and management, and to treatment delivery. The European life science digital transformation service provides a forward-looking analysis of IT and technologies that are being adopted all along the value chain of the European life science industry. The service focuses on how European life science organizations can leverage the new range of technologies underpinned by the 3rd Platform technologies and innovation accelerators to support efforts to improve the operational efficiency of the industry and better engage with the ultimate end user of life science innovations, namely patients. This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("aa5997b6-e887-43be-bcd5-3285a2a5edaa"), new Guid("e7492b0c-6e9d-471e-91bb-1f274af037ca"), null, null, null, new Guid("d0132dbf-3235-429e-846a-3eec0d72a88a"), true, false, false, null, "European Digital Hospital Evolution", "EUROPEAN DIGITAL HOSPITAL EVOLUTION", "European Digital Hospital service provides detailed information about the evolution of digital hospital transformation and technology adoption. It offers valuable insights into IT solution trends within the hospital sector, including healthcare-specific technologies such as electronic health records; admission, discharge, and transfer; digital medical imaging systems; VNA and archiving; and business intelligence and patient management systems. This service focuses on a vital component of healthcare — the digital hospital. Healthcare systems across the world are facing care quality and sustainability challenges that require a new approach to care delivery and reimbursement models. The fully digital hospital is in the center of that journey, being a key enabler of change. The hospital CIOs are facing new technology but, at the same time, a growing legacy IT burden that must be managed alongside of new digital initiatives. In healthcare, management of legacy systems is therefore a growing challenge — when looking at not only obsolete digital applications but also infrastructure and, in the end, how healthcare providers purchase digital applications and platforms. When IT fails to deliver because of legacy and lack of agility, the business model and clinical processes become legacy as well. Key solutions such as EHR, RIS, PACS, VNA, ECM, advanced analytics, cloud, and mobility will fundamentally impact hospitals' digital strategies execution and success.This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("e33025cd-dbc8-4ae2-9d64-a16d04a65896"), new Guid("afa94f84-fd29-466c-af20-9a3dc59046e6"), null, null, null, new Guid("d0132dbf-3235-429e-846a-3eec0d72a88a"), true, false, false, null, "European Value-Based Healthcare Digital Transformation", "EUROPEAN VALUE-BASED HEALTHCARE DIGITAL TRANSFORMATION", " With the advent of the value-based paradigm, healthcare organizations are today tasked to ensure equitable access and financial sustainability while improving and reducing the variation of clinical outcomes in healthcare systems. Increasing collaboration across the healthcare value chain, adopting a more personalized approach to treatment, championing the patient experience, and adopting outcome-based business models are fast emerging as necessary for the healthcare industry to succeed. The IDC Health Insights: European Value-Based Healthcare Digital Transformation Strategies service focuses on analyzing European healthcare providers, payers, and public health policy makers' digital strategy and best practices in supporting the adoption of value-based healthcare to deliver the maximum value to patients. Value-based healthcare is a key driver of digital transformation as it requires an unprecedented level of availability of patient information. Key solutions as HIE, population health management, smart patient engagement tools, advanced analytics and cognitive, and cloud and mobility will fundamentally impact care providers and payers' value-based healthcare strategies execution and success. Approach. This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("deb360a4-728a-44a3-9450-13999832128c"), new Guid("bf262f56-2f19-47a8-8c0b-cefbbd055d2f"), null, null, null, new Guid("2042dbce-edbb-4923-914f-e2d485cedb87"), true, false, false, null, "Cybersecurity Analytics, Intelligence, Response and Orchestration", "CYBERSECURITY ANALYTICS, INTELLIGENCE, RESPONSE AND ORCHESTRATION", "Cybersecurity Analytics, Intelligence, Response and Orchestration service covers security software and hardware products related to analytic security platforms, security and vulnerability management (SVM), and security orchestration platforms. Specific functions covered include vulnerability management and intelligence, SIEM, security analytics, threat hunting, incident detection and response, and orchestration. The service is designed to create in-depth coverage of the analytic-based and platform security markets. Markets and Subjects Analyzed Vulnerability management SIEM Security analytics Incident detection and response Threat analytics Automation Orchestration Core Research Security AIRO Forecast Security AIRO Market Share SIEM Market Share and Forecast Market MAP In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment. Key Questions Answered What is the size and market opportunity for security analytics solutions? Who are the major players in the security analytics space? What is the size and market opportunity for security orchestration solutions? What is the size and market opportunity for threat analytics solutions? How has the competitive landscape changed through digital transformation and adoption of cloud and enabling technologies?" },
                    { new Guid("9ae87fe8-9be5-488d-9e80-d1f9c546a4e3"), new Guid("84b25c90-772b-4eb9-a6b6-c80b22217fc7"), null, null, null, new Guid("2042dbce-edbb-4923-914f-e2d485cedb87"), true, false, false, null, "DevOps Analytics, Automation and Security", "DEVOPS ANALYTICS, AUTOMATION AND SECURITY", "DevOps is a powerful modern approach to unifying the business strategy, development, testing, deployment, and life-cycle operation of software. This approach is accomplished by improving business, IT, and development collaboration while taking full advantage of automation technologies, end-to-end processes, microservices architecture, and cloud infrastructure to accelerate development and delivery and enable innovation. This subscription service provides insight, forecasts, and thought leadership to assist IT management, IT professionals, IT vendors, and service providers in creating compelling DevOps strategies and solutions. Markets and Subjects Analyzed DevOps adoption drivers, benefits, and use cases Identification of DevOps innovators and best practices Identification of critical DevOps tools enabling automation, open source technologies, and market leaders Analysis of the impact DevOps is having on IT infrastructure hardware and software purchasing and deployment priorities across on-premises and cloud service platforms Major transformation and impact DevOps is having on staffing, skills, and internal processes Core Research Worldwide DevOps Software Forecast, 2018-2022 Worldwide DevOps Software Market Share, 2018-2022 The Role of Cloud-Based IDEs to Drive DevOps Adoption Changing Drivers of DevOps: Role of Serverless and Microservices/Cloud-Native Architectures Market Analysis Perspective: Worldwide Developer, Operations, and DevOps Evolving Approaches and Practices in DevOps The Shift to Continuous Delivery and Deployment Tools and Frameworks for Supporting DevOps Workflows In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("fb9ae4bc-e6d6-4c8d-a0be-eba5496c77cd"), new Guid("941d0105-2b53-4f34-a1ed-b524f9ace923"), null, null, null, new Guid("2042dbce-edbb-4923-914f-e2d485cedb87"), true, false, false, null, "Multicloud Data Management and Protection", "MULTICLOUD DATA MANAGEMENT AND PROTECTION", "Multicloud Data Management and Protection service enables storage vendors and IT users to have a more comprehensive view of the evolving data protection, availability, and recovery market. Integrating cloud technology with traditional hardware and software components, the report series will include market forecasts and provide a combination of timely tactical market information and long-term strategic analysis. Markets and Subjects Analyzed Cloud-based solutions, including backup as a service (BaaS) and disaster recovery as a service (DRaaS) Public, private, and hybrid cloud data protection technologies Disk-based data protection and recovery solutions packaged as software, appliance, or gateway system with a focus on technology evolution Market forecast based on total terabytes shipped and revenue for disk-based data protection and recovery (No vendor shares will be published.) End-user adoption of different data protection and recovery technologies and solutions Cloud service providers delivering cloud-based data protection Data protection for nontraditional data types (e.g., Mongo DB, Cassandra, Hadoop) Vendors delivering disk-based data protection and recovery solutions with focus on architecture based on use cases and applications Technologies and processes, including backup, snapshots, replication, continuous data protection, and data deduplication Impact of purpose-built backup appliances (PBBAs) and hyperconverged systems on overall market growth and customer adoption Implications of copy data management and impact on data availability, capacity management, cost, governance, and security The evolutionary impact of virtual infrastructure and object storage on data protection schemes Implications of flash technology for data protection Data protection best practice guidelines and adoption Core Research Market Analysis, Sizing, and Vendor Shares of Backup as a Service and Disaster Recovery as a Service Use of Disk-Based Technologies for Protection and Recovery Adoption Patterns and Role of PBBAs in Customer Environments End-User Needs and Requirements Data Protection and Recovery Software Market Size, Shares, and Forecasts Disk-Based Data Protection Market Size and Forecast Virtual Backup Appliance Solutions Intersection of Data Protection Software and Management with Cloud Services In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("3b480a1c-ae0e-4efe-8083-19d1635ca2c1"), new Guid("941d0105-2b53-4f34-a1ed-b524f9ace923"), null, null, null, new Guid("2042dbce-edbb-4923-914f-e2d485cedb87"), true, false, false, null, "Software Channels and Ecosystems", "SOFTWARE CHANNELS AND ECOSYSTEMS", "Software Channels and Ecosystems research service offers intelligence and expertise to help channel executives and program managers develop, implement, support, and manage effective channel strategies and programs to drive successful relationships with the spectrum of partner activities (e.g., resale, managed/cloud services, consulting professional services, and software development). In addition, it provides a comprehensive view of the value of the indirect software market and its leading vendor proponents. This service also identifies and analyzes key industry trends and their impact on channel relationship drivers and channel partner business models. Subscribers are invited to 's semiannual Software Channel Leadership Council where and channel executives present and discuss key industry issues. Markets and Subjects Analyzed Multinational software vendors Digital transformation in the partner ecosystem Software and SaaS channel revenue flow Digital marketplaces Cloud and channels Partner-to-partner networking Key channel trends Software partner program profiles and innovative practices Core Research Channel Revenue Forecast and Analysis Partner Marketing and Communications Ecosystem Digital Transformation Cloud and Channels Emerging Partner Business Models Partner Collaboration." },
                    { new Guid("074d940b-da31-42d9-9aea-de5a6c402887"), new Guid("e7492b0c-6e9d-471e-91bb-1f274af037ca"), null, null, null, new Guid("2042dbce-edbb-4923-914f-e2d485cedb87"), true, false, false, null, "Agile Application Life-Cycle, Quality and Portfolio Strategies", "AGILE APPLICATION LIFE-CYCLE, QUALITY AND PORTFOLIO STRATEGIES", "Agile Application Life-Cycle, Quality and Portfolio Strategies service provides insight into business and IT alignment through end-to-end application life-cycle management (ALM) and DevOps strategies including agile, IT portfolio management, software quality, testing and verification tools, software change and configuration, and continuous releases for digital transformation. This service provides insights on how product, organizational, and process transitions can help address regulatory compliance, security, and licensing issues covering cloud, SaaS, and open source software. It also analyzes trends related to ALM and software deployment with applications that leverage and target artificial intelligence (AI) and machine learning (ML), collaboration, mobile and embedded apps, IoT, virtualization, and complex sourcing and evolve to adaptive work, project and portfolio management (PPM), and agile adoption with organizational and process strategies (e.g., Scaled Agile Framework [SAFe]). Markets and Subjects Analyzed Software life-cycle process, requirements, configuration management and collaboration, and agile development and projects Software quality, including mobile, cloud, and embedded strategies PPM and work management software trends and strategies IT project and portfolio management software Developer, business, and operations trends Tools enabling DevOps and governance solutions to align complex deployment, financial management, infrastructure, and business priorities Trends in software configuration management (SCM), including connecting business to IT via requirements management and web services demands on agile life-cycle management solutions Automated software quality (ASQ) tools evolution: Hosted testing, web services, API and cloud testing, service virtualization, vulnerability, risk and value-based testing, and other emerging technologies (e.g., mobile, analysis, and metrics) Trends in collaborative software development with social media" },
                    { new Guid("3262945f-2b98-4e9c-aae5-7f2b5ec193de"), new Guid("afa94f84-fd29-466c-af20-9a3dc59046e6"), null, null, null, new Guid("2042dbce-edbb-4923-914f-e2d485cedb87"), true, false, false, null, "Canadian Internet of Things Ecosystem and Trends", "CANADIAN INTERNET OF THINGS ECOSYSTEM AND TRENDS", "The Internet of Things (IoT) market is poised for exponential growth over the next several years. However, the IoT ecosystem is a complex segment with multiple layers and hundreds of players, including device and module manufacturers, communication service providers (CSPs), IoT platform players, applications, analytics and security software vendors, and professional services providers. 's Canadian Internet of Things Ecosystem and Trends service analyzes the regionally specific growth of this market from the autonomously connected units in the enterprise world to the applications and services that will demonstrate the power of a world of connected .Markets and Subjects Analyzed Market size and forecast for the Canadian IoT market Canadian IoT revenue forecast by industry Canadian IoT revenue forecast by use cases Taxonomy of the IoT ecosystem: How is the market organized, and what are the key segments and technologies? Canadian-specific demand-side data from various segments (e.g., IT and LOB) Analysis of Canadian-specific vendor readiness and go-to-market strategies for IoT Vertical use cases/case studies of IoT Core Research Taxonomy of the IoT Ecosystem Forecast for the Canadian IoT Market by Industry Forecast for the Canadian IoT Market by Use Cases Profiles of Canadian Vendors Leading IoT Evolution Case Studies of Successful Canadian IoT Implementations Canadian Internet of Things Decision-Maker Survey In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("1c0c7738-81ea-46b6-9c4e-240fa6f1cf5a"), new Guid("bf262f56-2f19-47a8-8c0b-cefbbd055d2f"), null, null, null, new Guid("2e707290-83df-4b8e-8419-7c425bc444be"), true, false, false, null, "Worldwide Utilities Connected Asset", "WORLDWIDE UTILITIES CONNECTED ASSET", "Energy and digital technologies are radically transforming utilities asset operations and strategies. Renewables and decentralized power generation, energy delivery networks (electricity, gas, and heat), and water and waste water management benefit from the developments and adoption of IoT, AI, and digital twins.  Energy Insights: Worldwide Utilities Connected Asset Strategies service focuses on all these. It provides guidance to end users in terms of digital transformation (DX) use cases road map, analysis of emerging IT trends, and evaluation of technology providers in this space. It looks into the future of work in the utilities value chain context. At the same time, it provides technology vendors a view on utilities' operational excellence asset strategies addressing their go to market.This service develops unique analysis and comprehensive data through  Energy Insights' proprietary research projects, along with ongoing communications with industry experts, utility management, ICT vendors, and service providers. With decades of experience in the utility industry, our analysts leverage a broad spectrum of expertise and intellectual property from both  and  Energy Insights. Research documents elucidate business strategy, best practices, technology selection, and vendor assessment, along with short perspectives on topical issues. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research documents." },
                    { new Guid("bcc8b91e-78c7-449f-98fe-702c3ce0680e"), new Guid("941d0105-2b53-4f34-a1ed-b524f9ace923"), null, null, null, new Guid("2e707290-83df-4b8e-8419-7c425bc444be"), true, false, false, null, "Worldwide Mining Policy", "WORLDWIDE MINING POLICY", "Worldwide Mining Strategies examines the business environment across the mining sector value chain from exploration through to operations, processing, supply chain, and trading globally. Mining companies are operating in an increasingly competitive commodity market environment, driven by pressures relating to accessing funding, assets, and talent. Technology and data are playing an increasingly critical role across the operation to enable decision support, automation, integration, and control. This service provides comprehensive insights into the best practices that show how mining companies are responding and what road maps these companies will need to build the information technology (IT) capabilities required to create integrated, agile, and responsive operations. Mining companies are changing the way they buy technology, how they collaborate to frame the problems to be solved, how they engage with technology suppliers, and how they innovate across their businesses bringing together the capabilities of IT and operational technology (OT). This service tracks the IT investment priorities for organizations seeking to scale value creation across their organization and the impact on decision making and best practices relating to technology, process, and organizational change. This service distills market and industry data into incisive analysis drawn from in-depth interviews with industry experts, mining staff from across the business, and technology vendors. Insight and analysis are further supported and validated through rigorous research methodologies in quantitative market analysis.  Energy Insights' analysts develop unique and comprehensive analyses of this data, focused on providing actionable recommendations. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("27f99c7d-31d9-4350-bb41-aa804e44150a"), new Guid("84b25c90-772b-4eb9-a6b6-c80b22217fc7"), null, null, null, new Guid("2e707290-83df-4b8e-8419-7c425bc444be"), true, false, false, null, "Asia/Pacific Oil and Gas Digital Transformation Procedure", "ASIA/PACIFIC OIL AND GAS DIGITAL TRANSFORMATION PROCEDURE", "Asia/Pacific Oil and Gas Digital Transformation Strategies examines the business environment of the oil and gas industry in the Asia/Pacific region, excluding Japan. The service covers the whole value chain from upstream, midstream, and downstream but is focused on upstream. It seeks to support companies working to deliver against efficiency objectives through technology-led change that will deliver improved decision support and insight and flexibility to adjust to changing technology, markets, and political pressures. The Energy Insights: Asia/Pacific Oil and Gas Digital Transformation Strategies program provides advice on best practices and technology priorities that will support technology decision making, particularly, in relation to innovation, data management, and greater integration across silos and processes. The service is designed to support oil and gas companies with market insights and road mapping advice relating to technology decision making, particularly delivering insight to how increased value from investments can be delivered through better understanding of new metric, organization, talent, and process change. Key technology focus areas of the program include IT/OT integration, operational cybersecurity, IoT strategies, and asset management priorities. This service is built on the foundations of extensive engagement and research by the Energy Insights team across the oil and gas industry in this region. Our analysts work with industry experts, staff from the oil and gas business, and technology vendors to ensure that the research service is relevant and prioritizes areas that are of importance to them and the industry at large." },
                    { new Guid("a9ad77fe-9096-44ae-b239-da190229ff92"), new Guid("941d0105-2b53-4f34-a1ed-b524f9ace923"), null, null, null, new Guid("2e707290-83df-4b8e-8419-7c425bc444be"), true, false, false, null, "Global Utilities Customer Experience Scheme", "GLOBAL UTILITIES CUSTOMER EXPERIENCE SCHEME", "With the evolution of energy and water markets and the possibilities offered by digital transformation, utilities are developing new business models and are focusing on providing customers more interactive experiences. Smart metering, analytics, social media, mobility, cloud, and IoT are fundamentally impacting customer operation practices in both competitive and regulated markets. The Energy Insights: Worldwide Utilities Customer Experience Strategies service is designed to help utilities and energy retailers servicing customers in competitive and regulated markets at the worldwide level (including electricity, gas, and water). The service provides exclusive research and direct access to experts providing guidance to make the right IT investments and meet corporate objectives of customer satisfaction, reduction of cost to serve, and innovation. This service develops unique analysis and comprehensive data through Energy Insights' proprietary research projects, along with ongoing communications with industry experts, utility management, ICT vendors, and service providers. With decades of experience in the utility industry, our analysts leverage a broad spectrum of expertise and intellectual property from Energy Insights. Research reports elucidate business strategy, best practices, technology selection, and vendor assessment, along with short perspectives on topical issues. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("5b4c3342-64a2-467c-83da-1d2874185c4c"), new Guid("afa94f84-fd29-466c-af20-9a3dc59046e6"), null, null, null, new Guid("2e707290-83df-4b8e-8419-7c425bc444be"), true, false, false, null, "Worldwide Oil and Gas Downstream IT Blueprint", "WORLDWIDE OIL AND GAS DOWNSTREAM IT BLUEPRINT", "Energy Insights: Worldwide Oil and Gas Downstream IT Strategies advisory research service takes the worldwide oil and gas IT strategies research and applies it to the downstream operations in oil and gas. The research focuses on companies that are involved in the production for sale of all petroleum products, from incoming crude to outgoing product. It will also encompass the technology companies that provide the technology foundation for the operation of the downstream business. Energy Insights: Worldwide Oil and Gas Downstream IT Strategies will focus its analysis on the digital transformation (DX) use cases, priorities, innovation technology, and road map for all aspect of companies that produce and distribute petroleum products. It will provide technology vendors a road map for product development and messaging in oil and gas downstream. Throughout the year, this service will address the following topics: Digital transformation – Investment priorities and road maps for transformation of the downstream business operation Operations technology — IT/OT convergence investments that help downstream organizations transform how they operate the production lines Asset management – Technology changes driving asset management organization and transformation Transformation to scale – Development of a transformed infrastructure to scale to market volatility Our research addresses the following issues that are critical to your success: What is the impact of transformational technologies on the oil and gas downstream DX road map? How does an oil and gas downstream company organize itself around the Future of Work? What are the market trends in oil and gas downstream that affect the use case prioritization for DX? How should trading partners be integrated into the DX road map? Who are the technology vendors that can support DX in oil and gas downstream?" },
                    { new Guid("91f95a71-4116-4064-a831-c492d033207a"), new Guid("84b25c90-772b-4eb9-a6b6-c80b22217fc7"), null, null, null, new Guid("c042988d-f630-4e8e-bd3f-e3ccb3e39158"), true, false, false, null, "Customer Experience Management Strategies", "CUSTOMER EXPERIENCE MANAGEMENT STRATEGIES", "Customer Experience Management Strategies SIS provides a framework and critical knowledge for understanding the changing nature of the customer experience (CX) and guides chief experience officers and their organizations as they master the digital transformation of the customer experience. This product covers the concepts of experience management and customer experience, looking at how digital transformation is driving change to customer expectations, preferences, and behavior and how enterprises must adopt new technologies to meet these urgent challenges. Markets and Subjects Analyzed: Experience management; Customer-centric engagement Customer-centric operations; Customer-centric solution design; Impact of 3rd Platform technologies on customer experience; Intelligence and analytics-driven customer experience; Customer experience along the customer journey; Customer experience benchmarks Core Research: Customer Experience Taxonomy; Digital Transformation of the Customer Experience; Redefining Experience Management; PeerScape: Customer Experience; Customer Experience in an Algorithm Economy; Technology-Enabled Storytelling; MaturityScape: Customer Experience; Customer Intelligence and Analytics; Innovation Accelerators in Customer Experience: Artificial Intelligence; In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("093713ed-0a47-456f-a7cf-7b56a64c2042"), new Guid("bf262f56-2f19-47a8-8c0b-cefbbd055d2f"), null, null, null, new Guid("c042988d-f630-4e8e-bd3f-e3ccb3e39158"), true, false, false, null, "Retail Insights: European Retail Digital Transformation Strategies", "RETAIL INSIGHTS: EUROPEAN RETAIL DIGITAL TRANSFORMATION STRATEGIES", "European retailing is in a state of change since digital technologies have become so pervasive in the retail journey and consumer expectations, in terms of customer experience, with their preferred brands rapidly evolving. European retailers are adapting to these changes through the adoption of commerce everywhere business models and technology. Retailers from across Europe are in the process of determining how digital impacts them and what their digital transformation approach and strategy should be. This reveals a more and more complex and highly differentiated European region, where differences exist and persist across countries as well as subsegments (fashion and apparel, department stores, food and grocery, etc.). Retail Insights is witnessing among European retailers, at varying degrees of maturity, a race to digitize because of the potential new revenue streams and retail operational efficiencies that can be derived. Retail Insights: European Retail Digital Transformation Strategies advisory service examines the impact of digital transformation on the European retailers' business, technology, and organizational areas. Specific coverage is given to provide valuable insights into the European retail industry, with a specific focus on digital transformation strategies applied by retail companies to improve the omni-channel customer experience. This advisory service develops unique analysis and comprehensive data through Retail Insights' proprietary research projects, along with ongoing communications with industry experts, retail CIOs, and line-of-business executives, and ICT product and service vendors. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports. Our analysts are also available to provide personalized advice for retail executives and ICT vendors to help them make better-informed decisions." }
                });

            migrationBuilder.InsertData(
                table: "ReportTags",
                columns: new[] { "TagId", "ReportId" },
                values: new object[,]
                {
                    { new Guid("2bf7759f-8e07-4415-b620-182f259237c6"), new Guid("41d7ceee-fc64-4227-b531-2f6946ad3431") },
                    { new Guid("26c4bcfc-935a-4a3e-bc39-e995aa117a4a"), new Guid("39e9d969-1364-4c71-a72b-af5b3f1d6da8") },
                    { new Guid("52ae14e3-33bc-4d5c-a9bc-09c0afb88e00"), new Guid("39e9d969-1364-4c71-a72b-af5b3f1d6da8") },
                    { new Guid("b6d5154e-4db9-4b1d-b33c-16f766fafc2f"), new Guid("315002c7-fff8-4aa1-8f4c-e75617ec2c1d") },
                    { new Guid("52ae14e3-33bc-4d5c-a9bc-09c0afb88e00"), new Guid("315002c7-fff8-4aa1-8f4c-e75617ec2c1d") },
                    { new Guid("ddfb2078-91ef-469e-a019-ae8a5a6b95a5"), new Guid("025e9288-b2de-47e3-ab30-5ccff81025d0") },
                    { new Guid("2ef11211-e751-4d9f-885e-8071bdd22e48"), new Guid("025e9288-b2de-47e3-ab30-5ccff81025d0") },
                    { new Guid("2ef11211-e751-4d9f-885e-8071bdd22e48"), new Guid("70123bf4-a702-402c-9ff3-cb2ea8eba45d") },
                    { new Guid("60f2f2cf-8052-47ab-ad8a-948a4b60a1d8"), new Guid("70123bf4-a702-402c-9ff3-cb2ea8eba45d") },
                    { new Guid("18113347-addf-4b30-a8c3-5dda7ef744a9"), new Guid("9c6ad87d-b16a-4773-8e9f-af71088d848d") },
                    { new Guid("60f2f2cf-8052-47ab-ad8a-948a4b60a1d8"), new Guid("9c6ad87d-b16a-4773-8e9f-af71088d848d") },
                    { new Guid("ddfb2078-91ef-469e-a019-ae8a5a6b95a5"), new Guid("24ba7d9f-ef1a-4bdb-b966-44b09c01c244") },
                    { new Guid("2ef11211-e751-4d9f-885e-8071bdd22e48"), new Guid("24ba7d9f-ef1a-4bdb-b966-44b09c01c244") },
                    { new Guid("60f2f2cf-8052-47ab-ad8a-948a4b60a1d8"), new Guid("24ba7d9f-ef1a-4bdb-b966-44b09c01c244") },
                    { new Guid("4f9e14aa-3fb5-4235-9066-d9ab835a891a"), new Guid("56223693-ff5a-4b25-9233-e65856cab234") },
                    { new Guid("f7f4c29f-e67f-4a5a-ade8-5e65f72b005a"), new Guid("cc7e2d5e-8fbe-44b7-b46d-f7c953f36453") },
                    { new Guid("ddfb2078-91ef-469e-a019-ae8a5a6b95a5"), new Guid("6f308b37-d97a-478f-88c0-3256c8b123c5") },
                    { new Guid("18113347-addf-4b30-a8c3-5dda7ef744a9"), new Guid("6f308b37-d97a-478f-88c0-3256c8b123c5") },
                    { new Guid("4454473e-9573-4454-900e-6415ffbc6fb8"), new Guid("8440a849-2935-4c53-b6e1-77db5bf134ae") },
                    { new Guid("d38ab07a-c1c8-4ead-a8fc-6aa64767866c"), new Guid("8440a849-2935-4c53-b6e1-77db5bf134ae") },
                    { new Guid("f7f4c29f-e67f-4a5a-ade8-5e65f72b005a"), new Guid("7ceb649a-9fc2-409f-afe8-8d747c3a80ba") },
                    { new Guid("14f125e6-0aba-4f86-9c58-f725026e945e"), new Guid("7ceb649a-9fc2-409f-afe8-8d747c3a80ba") },
                    { new Guid("4454473e-9573-4454-900e-6415ffbc6fb8"), new Guid("7ceb649a-9fc2-409f-afe8-8d747c3a80ba") },
                    { new Guid("60f2f2cf-8052-47ab-ad8a-948a4b60a1d8"), new Guid("99ac8caf-4841-4efc-90a6-5d3f58587f43") },
                    { new Guid("14f125e6-0aba-4f86-9c58-f725026e945e"), new Guid("99ac8caf-4841-4efc-90a6-5d3f58587f43") },
                    { new Guid("14f125e6-0aba-4f86-9c58-f725026e945e"), new Guid("5133194c-2c5f-415f-afae-04ce148762a4") },
                    { new Guid("d38ab07a-c1c8-4ead-a8fc-6aa64767866c"), new Guid("5133194c-2c5f-415f-afae-04ce148762a4") },
                    { new Guid("14f125e6-0aba-4f86-9c58-f725026e945e"), new Guid("91f95a71-4116-4064-a831-c492d033207a") },
                    { new Guid("4454473e-9573-4454-900e-6415ffbc6fb8"), new Guid("91f95a71-4116-4064-a831-c492d033207a") },
                    { new Guid("2ef11211-e751-4d9f-885e-8071bdd22e48"), new Guid("cc7e2d5e-8fbe-44b7-b46d-f7c953f36453") },
                    { new Guid("26c4bcfc-935a-4a3e-bc39-e995aa117a4a"), new Guid("56223693-ff5a-4b25-9233-e65856cab234") },
                    { new Guid("4f9e14aa-3fb5-4235-9066-d9ab835a891a"), new Guid("f8ac51be-bafe-4f7a-bcaa-28c18691d057") },
                    { new Guid("f7f4c29f-e67f-4a5a-ade8-5e65f72b005a"), new Guid("f8ac51be-bafe-4f7a-bcaa-28c18691d057") },
                    { new Guid("f1b152e3-7ba8-4c85-8d0b-03f7266a00b4"), new Guid("41d7ceee-fc64-4227-b531-2f6946ad3431") },
                    { new Guid("2bf7759f-8e07-4415-b620-182f259237c6"), new Guid("5b4c3342-64a2-467c-83da-1d2874185c4c") },
                    { new Guid("946c72f1-2bfc-423b-a43b-92ca35669b85"), new Guid("5b4c3342-64a2-467c-83da-1d2874185c4c") },
                    { new Guid("f1b152e3-7ba8-4c85-8d0b-03f7266a00b4"), new Guid("a9ad77fe-9096-44ae-b239-da190229ff92") },
                    { new Guid("b0987c84-5552-4b87-ae09-0400a1e86b4d"), new Guid("a9ad77fe-9096-44ae-b239-da190229ff92") },
                    { new Guid("f7f4c29f-e67f-4a5a-ade8-5e65f72b005a"), new Guid("27f99c7d-31d9-4350-bb41-aa804e44150a") },
                    { new Guid("946c72f1-2bfc-423b-a43b-92ca35669b85"), new Guid("27f99c7d-31d9-4350-bb41-aa804e44150a") },
                    { new Guid("946c72f1-2bfc-423b-a43b-92ca35669b85"), new Guid("bcc8b91e-78c7-449f-98fe-702c3ce0680e") },
                    { new Guid("b0987c84-5552-4b87-ae09-0400a1e86b4d"), new Guid("bcc8b91e-78c7-449f-98fe-702c3ce0680e") },
                    { new Guid("f7f4c29f-e67f-4a5a-ade8-5e65f72b005a"), new Guid("1c0c7738-81ea-46b6-9c4e-240fa6f1cf5a") },
                    { new Guid("2bf7759f-8e07-4415-b620-182f259237c6"), new Guid("1c0c7738-81ea-46b6-9c4e-240fa6f1cf5a") },
                    { new Guid("f1b152e3-7ba8-4c85-8d0b-03f7266a00b4"), new Guid("1c0c7738-81ea-46b6-9c4e-240fa6f1cf5a") },
                    { new Guid("b70c6f3b-4b1a-47e1-98de-24c133a33f62"), new Guid("3262945f-2b98-4e9c-aae5-7f2b5ec193de") },
                    { new Guid("256d5428-6baa-408a-98b7-80abffb6d05f"), new Guid("3262945f-2b98-4e9c-aae5-7f2b5ec193de") },
                    { new Guid("fcb8d5a3-818c-4f3f-9241-3907d826794e"), new Guid("3262945f-2b98-4e9c-aae5-7f2b5ec193de") },
                    { new Guid("b70c6f3b-4b1a-47e1-98de-24c133a33f62"), new Guid("074d940b-da31-42d9-9aea-de5a6c402887") },
                    { new Guid("87e2c0e3-e7b3-4659-9725-bd48052fe187"), new Guid("074d940b-da31-42d9-9aea-de5a6c402887") },
                    { new Guid("256d5428-6baa-408a-98b7-80abffb6d05f"), new Guid("3b480a1c-ae0e-4efe-8083-19d1635ca2c1") },
                    { new Guid("fcb8d5a3-818c-4f3f-9241-3907d826794e"), new Guid("3b480a1c-ae0e-4efe-8083-19d1635ca2c1") },
                    { new Guid("87e2c0e3-e7b3-4659-9725-bd48052fe187"), new Guid("fb9ae4bc-e6d6-4c8d-a0be-eba5496c77cd") },
                    { new Guid("fcb8d5a3-818c-4f3f-9241-3907d826794e"), new Guid("fb9ae4bc-e6d6-4c8d-a0be-eba5496c77cd") },
                    { new Guid("b70c6f3b-4b1a-47e1-98de-24c133a33f62"), new Guid("9ae87fe8-9be5-488d-9e80-d1f9c546a4e3") },
                    { new Guid("fcb8d5a3-818c-4f3f-9241-3907d826794e"), new Guid("9ae87fe8-9be5-488d-9e80-d1f9c546a4e3") },
                    { new Guid("256d5428-6baa-408a-98b7-80abffb6d05f"), new Guid("deb360a4-728a-44a3-9450-13999832128c") },
                    { new Guid("fcb8d5a3-818c-4f3f-9241-3907d826794e"), new Guid("deb360a4-728a-44a3-9450-13999832128c") },
                    { new Guid("26c4bcfc-935a-4a3e-bc39-e995aa117a4a"), new Guid("e33025cd-dbc8-4ae2-9d64-a16d04a65896") },
                    { new Guid("f7f4c29f-e67f-4a5a-ade8-5e65f72b005a"), new Guid("e33025cd-dbc8-4ae2-9d64-a16d04a65896") },
                    { new Guid("f7f4c29f-e67f-4a5a-ade8-5e65f72b005a"), new Guid("aa5997b6-e887-43be-bcd5-3285a2a5edaa") },
                    { new Guid("b6d5154e-4db9-4b1d-b33c-16f766fafc2f"), new Guid("aa5997b6-e887-43be-bcd5-3285a2a5edaa") },
                    { new Guid("60f2f2cf-8052-47ab-ad8a-948a4b60a1d8"), new Guid("093713ed-0a47-456f-a7cf-7b56a64c2042") },
                    { new Guid("d38ab07a-c1c8-4ead-a8fc-6aa64767866c"), new Guid("093713ed-0a47-456f-a7cf-7b56a64c2042") }
                });
        }
    }
}
