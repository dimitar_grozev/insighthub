﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InsightHub.Data.Configurations
{
	public class AuthorsConfig : IEntityTypeConfiguration<Author>
    {
        public void Configure(EntityTypeBuilder<Author> builder)
        {
            builder.HasMany(a => a.WrittenReports)
                   .WithOne(wr => wr.Author)
                   .HasForeignKey(wr => wr.AuthorId);

            builder.HasOne(a => a.User)
                   .WithOne(u => u.Author)
                   .HasForeignKey<Author>(a => a.UserId);

        }
    }
}
