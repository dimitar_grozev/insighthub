﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InsightHub.Data.Configurations
{
	public class IndustriesConfig : IEntityTypeConfiguration<Industry>
    {
        public void Configure(EntityTypeBuilder<Industry> builder)
        {
            builder.HasMany(i => i.Reports)
                   .WithOne(r => r.Industry)
                   .HasForeignKey(r => r.IndustryId);

            builder.HasMany(i => i.Subscribers)
                   .WithOne(s => s.Industry)
                   .HasForeignKey(s => s.IndustryId);

            builder.HasIndex(i => i.Name).IsUnique();

            builder.Property(a => a.CreatedOn).HasDefaultValueSql("getutcdate()");
        }
    }
}
