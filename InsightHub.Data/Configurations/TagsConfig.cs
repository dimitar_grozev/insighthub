﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InsightHub.Data.Configurations
{
	public class TagsConfig : IEntityTypeConfiguration<Tag>
    {
        public void Configure(EntityTypeBuilder<Tag> builder)
        {
            builder.HasMany(t => t.ReportTags)
                   .WithOne(rt => rt.Tag)
                   .HasForeignKey(rt => rt.TagId);

            builder.HasIndex(t => t.Name).IsUnique();

            builder.Property(a => a.CreatedOn).HasDefaultValueSql("getutcdate()");
        }
    }
}
