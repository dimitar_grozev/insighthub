﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InsightHub.Data.Configurations
{
	public class UsersConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasOne(u => u.Author)
                   .WithOne(a => a.User)
                   .HasForeignKey<Author>(a => a.UserId);

            builder.HasOne(u => u.Client)
                   .WithOne(c => c.User)
                   .HasForeignKey<Client>(a => a.UserId);

            builder.Property(u => u.CreatedOn).HasDefaultValueSql("getutcdate()");
        }
    }
}
